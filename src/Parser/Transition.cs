using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser
{
    using static EqualityComparerFactories;

    public abstract class Transition
    {
        public IState Source { get; }

        private Transition(IState source)
        {
            Source = source;
        }

        public abstract class Binary : Transition
        {
            public IState Next { get; }

            public Binary(IState source, IState next)
                : base(source)
            {
                Next = next;
            }
        }

        public sealed class Call : Binary
        {
            public IState Child { get; }

            public Call(IState source, IState child, IState next)
                : base(source, next)
            {
                Child = child;
            }

            public override String ToString() =>
                $"call<{Source}, {Child}, {Next}>";
        }

        public sealed class Shift : Binary
        {
            public Char Terminal { get; }

            public Shift(IState source, Char terminal, IState next)
                : base(source, next)
            {
                Terminal = terminal;
            }

            public override String ToString() =>
                $"shift<{Source}, {Terminal}, {Next}>";
        }

        public sealed class Reduce : Transition
        {
            public IState Label { get; }

            public Reduce(IState source, IState label = null)
                : base(source)
            {
                Label = label;
            }

            public override String ToString() =>
                $"reduce<{Source}>";
        }

        public static IEqualityComparer<Transition> Comparer { get; }
            = new VariantsEqualityComparer<Transition>(_ => _.GetType())
            {
                new ComponentsEqualityComparer<Call>
                {
                    { _ => _.Source, State.Comparer },
                    { _ => _.Child, State.Comparer },
                    { _ => _.Next, State.Comparer },
                },
                new ComponentsEqualityComparer<Shift>
                {
                    { _ => _.Source, State.Comparer },
                    { _ => _.Terminal, OperatorEC<Char>() },
                    { _ => _.Next, State.Comparer },
                },
                new ComponentsEqualityComparer<Reduce>
                {
                    { _ => _.Source, State.Comparer },
                },
            };
    }
}

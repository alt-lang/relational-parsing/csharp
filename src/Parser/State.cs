﻿using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser
{
    using static EqualityComparerFactories;

    public interface IState : IEquatable<IState>
    {
        IEnumerable<Transition> Transitions { get; }
        String Name { get; }
    }

    public static class State
    {
        public static IEqualityComparer<IState> Comparer { get; }
            = IdentityEC<IState>();
    }
}

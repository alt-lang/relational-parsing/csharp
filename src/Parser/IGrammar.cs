﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser
{
    public interface IGrammar
    {
        IState StartState { get; }
        IState StopState { get; }
    }
}

using System.Collections.Generic;

namespace Parser.Caching
{
    using Utilities;

    public class PermanentCache<T> : Dictionary<T, T>, ICache<T>
    {
        private readonly Counter Calls = new Counter();
        private readonly Counter DistinctValues = new Counter();

        public PermanentCache(IEqualityComparer<T> comparer)
            : base(comparer)
        {
        }

        public T Add(T value)
        {
            Calls.Increment();
            if (base.TryGetValue(value, out var previous))
                return previous;
            base.Add(value, value);
            DistinctValues.Increment();
            return value;
        }
    }

    public class PermanentCacheStrategy : CacheStrategy
    {
        public override ICache<T> Create<T>(IEqualityComparer<T> comparer)
            => new PermanentCache<T>(comparer);
    }
}

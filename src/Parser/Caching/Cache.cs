using System.Collections.Generic;

namespace Parser.Caching
{
    public interface ICacheStrategy
    {
        ICache<T> Create<T>(IEqualityComparer<T> comparer);
    }

    public abstract class CacheStrategy : ICacheStrategy
    {
        public abstract ICache<T> Create<T>(IEqualityComparer<T> comparer);
    }

    public interface ICache<T>
    {
        T Add(T value);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Graphs
{
    using Caching;
    using Utilities;
    using static EqualityComparerFactories;

    public class DictionaryGraphEngine<TEdge> : GraphEngine<TEdge, DictionaryGraphEngine<TEdge>.Node>
    {
        [System.Diagnostics.DebuggerDisplay("{Count} neighbor(s)")]
        [System.Diagnostics.DebuggerTypeProxy(typeof(CollectionDisplay))]
        public class Node : Dictionary<(Node, TEdge), TEdge>, CollectionDisplay.ITarget
        {
            private static Int32 NextOrdinal = 0;
            public readonly Int32 Ordinal = NextOrdinal++;
            public Int32 Rank;

            public Node(IEqualityComparer<(Node, TEdge)> comparer)
                : base(comparer)
            {
                Rank = 0;
            }

            IEnumerable CollectionDisplay.ITarget.Items
                => this.Select(_ => (_.Value, _.Key.Item1));

            public override String ToString()
                => "$";
        }

        public override IEqualityComparer<Node> Comparer { get; }
        public override IEqualityComparer<TEdge> EdgeEC { get; }
        public override IEqualityComparer<TEdge> MergeEC { get; }
        private readonly IEqualityComparer<(Node, TEdge)> OutgoingEC;
        private readonly Func<TEdge, TEdge, TEdge> MergeEdges;
        private static readonly ICacheStrategy CacheStrategy = new PermanentCacheStrategy();
        private readonly ICache<Node> Cache;
        private readonly Counter EdgesTraversed = new Counter();

        public DictionaryGraphEngine(IEqualityComparer<TEdge> edgeEC, IEqualityComparer<TEdge> mergeEC, Func<TEdge, TEdge, TEdge> mergeEdges)
        {
            var comparer = UniqueHashCodeEC(IdentityEC<Object>());
            Comparer = comparer;
            EdgeEC = edgeEC;
            MergeEC = mergeEC;
            OutgoingEC = ValueTupleEC(Comparer, MergeEC);
            MergeEdges = mergeEdges;
            Cache = CacheStrategy.Create(DictionaryEC(OutgoingEC, EdgeEC) as IEqualityComparer<Node>);
        }

        public override Int32 Ordinal(Node node)
            => node.Ordinal;

        public override Int32 Rank(Node node)
            => node.Rank;

        public override IEnumerable<(TEdge, Node)> Neighbors(Node node)
        {
            EdgesTraversed.Increment(node.Count);
            return from kvp in node select (kvp.Value, kvp.Key.Item1);
        }

        public override Node Create(IEnumerable<(TEdge, Node)> edges)
        {
            var result = new Node(OutgoingEC);
            foreach (var (edge, target) in edges)
            {
                result[(target, edge)] = result.TryGetValue((target, edge), out var previous) ? MergeEdges(previous, edge) : edge;
                result.Rank = Math.Max(result.Rank, target.Rank + 1);
            }
            return Cache.Add(result);
        }
    }

    public class DictionaryGraphEngineFactory : IGraphEngineFactory
    {
        public IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, IEqualityComparer<TEdge> mergeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
            => new DictionaryGraphEngine<TEdge>(edgeEC, mergeEC, mergeEdges);

        public override String ToString()
            => $"{nameof(DictionaryGraphEngineFactory)}";
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Graphs
{
    using static EqualityComparerFactories;

    public class TrivialGraphEngine<TEdge> : GraphEngine<TEdge, TrivialGraphEngine<TEdge>.Node>
    {
        public class Node : List<(TEdge, Node)>
        {
            private static Int32 NextOrdinal = 0;
            public readonly Int32 Ordinal = NextOrdinal++;
            public readonly Int32 Rank;

            public Node(IEnumerable<(TEdge, Node)> items)
                : base(items)
            {
                Rank = this.Select(_ => _.Item2.Rank).DefaultIfEmpty(-1).Max() + 1;
            }
        }

        public override IEqualityComparer<TEdge> EdgeEC { get; }
        public override IEqualityComparer<TEdge> MergeEC { get; }
        private readonly IEqualityComparer<(TEdge, Node)> GroupingEC;
        private readonly Func<TEdge, TEdge, TEdge> MergeEdges;

        public TrivialGraphEngine(IEqualityComparer<TEdge> edgeEC, IEqualityComparer<TEdge> mergeEC, Func<TEdge, TEdge, TEdge> mergeEdges)
        {
            EdgeEC = edgeEC;
            MergeEC = mergeEC;
            GroupingEC = ValueTupleEC(MergeEC, Comparer);
            MergeEdges = mergeEdges;
        }

        public override Node Create(IEnumerable<(TEdge, Node)> edges)
            => new Node(edges.GroupBy(_ => _, (k, vs) => (vs.Select(_ => _.Item1).Aggregate(MergeEdges), k.Item2), GroupingEC));

        public override Int32 Ordinal(Node node)
            => node.Ordinal;

        public override Int32 Rank(Node node)
            => node.Rank;

        public override IEnumerable<(TEdge, Node)> Neighbors(Node node)
            => node;

        public override Boolean IsZero(Node value)
            => value.Count == 0;
    }

    public class TrivialGraphEngineFactory : IGraphEngineFactory
    {
        public IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, IEqualityComparer<TEdge> mergeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
            => new TrivialGraphEngine<TEdge>(edgeEC, mergeEC, mergeEdges);
    }
}

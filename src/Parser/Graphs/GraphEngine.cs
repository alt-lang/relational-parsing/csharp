using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Graphs
{
    using Algebra;
    using Utilities;

    public interface IGraphEngineFactory
    {
        /// <param name="edgeEC">
        /// An EqualityComparer classifying identical edges.
        /// </param>
        /// <param name="mergeEC">
        /// An EqualityComparer classifying mergeable parallel edges.
        /// Edges deemed equal by this comparer will be merged using <see cref="mergeEdges" />.
        /// Other edges will be left parallel.
        /// </param>
        /// <param name="mergeEdges">
        /// The function used to merge parallel edges.
        /// </param>
        /// <param name="mergeIsIdempotent">
        /// A hint. If set to true, the engine may optimize its operations by assuming <see cref="mergeEdges" /> is idempotent with respect to <see cref="edgeEC" />.
        /// </param>
        IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, IEqualityComparer<TEdge> mergeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent = false);
    }

    public interface IGraphEngine<TEdge>
    {
        IEqualityComparer<TEdge> EdgeEC { get; }
        IEqualityComparer<TEdge> MergeEC { get; }
        TResult Accept<TResult>(IGraphEngineVisitor<TEdge, TResult> visitor);
    }

    public interface IGraphEngineVisitor<TEdge, out TResult>
    {
        TResult Visit<TNode>(IGraphEngine<TEdge, TNode> engine);
    }

    public interface IGraphEngine<TEdge, TNode> : IAdditive<TNode>, IGraphEngine<TEdge>
    {
        Int32 Ordinal(TNode node);
        Int32 Rank(TNode node);
        IEnumerable<(TEdge, TNode)> Neighbors(TNode node);
        TNode Create(IEnumerable<(TEdge, TNode)> edges);
        TNode Map(TNode node, Func<TEdge, TEdge> mapping);
        void AddEdge(TNode source, TEdge edge, TNode target);
    }

    public static class GraphEngineExtensions
    {
        public static TNode Create<TEdge, TNode>(this IGraphEngine<TEdge, TNode> graphEngine, params (TEdge, TNode)[] edges)
            => graphEngine.Create(edges as IEnumerable<(TEdge, TNode)>);
    }

    public abstract class GraphEngine<TEdge, TNode> : Additive<TNode>, IGraphEngine<TEdge, TNode>
    {
        public abstract IEqualityComparer<TEdge> EdgeEC { get; }
        public abstract IEqualityComparer<TEdge> MergeEC { get; }
        public abstract Int32 Ordinal(TNode node);
        public abstract Int32 Rank(TNode node);
        public abstract IEnumerable<(TEdge, TNode)> Neighbors(TNode node);
        public abstract TNode Create(IEnumerable<(TEdge, TNode)> edges);

        public override TNode Add(IEnumerable<TNode> nodes)
            => Create(nodes.SelectMany(Neighbors));

        public virtual TNode Map(TNode node, Func<TEdge, TEdge> mapping)
            => Create(Neighbors(node).Select(_ => (mapping(_.Item1), _.Item2)));

        public virtual void AddEdge(TNode source, TEdge edge, TNode target)
            => throw new NotSupportedException();

        TResult IGraphEngine<TEdge>.Accept<TResult>(IGraphEngineVisitor<TEdge, TResult> visitor)
            => visitor.Visit(this);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Algebra
{

    public interface IOne<TValue> : ISet<TValue>
    {
        TValue One { get; }
        Boolean IsOne(TValue value);
    }

    public interface ILeftMultiplicative<TValue, TFactor> : IOne<TValue>
    {
        TValue Multiply(TFactor left, TValue right);
    }

    public interface IRightMultiplicative<TValue, TFactor> : IOne<TValue>
    {
        TValue Multiply(TValue left, TFactor right);
    }

    public interface IMultiplicative<TValue> : IOne<TValue>
    {
        Boolean IsCommutative { get; }
        TValue Multiply(TValue left, TValue right);
        TValue Multiply(IEnumerable<TValue> values);
    }

    public abstract class Multiplicative<TValue> : Set<TValue>, IMultiplicative<TValue>
    {
        public virtual Boolean IsCommutative
            => false;

        public virtual TValue One
            => Multiply(Enumerable.Empty<TValue>());

        public virtual Boolean IsOne(TValue value)
            => Comparer.Equals(value, One);

        public virtual TValue Multiply(IEnumerable<TValue> values)
            => values.Aggregate(One, Multiply);

        public virtual TValue Multiply(TValue left, TValue right)
            => Multiply(new [] { left, right });
    }

    public static class MultiplicativeExtensions
    {
        public static TValue Embed<TValue, TFactor>(this ILeftMultiplicative<TValue, TFactor> multiplicative, TFactor factor)
            => multiplicative.Multiply(factor, multiplicative.One);

        public static TValue Embed<TValue, TFactor>(this IRightMultiplicative<TValue, TFactor> multiplicative, TFactor factor)
            => multiplicative.Multiply(multiplicative.One, factor);

        public static TValue Multiply<TValue>(this IMultiplicative<TValue> multiplicative, params TValue[] values)
            => multiplicative.Multiply(values as IEnumerable<TValue>);
    }
}

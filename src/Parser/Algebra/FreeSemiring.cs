using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Algebra
{
    using Utilities;
    using static EqualityComparerFactories;

    public class FreeSemiring : Semiring<FreeValue>
    {
        public sealed override IEqualityComparer<FreeValue> Comparer
            => FreeValue.Comparer;

        public FreeSemiring()
        {
            AddMemoizer = new Memoizer<AddValue>(new ComponentsEqualityComparer<AddValue>
            {
                { _ => _.Values, MultiSetEC(Comparer) },
            }, _ => AddUniqueValues.Increment());
            MultiplyMemoizer = new Memoizer<MultiplyValue>(new ComponentsEqualityComparer<MultiplyValue>
            {
                { _ => _.Values, IsCommutative ? MultiSetEC(Comparer) : SequenceEC(Comparer) },
            }, _ => MultiplyUniqueValues.Increment());
            Zero = base.Zero;
            One = base.One;
        }

        public class AddValue : FreeValue
        {
            public IReadOnlyCollection<FreeValue> Values;

            public override String Description
                => $"Add({String.Join(", ", Values)})";
        }

        private readonly Counter AddCalls = new Counter();
        private readonly Counter AddUniqueValues = new Counter();
        private readonly Memoizer<AddValue> AddMemoizer;

        public override FreeValue Add(IEnumerable<FreeValue> values)
        {
            Cancellation.Check();
            AddCalls.Increment();
            var children = IsIdempotent ? new HashSet<FreeValue>(Comparer) : new List<FreeValue>() as ICollection<FreeValue>;
            foreach (var value in values)
            {
                if (IsZero(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 1)
                return children.Single();
            return AddMemoizer.Add(new AddValue { Values = children as IReadOnlyCollection<FreeValue> });
        }

        public override FreeValue Zero { get; }

        public class MultiplyValue : FreeValue
        {
            public IReadOnlyCollection<FreeValue> Values;

            public override String Description
                => $"Multiply({String.Join(", ", Values)})";
        }

        private readonly Counter MultiplyCalls = new Counter();
        private readonly Counter MultiplyUniqueValues = new Counter();
        private readonly Memoizer<MultiplyValue> MultiplyMemoizer;

        public override FreeValue Multiply(IEnumerable<FreeValue> values)
        {
            Cancellation.Check();
            MultiplyCalls.Increment();
            var children = new List<FreeValue>();
            foreach (var value in values)
            {
                if (IsZero(value))
                    return Zero;
                if (IsOne(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 1)
                return children.Single();
            return MultiplyMemoizer.Add(new MultiplyValue { Values = children });
        }

        public override FreeValue One { get; }
    }
}

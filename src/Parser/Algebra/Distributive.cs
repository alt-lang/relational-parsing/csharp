using System;

namespace Parser.Algebra
{
    public interface ILeftDistributive<TValue, TFactor> : ILeftMultiplicative<TValue, TFactor>, IAdditive<TValue>
    {
    }

    public abstract class LeftDistributive<TValue, TFactor> : Additive<TValue>, ILeftDistributive<TValue, TFactor>
    {
        public abstract TValue One { get; }
        public abstract TValue Multiply(TFactor left, TValue right);

        public virtual Boolean IsOne(TValue value)
            => Comparer.Equals(value, One);
    }

    public interface IRightDistributive<TValue, TFactor> : IRightMultiplicative<TValue, TFactor>, IAdditive<TValue>
    {
    }

    public abstract class RightDistributive<TValue, TFactor> : Additive<TValue>, IRightDistributive<TValue, TFactor>
    {
        public abstract TValue One { get; }
        public abstract TValue Multiply(TValue left, TFactor right);

        public virtual Boolean IsOne(TValue value)
            => Comparer.Equals(value, One);
    }
}

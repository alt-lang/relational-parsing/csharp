using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Algebra
{
    using static EqualityComparerFactories;

    [System.Diagnostics.DebuggerDisplay("#{Id} = {Description}")]
    public abstract class FreeValue
    {
        private static Int32 NextId = 0;
        public readonly Int32 Id = NextId++;

        public static IEqualityComparer<FreeValue> Comparer { get; }
            = IdentityEC<FreeValue>(); // new ComponentsEqualityComparer<FreeValue> { { _ => _.Id, OperatorEC<Int32>() } };

        public abstract String Description { get; }

        public sealed override String ToString()
            => $"#{Id}";
    }
}

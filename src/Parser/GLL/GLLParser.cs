using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.GLL
{
    using Common;
    using Valuations;
    using Descriptors = WorkingSet<(IState, Common.GSSNode<IState>)>;

    public partial class GLLParser : IParser<Boolean>
    {
        private readonly IGrammar Grammar;
        private readonly LookaheadAnalysis Analysis;
        public IValuationSemiring<Boolean> Semiring { get; }
            = new BooleanSemiring();

        public GLLParser(IGrammar grammar, Boolean useLookahead)
        {
            Grammar = grammar;
            if (useLookahead)
                Analysis = new LookaheadAnalysis(grammar);
        }

        private Boolean CallerAndReducer(Descriptors descriptors, Char? lookahead, GSSNode<IState> rootNode, Int32 layerIndex)
        {
            Boolean accept = false;
            var layer = new GSSLayer<IState>(layerIndex);
            var popped = new HashSet<GSSNode<IState>>();
            foreach (var (state, node) in descriptors.Remaining)
                foreach (var transition in state.Transitions)
                    if (Analysis == null || Analysis.IsViable(transition, lookahead))
                        switch (transition)
                        {
                            case Transition.Call call:
                                var parent = layer.AddNode(call.Next);
                                if (parent.AddChild(node))
                                    if (popped.Contains(parent))
                                        descriptors.Add((call.Next, node));
                                descriptors.Add((call.Child, parent));
                                break;
                            case Transition.Reduce reduce:
                                if (node == rootNode)
                                    accept = true;
                                if (popped.Add(node))
                                    foreach (var child in node.Children)
                                        descriptors.Add((node.Label, child));
                                break;
                        }
            return accept;
        }

        private void Shifter(Descriptors currentDescriptors, Descriptors nextDescriptors, Char terminal)
        {
            foreach (var (state, node) in currentDescriptors.All)
                foreach (var shift in state.Transitions.OfType<Transition.Shift>())
                    if (shift.Terminal == terminal)
                        nextDescriptors.Add((shift.Next, node));
        }

        public Boolean Run(IEnumerable<Char> input, Action step)
        {
            var layerIndex = 0;
            var currentDescriptors = new Descriptors();
            var specialState = new EpsilonState();
            var root = new GSSNode<IState>(specialState, 0);
            currentDescriptors.Add((Grammar.StartState, root));
            Boolean accept;
            foreach (var terminal in input)
            {
                CallerAndReducer(currentDescriptors, terminal, root, layerIndex);
                step();
                layerIndex += 1;
                var nextDescriptors = new Descriptors();
                Shifter(currentDescriptors, nextDescriptors, terminal);
                currentDescriptors = nextDescriptors;
            }
            accept = CallerAndReducer(currentDescriptors, null, root, layerIndex);
            step();
            return accept;
        }

        private sealed class EpsilonState : IState
        {
            public IEnumerable<Transition> Transitions =>
                Enumerable.Empty<Transition>();

            public String Name =>
                "$\\epsilon$";

            public Boolean Equals(IState other) =>
                ReferenceEquals(this, other);
        }
    }
}

using System;
using System.Collections.Generic;

namespace Parser.GLL
{
    internal class WorkingSet<T>
        where T : IEquatable<T>
    {
        private readonly HashSet<T> Added =
            new HashSet<T>();
        private readonly Queue<T> Queue =
            new Queue<T>();

        public Boolean Add(T item)
        {
            if (Added.Add(item))
            {
                Queue.Enqueue(item);
                return true;
            }
            return false;
        }

        public IEnumerable<T> Remaining
        {
            get
            {
                while (Queue.Count > 0)
                    yield return Queue.Dequeue();
            }
        }

        public IEnumerable<T> All =>
            Added;
    }
}

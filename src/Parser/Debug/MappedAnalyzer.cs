using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Debug
{
    public class MappedAnalyzer<T> : IAnalyzer<T>
    {
        private readonly IAnalyzer<T> InnerAnalyzer;
        private readonly Func<IAnalyzable, IAnalyzable> Map;

        public MappedAnalyzer(IAnalyzer<T> innerAnalyzer, Func<IAnalyzable, IAnalyzable> map)
        {
            InnerAnalyzer = innerAnalyzer;
            Map = map;
        }

        private IAnalyzable Loopback(IAnalyzable analyzable)
            => (analyzable is IAnalyzable<T> a && a.Analyzer.Equals(InnerAnalyzer)) ? Analyzable.Create(this, a.Value) : analyzable;

        IEqualityComparer<T> IAnalyzer<T>.Comparer
            => InnerAnalyzer.Comparer;

        IEnumerable<IAnalyzable> IAnalyzer<T>.Constituents(T value)
            => InnerAnalyzer.Constituents(value)?.Select(Map).Select(Loopback);
    }

    public static class MappedAnalyzer
    {
        public static IAnalyzer<T> Map<T>(this IAnalyzer<T> analyzer, Func<IAnalyzable, IAnalyzable> map)
            => new MappedAnalyzer<T>(analyzer, map);
    }
}

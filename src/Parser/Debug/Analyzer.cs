using System;
using System.Collections.Generic;

namespace Parser.Debug
{
    public interface IAnalyzer<T>
    {
        IEqualityComparer<T> Comparer { get; }
        IEnumerable<IAnalyzable> Constituents(T value);
    }

    public interface IForwardingAnalyzer<S> : IAnalyzer<S>
    {
        TResult Accept<TResult>(IForwardingAnalyzerVisitor<S, TResult> visitor);
    }

    public interface IForwardingAnalyzerVisitor<S, out TResult>
    {
        TResult Visit<T>(IForwardingAnalyzer<S, T> analyzer);
    }

    public interface IForwardingAnalyzer<S, T> : IForwardingAnalyzer<S>
    {
        Func<S, T> Forward { get; }
        IAnalyzer<T> TargetAnalyzer { get; }
    }

    public class LeafAnalyzer<T> : IAnalyzer<T>
    {
        public IEqualityComparer<T> Comparer
            => EqualityComparer<T>.Default;

        public IEnumerable<IAnalyzable> Constituents(T value)
            => null;

        public static LeafAnalyzer<T> Instance
            = new LeafAnalyzer<T>();
    }
}

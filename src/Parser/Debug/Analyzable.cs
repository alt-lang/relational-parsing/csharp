using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Debug
{
    using static EqualityComparerFactories;

    public interface IAnalyzable : IEquatable<IAnalyzable>
    {
        IEnumerable<IAnalyzable> Constituents { get; }
        TResult Accept<TResult>(IAnalyzableVisitor<TResult> visitor);
    }

    public interface IAnalyzableVisitor<out TResult>
    {
          TResult Visit<T>(IAnalyzable<T> analyzable);
    }

    public interface IAnalyzable<T> : IAnalyzable
    {
        IAnalyzer<T> Analyzer { get; }
        T Value { get; }
    }

    public class Analyzable<T> : IAnalyzable<T>
    {
        public IAnalyzer<T> Analyzer { get; }
        public T Value { get; }

        public Analyzable(IAnalyzer<T> analyzer, T value)
        {
            Analyzer = analyzer;
            Value = value;
        }

        public IEnumerable<IAnalyzable> Constituents
            => Analyzer.Constituents(Value);

        public Boolean Equals(IAnalyzable other)
            => (other is Analyzable<T> analyzable) &&
                (Analyzer.Equals(analyzable.Analyzer) || analyzable.Analyzer.Equals(Analyzer)) &&
                Analyzer.Comparer.Equals(Value, analyzable.Value);

        public override Boolean Equals(object obj)
            => (obj is IAnalyzable analyzable) && Equals(analyzable);

        public override Int32 GetHashCode()
            => Analyzer.Comparer.GetHashCode(Value);

        public override String ToString()
            => Value.ToString();

        TResult IAnalyzable.Accept<TResult>(IAnalyzableVisitor<TResult> visitor)
            => visitor.Visit(this);
    }

    public static class Analyzable
    {
        public static IAnalyzable<T> Create<T>(IAnalyzer<T> analyzer, T value)
            => new Analyzable<T>(analyzer, value);

        public static IAnalyzable<T> CreateLeaf<T>(T value)
            => Create(LeafAnalyzer<T>.Instance, value);

        public static void Forward(ref IAnalyzable analyzable)
        {
            IAnalyzable previous = null;
            while (analyzable != previous)
            {
                previous = analyzable;
                analyzable = previous.Accept(new ForwardRequest());
            }
        }

        private class ForwardRequest : IAnalyzableVisitor<IAnalyzable>
        {
            public IAnalyzable Visit<S>(IAnalyzable<S> analyzable)
                => (analyzable.Analyzer is IForwardingAnalyzer<S> forwarding) ? forwarding.Accept(new ForwardRequest<S> { Source = analyzable }) : analyzable;
        }

        private class ForwardRequest<S> : IForwardingAnalyzerVisitor<S, IAnalyzable>
        {
            public IAnalyzable<S> Source;

            public IAnalyzable Visit<T>(IForwardingAnalyzer<S, T> analyzer)
                => Analyzable.Create(analyzer.TargetAnalyzer, analyzer.Forward(Source.Value));
        }

        public class Difference
        {
            public IAnalyzable Analyzable1 { get; }
            public IAnalyzable Analyzable2 { get; }
            public Difference Parent { get; }

            public Difference(IAnalyzable analyzable1, IAnalyzable analyzable2, Difference parent = null)
            {
                Analyzable1 = analyzable1;
                Analyzable2 = analyzable2;
                Parent = parent;
            }
        }

        private static IEqualityComparer<Difference> DifferenceEC
            = new ComponentsEqualityComparer<Difference>
            {
                { _ => _.Analyzable1, EquatableEC<IAnalyzable>() },
                { _ => _.Analyzable2, EquatableEC<IAnalyzable>() },
            };

        public static IEnumerable<Difference> FindDifferences(IAnalyzable analyzable1, IAnalyzable analyzable2)
        {
            var seen = new HashSet<Difference>(DifferenceEC);
            var queue = new Queue<Difference>();
            var root = new Difference(analyzable1, analyzable2);
            if (seen.Add(root))
                queue.Enqueue(root);
            while (queue.Count > 0)
            {
                var potential = queue.Dequeue();
                var component1 = potential.Analyzable1;
                Analyzable.Forward(ref component1);
                var component2 = potential.Analyzable2;
                Analyzable.Forward(ref component2);
                if (component1.Equals(component2))
                    continue;
                var constituents1 = component1.Constituents;
                var constituents2 = component2.Constituents;
                if (constituents1 == null || constituents2 == null)
                {
                    yield return potential;
                    continue;
                }
                using (var enumerator1 = constituents1.GetEnumerator())
                using (var enumerator2 = constituents2.GetEnumerator())
                while (true)
                {
                    var next1 = enumerator1.MoveNext();
                    var next2 = enumerator2.MoveNext();
                    if (next1 != next2)
                        yield return potential;
                    if (!next1 || !next2)
                        break;
                    var next = new Difference(enumerator1.Current, enumerator2.Current, potential);
                    if (seen.Add(next))
                        queue.Enqueue(next);
                }
            }
        }
    }
}

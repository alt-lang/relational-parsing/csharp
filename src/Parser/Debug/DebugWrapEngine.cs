using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Debug
{
    using Algebra;
    using Caching;
    using Relational;
    using static EqualityComparerFactories;

    public class DebugWrapEngine<TValue, TAtom, TWrap> : WrapEngine<TValue, TAtom, DebugWrapEngine<TValue, TAtom, TWrap>.Node>, IAnalyzer<DebugWrapEngine<TValue, TAtom, TWrap>.Node>
    {
        public class Node
        {
            private static Int32 NextId = 0;
            public readonly Int32 Id = NextId++;
            public readonly TWrap Inner;
            public readonly String Operation;
            public readonly IReadOnlyList<Node> Arguments;

            public Node(TWrap inner, String operation, params Node[] arguments)
            {
                Inner = inner;
                Operation = operation;
                Arguments = arguments;
            }

            public override String ToString()
                => $"{Inner} (%{Id} = {Operation}{String.Join(", ", Arguments.Select(_ => $"%{_.Id}"))})";
        }

        private readonly IWrapEngine<TValue, TAtom, TWrap> InnerEngine;
        public override IEqualityComparer<Node> Comparer { get; }
        private readonly IEqualityComparer<Node> ValueComparer;
        private static readonly ICacheStrategy NodeCacheStrategy
            = new PermanentCacheStrategy();
        private readonly ICache<Node> NodeCache;
        IEqualityComparer<Node> IAnalyzer<Node>.Comparer
            => ValueComparer;
        public override ISemiring<TValue> Semiring
            => InnerEngine.Semiring;

        public DebugWrapEngine(IWrapEngine<TValue, TAtom, TWrap> innerEngine)
        {
            InnerEngine = innerEngine;
            Comparer = IdentityEC<Node>();
            ValueComparer = new ComponentsEqualityComparer<Node>
            {
                { _ => _.Inner, InnerEngine.Comparer },
            };
            NodeCache = NodeCacheStrategy.Create(new ComponentsEqualityComparer<Node>
            {
                { _ => _.Inner, InnerEngine.Comparer },
                { _ => _.Operation, OperatorEC<String>() },
                { _ => _.Arguments, SequenceEC(Comparer) },
            });
        }

        public override Node Add(IEnumerable<Node> values)
        {
            var arguments = values.ToArray();
            return NodeCache.Add(new Node(InnerEngine.Add(arguments.Select(_ => _.Inner)), "Add", arguments));
        }

        public override Node Atom(TAtom atom)
            => NodeCache.Add(new Node(InnerEngine.Atom(atom), "Atom"));

        public override TValue Flatten(Node wrap)
            => InnerEngine.Flatten(wrap.Inner);

        public override Node Fold(Node wrap)
            => NodeCache.Add(new Node(InnerEngine.Fold(wrap.Inner), "Fold", wrap));

        public override Node Multiply(IEnumerable<Node> values)
        {
            var arguments = values.ToArray();
            return NodeCache.Add(new Node(InnerEngine.Multiply(arguments.Select(_ => _.Inner)), "Multiply", arguments));
        }

        public IEnumerable<IAnalyzable> Constituents(Node value)
        {
            yield return Analyzable.CreateLeaf(value.Operation);
            foreach (var argument in value.Arguments)
                if (value.Operation == "Multiply" && argument.Operation == "Multiply")
                    foreach (var leaf in Constituents(argument).Skip(1))
                        yield return leaf;
                else
                    yield return Analyzable.Create(this, argument);
        }

        public override Boolean Equals(object obj)
            => (obj is DebugWrapEngine<TValue, TAtom, TWrap> other) && InnerEngine.Equals(other.InnerEngine);

        public override Int32 GetHashCode()
            => InnerEngine.GetHashCode();
    }

    public class DebugWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IWrapEngineFactory InnerFactory;

        public DebugWrapEngineFactory(IWrapEngineFactory innerFactory)
        {
            InnerFactory = innerFactory;
        }

        public IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => InnerFactory.Create(atomEngine).Accept(new CreateRequest<TValue, TAtom>());

        private class CreateRequest<TValue, TAtom> : IWrapEngineVisitor<TAtom, IWrapEngine<TAtom>>
        {
            public IWrapEngine<TAtom> Visit<TValue1, TWrap>(IWrapEngine<TValue1, TAtom, TWrap> engine)
                => new DebugWrapEngine<TValue1, TAtom, TWrap>(engine);
        }
    }
}

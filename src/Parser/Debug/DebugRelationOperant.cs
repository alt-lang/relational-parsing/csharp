﻿using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Debug
{
    using Relational;
    using static EqualityComparerFactories;

    public class DebugRelationOperant<TValue, TAtom, TRelation1, TRelation2> : IRelationOperant<TValue, TAtom, (TRelation1, TRelation2)>
    {
        private readonly IRelationOperant<TValue, TAtom, TRelation1> Operant1;
        private readonly IAnalyzer<TRelation1> Analyzer1;
        private readonly IRelationOperant<TValue, TAtom, TRelation2> Operant2;
        private readonly IAnalyzer<TRelation2> Analyzer2;

        public DebugRelationOperant(IRelationOperant<TValue, TAtom, TRelation1> operant1, IRelationOperant<TValue, TAtom, TRelation2> operant2)
        {
            Operant1 = operant1;
            Analyzer1 = (IAnalyzer<TRelation1>) operant1;
            Operant2 = operant2;
            Analyzer2 = (IAnalyzer<TRelation2>) operant2;
        }

        public IEqualityComparer<(TRelation1, TRelation2)> Comparer
            => ValueTupleEC(Operant1.Comparer, Operant2.Comparer);

        public (TRelation1, TRelation2) One
            => (Operant1.One, Operant2.One);

        public TValue FlatEpsilon((TRelation1, TRelation2) relation)
            => Operant1.FlatEpsilon(relation.Item1);

        public void Perform(IRelationalOperation<TAtom> operation, ref (TRelation1, TRelation2) relation)
        {
            Operant1.Perform(operation, ref relation.Item1);
            Operant2.Perform(operation, ref relation.Item2);
            var analyzable1 = Analyzable.Create(Analyzer1, relation.Item1);
            var analyzable2 = Analyzable.Create(Analyzer2, relation.Item2);
            foreach (var reason in Analyzable.FindDifferences(analyzable1, analyzable2))
                for (var difference = reason; difference != null; difference = difference.Parent)
                    Console.Error.WriteLine($"{difference.Analyzable1} != {difference.Analyzable2}");
        }

        public TResult Accept<TResult>(IRelationOperantVisitor<TValue, TAtom, TResult> visitor)
            => visitor.Visit(this);
    }

    public class DebugRelationOperantFactory : IRelationOperantFactory
    {
        private readonly IRelationOperantFactory Factory1;
        private readonly IRelationOperantFactory Factory2;

        public DebugRelationOperantFactory(IRelationOperantFactory factory1, IRelationOperantFactory factory2)
        {
            Factory1 = factory1;
            Factory2 = factory2;
        }

        public IRelationOperant<TValue, TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => Factory2.Create(atomEngine).Accept(Factory1.Create(atomEngine).Accept(new CreateRequest<TValue, TAtom>()));

        private class CreateRequest<TValue, TAtom> : IRelationOperantVisitor<TValue, TAtom, IRelationOperantVisitor<TValue, TAtom, IRelationOperant<TValue, TAtom>>>
        {
            public IRelationOperantVisitor<TValue, TAtom, IRelationOperant<TValue, TAtom>> Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
                => new CreateRequest<TValue, TAtom, TRelation> { Operant1 = operant };
        }

        private class CreateRequest<TValue, TAtom, TRelation1> : IRelationOperantVisitor<TValue, TAtom, IRelationOperant<TValue, TAtom>>
        {
            public IRelationOperant<TValue, TAtom, TRelation1> Operant1;

            public IRelationOperant<TValue, TAtom> Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
                => new DebugRelationOperant<TValue, TAtom, TRelation1, TRelation>(Operant1, operant);
        }
    }
}

using System;
using System.Collections.Generic;

namespace Parser.GLR
{
    public interface IConfigurationEngine
    {
        GrammarAnalysis Analysis { get; }
        TResult Accept<TResult>(IConfigurationEngineVisitor<TResult> visitor);
    }

    public interface IConfigurationEngineVisitor<out TResult>
    {
        TResult Visit<TConfiguration>(IConfigurationEngine<TConfiguration> engine)
            where TConfiguration : IEquatable<TConfiguration>;
    }

    public interface IConfigurationEngine<TConfiguration> : IConfigurationEngine
        where TConfiguration : IEquatable<TConfiguration>
    {
        Boolean IsAlive(TConfiguration configuration);
        TConfiguration Atomic(IState start);
        TConfiguration Call(TConfiguration configuration, IState child);
        TConfiguration Shift(TConfiguration configuration, Char terminal);
        IEnumerable<IState> Reduce(TConfiguration configuration, Char? lookahead);
        IEnumerable<TConfiguration> Successors(TConfiguration configuration);
    }
}

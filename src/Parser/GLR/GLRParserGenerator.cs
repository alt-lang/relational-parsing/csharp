using System;

namespace Parser.GLR
{
    public class GLRParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.Recognition;

        public Boolean UseLookahead { get; }
        public Boolean UseMemoization { get; }
        public Boolean Precompute { get; }

        public GLRParserGenerator(Boolean useLookahead, Boolean useMemoization = true, Boolean precompute = true)
        {
            UseLookahead = useLookahead;
            UseMemoization = useMemoization;
            Precompute = precompute;
        }

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar)
        {
            var analysis = new GrammarAnalysis(grammar);
            var engine = UseLookahead ? new LR1ConfigurationEngine(analysis) : new LR0ConfigurationEngine(analysis) as IConfigurationEngine;
            if (UseMemoization)
                engine = MemoizingConfigurationEngine.Create(engine);
            return engine.Accept(new CreateVisitor { Precompute = Precompute });
        }

        private class CreateVisitor : IConfigurationEngineVisitor<IParser<Boolean>>
        {
            public Boolean Precompute { get; set; }

            public IParser<Boolean> Visit<TConfiguration>(IConfigurationEngine<TConfiguration> engine)
                where TConfiguration : IEquatable<TConfiguration> =>
                new GLRParser<TConfiguration>(engine, Precompute);
        }

        public override String ToString()
            => $"{nameof(GLRParserGenerator)}({UseLookahead},{UseMemoization},{Precompute})";
    }
}

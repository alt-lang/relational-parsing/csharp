using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.GLR
{
    internal class LR0ConfigurationEngine : IConfigurationEngine<ClosedSet<IState>>
    {
        public GrammarAnalysis Analysis { get; }

        public LR0ConfigurationEngine(GrammarAnalysis analysis)
        {
            Analysis = analysis;
        }

        public Boolean IsAlive(ClosedSet<IState> configuration) =>
            configuration.Count > 0;

        public ClosedSet<IState> Atomic(IState start) =>
            new ClosedSet<IState>(new[] { start }, Closure);

        public ClosedSet<IState> Call(ClosedSet<IState> configuration, IState child) =>
            new ClosedSet<IState>(
                from state in configuration
                from call in state.Transitions.OfType<Transition.Call>()
                where call.Child == child
                select call.Next,
                Closure);

        public IEnumerable<IState> Reduce(ClosedSet<IState> configuration, Char? lookahead) =>
            new HashSet<IState>(from state in configuration where Analysis.IsNullable[state] select state);

        public ClosedSet<IState> Shift(ClosedSet<IState> configuration, Char terminal) =>
            new ClosedSet<IState>(
                from state in configuration
                from shift in state.Transitions.OfType<Transition.Shift>()
                where shift.Terminal == terminal
                select shift.Next,
                Closure);

        public IEnumerable<ClosedSet<IState>> Successors(ClosedSet<IState> configuration)
        {
            foreach (var state in configuration)
                foreach (var transition in state.Transitions)
                    switch (transition)
                    {
                        case Transition.Shift shift:
                            yield return Shift(configuration, shift.Terminal);
                            break;
                        case Transition.Call call:
                            yield return Call(configuration, call.Child);
                            break;
                    }
        }

        private IEnumerable<IState> Closure(IState state) =>
            state.Transitions.OfType<Transition.Call>().Select(call => call.Child);

        TResult IConfigurationEngine.Accept<TResult>(IConfigurationEngineVisitor<TResult> visitor) =>
            visitor.Visit(this);
    }
}

using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Debug;

    partial class CommutativeWrapEngine<TValue> : IAnalyzer<TValue>
    {
        IEnumerable<IAnalyzable> IAnalyzer<TValue>.Constituents(TValue value)
            => null;

        public override Boolean Equals(object obj)
            => obj is CommutativeWrapEngine<TValue>;

        public override Int32 GetHashCode()
            => typeof(CommutativeWrapEngine<TValue>).GetHashCode();
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Utilities;

    public sealed class RelationalInitialization<TAtom> : IRelationalOperation<TAtom>
    {
        private readonly GrammarAnalysis Analysis;
        private readonly IAtomicRelations<TAtom, IState> AtomicRelations;

        public RelationalInitialization(GrammarAnalysis analysis, IAtomicRelations<TAtom, IState> atomicRelations)
        {
            Analysis = analysis;
            AtomicRelations = atomicRelations;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            relation = engine.Multiply(AtomicRelations.StateRelations[Analysis.Grammar.StopState], relation);
            relation = engine.Multiply(AtomicRelations.StartRelation, relation);
            return relation;
        }

        public Boolean Equals(IRelationalOperation<TAtom> other) =>
            other is RelationalInitialization<TAtom>;

        public override Int32 GetHashCode() =>
            -1;
    }

    public sealed class RelationalPhase<TAtom> : IRelationalOperation<TAtom>
    {
        private readonly Boolean UseLookahead;
        private readonly GrammarAnalysis Analysis;
        private readonly IAtomicRelations<TAtom, IState> AtomicRelations;
        private readonly Char Symbol;

        public RelationalPhase(Boolean useLookahead, GrammarAnalysis analysis, IAtomicRelations<TAtom, IState> atomicRelations, Char symbol)
        {
            UseLookahead = useLookahead;
            Analysis = analysis;
            AtomicRelations = atomicRelations;
            Symbol = symbol;
        }

        private static Boolean NoFilter(IState state)
            => true;

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            var partial = new Dictionary<IState, List<TRelation>>();
            var nulled = new List<TRelation>();
            Func<IState, Boolean> filter = NoFilter;
            if (UseLookahead)
            {
                HashSet<IState> origins;
                if (!Analysis.ShiftOrigins.TryGetValue(Symbol, out origins))
                    return engine.Zero;
                filter = origins.Contains;
            }
            foreach (var (state0, relation0) in engine.Derivatives(relation, filter))
                foreach (var transition in state0.Transitions)
                    if (transition is Transition.Shift shift && shift.Terminal == Symbol)
                    {
                        var tail = engine.Multiply(AtomicRelations.ShiftAtoms[shift], relation0);
                        if (Analysis.ProductiveStates.Contains(shift.Next))
                            partial.GetOrCreate(shift.Next).Add(tail);
                        if (AtomicRelations.NullingAtoms.TryGetValue(shift.Next, out var nulling))
                            nulled.Add(engine.Multiply(nulling, tail));
                    }
            if (nulled.Count > 0)
                foreach (var (state1, relation1) in engine.Derivatives(engine.Add(nulled), NoFilter))
                    partial.GetOrCreate(state1).Add(relation1);
            var currents = new List<TRelation>();
            foreach (var (state, relations) in partial)
                currents.Add(engine.Multiply(AtomicRelations.StateRelations[state],engine.Add(relations)));
            return engine.Add(currents);
        }

        public Boolean Equals(IRelationalOperation<TAtom> other) =>
            other is RelationalPhase<TAtom> phase && phase.Symbol == Symbol;

        public override Int32 GetHashCode() =>
            Symbol.GetHashCode();
    }

    public sealed class RelationalCompletion<TAtom> : IRelationalOperation<TAtom>
    {
        private readonly GrammarAnalysis Analysis;

        public RelationalCompletion(GrammarAnalysis analysis)
        {
            Analysis = analysis;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation)
            => engine.Add(engine.Derivatives(relation, state => state == Analysis.Grammar.StopState).Select(_ => _.Item2));

        public Boolean Equals(IRelationalOperation<TAtom> other) =>
            other is RelationalCompletion<TAtom>;

        public override Int32 GetHashCode() =>
            -2;
    }

    public class RelationalParser<TValue, TAtom, TRelation> : IParser<TValue>
    {
        public IValuationSemiring<TValue> Semiring { get; }
        private readonly GrammarAnalysis Analysis;
        private readonly IAtomicRelations<TAtom, IState> AtomicRelations;
        private readonly IRelationOperant<TValue, TAtom, TRelation> RelationOperant;
        private readonly Boolean UseLookahead;

        public RelationalParser(IValuationSemiring<TValue> semiring, GrammarAnalysis analysis, IAtomicRelations<TAtom, IState> atomicRelations, IRelationOperant<TValue, TAtom, TRelation> relationOperant, Boolean useLookahead)
        {
            Semiring = semiring;
            Analysis = analysis;
            AtomicRelations = atomicRelations;
            RelationOperant = relationOperant;
            UseLookahead = useLookahead;
        }

        public TValue Run(IEnumerable<Char> input, Action step)
        {
            TRelation current = RelationOperant.One;
            RelationOperant.Perform(new RelationalInitialization<TAtom>(Analysis, AtomicRelations), ref current);
            step();
            foreach (var symbol in input)
            {
                RelationOperant.Perform(new RelationalPhase<TAtom>(UseLookahead, Analysis, AtomicRelations, symbol), ref current);
                step();
            }
            RelationOperant.Perform(new RelationalCompletion<TAtom>(Analysis), ref current);
            return RelationOperant.FlatEpsilon(current);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Graphs;
    using Utilities;
    using static EqualityComparerFactories;

    public struct DagWrapEdge<TValue, TAtom>
    {
        public readonly Int32 AtomCount;
        private readonly Object ValueOrAtom;

        public DagWrapEdge(TValue value)
        {
            AtomCount = 0;
            ValueOrAtom = value;
        }

        public DagWrapEdge(TAtom atom, Int32 count)
        {
            AtomCount = count;
            ValueOrAtom = atom;
        }

        public TValue Value
            => (TValue) ValueOrAtom;

        public TAtom Atom
            => (TAtom) ValueOrAtom;

        public static IEqualityComparer<DagWrapEdge<TValue, TAtom>> EdgeEC(IAtomEngine<TValue, TAtom> engine)
            => new VariantsEqualityComparer<DagWrapEdge<TValue, TAtom>, Boolean>(_ => _.AtomCount == 0, OperatorEC<Boolean>())
            {
                { true, _ => _.Value, engine.Semiring.Comparer },
                { false, _ => _, new ComponentsEqualityComparer<DagWrapEdge<TValue, TAtom>>
                    {
                        { _ => _.Atom, engine.Comparer },
                        { _ => _.AtomCount, OperatorEC<Int32>() },
                    }
                },
            };

        public static IEqualityComparer<DagWrapEdge<TValue, TAtom>> MergeEC(IAtomEngine<TValue, TAtom> engine)
            => new VariantsEqualityComparer<DagWrapEdge<TValue, TAtom>, Boolean>(_ => _.AtomCount == 0, OperatorEC<Boolean>())
            {
                { true, _ => _, new ComponentsEqualityComparer<DagWrapEdge<TValue, TAtom>>() },
                { false, _ => _.Atom, engine.Comparer },
            };

        public static Func<DagWrapEdge<TValue, TAtom>, DagWrapEdge<TValue, TAtom>, DagWrapEdge<TValue, TAtom>> Merge(IAtomEngine<TValue, TAtom> engine)
            => (one, two) =>
            {
                if (one.AtomCount == 0)
                    return new DagWrapEdge<TValue, TAtom>(engine.Semiring.Add(one.Value, two.Value));
                if (engine.IsIdempotent)
                    return one;
                return new DagWrapEdge<TValue, TAtom>(one.Atom, one.AtomCount + two.AtomCount);
            };
    }

    public class DagWrapEngine<TValue, TAtom, TNode> : WrapEngine<TValue, TAtom, TNode>
    {
        private readonly IAtomEngine<TValue, TAtom> AtomEngine;
        private readonly IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> GraphEngine;

        public DagWrapEngine(IAtomEngine<TValue, TAtom> atomEngine, IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> graphEngine)
        {
            AtomEngine = atomEngine;
            GraphEngine = graphEngine;
            Root = GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(AtomEngine.Semiring.Zero), GraphEngine.Zero));
            Zero = GraphEngine.Zero;
            One = GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(AtomEngine.Semiring.One), Root));
        }

        public override ISemiring<TValue> Semiring
            => AtomEngine.Semiring;

        public override Boolean IsIdempotent
            => AtomEngine.IsIdempotent;

        private readonly TNode Root;

        public override IEqualityComparer<TNode> Comparer
            => GraphEngine.Comparer;

        public override TNode Add(IEnumerable<TNode> wraps)
            => GraphEngine.Add(wraps);

        public override TNode Zero { get; }

        public override Boolean IsZero(TNode value)
            => GraphEngine.IsZero(value);

        public override TNode One { get; }

        public override TNode Atom(TAtom atom)
            => AtomEngine.IsOne(atom) ? One : GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(atom, 1), One));

        private readonly Counter MultiplyDfsCalls = new Counter();

        public override TNode Multiply(TNode inner, TNode outer)
        {
            if (IsZero(inner) || IsOne(outer))
                return inner;
            if (IsOne(inner))
                return outer;
            var results = new Dictionary<TNode, TNode>(Comparer) { { Root, outer } };
            TNode Dfs(TNode node)
            {
                MultiplyDfsCalls.Increment();
                TNode result;
                if (!results.TryGetValue(node, out result))
                {
                    result = GraphEngine.Create(GraphEngine.Neighbors(node).Select(_ => (_.Item1, Dfs(_.Item2))));
                    results.Add(node, result);
                }
                return result;
            }
            return Dfs(inner);
        }

        private readonly Counter FlattenDfsCalls = new Counter();

        public override TValue Flatten(TNode wrap)
        {
            var visited = new HashSet<TNode>(Comparer);
            var topological = new List<TNode>();
            void Dfs(TNode node)
            {
                FlattenDfsCalls.Increment();
                if (visited.Add(node))
                {
                    foreach (var (_, neighbor) in GraphEngine.Neighbors(node))
                        Dfs(neighbor);
                    topological.Add(node);
                }
            }
            Dfs(wrap);
            if (!visited.Contains(Root))
            {
                return Semiring.Zero;
            }
            topological.Reverse();
            var values = new Dictionary<TNode, List<TValue>>(Comparer);
            values.GetOrCreate(wrap).Add(Semiring.One);
            foreach (var node in topological)
            {
                var union = Semiring.Add(values[node]);
                if (Comparer.Equals(node, Root))
                    return union;
                foreach (var (edge, neighbor) in GraphEngine.Neighbors(node))
                    values.GetOrCreate(neighbor).Add(edge.AtomCount == 0 ? Semiring.Multiply(union, edge.Value) : Semiring.Multiply(edge.AtomCount, AtomEngine.Wrap(union, edge.Atom)));
            }
            throw new InvalidProgramException();
        }

        public override TNode Fold(TNode wrap)
            => IsZero(wrap) || IsOne(wrap) ? wrap : GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(Flatten(wrap)), One));
    }

    public class DagWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IGraphEngineFactory GraphEngineFactory;

        public DagWrapEngineFactory(IGraphEngineFactory graphEngineFactory)
        {
            GraphEngineFactory = graphEngineFactory;
        }

        public IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => GraphEngineFactory
                .Create(
                    DagWrapEdge<TValue, TAtom>.EdgeEC(atomEngine),
                    DagWrapEdge<TValue, TAtom>.MergeEC(atomEngine),
                    DagWrapEdge<TValue, TAtom>.Merge(atomEngine),
                    atomEngine.IsIdempotent)
                .Accept(new CreateRequest<TValue, TAtom> { AtomEngine = atomEngine });

        private class CreateRequest<TValue, TAtom> : IGraphEngineVisitor<DagWrapEdge<TValue, TAtom>, IWrapEngine<TAtom>>
        {
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }

            public IWrapEngine<TAtom> Visit<TNode>(IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> engine)
                => new DagWrapEngine<TValue, TAtom, TNode>(AtomEngine, engine);
        }

        public override String ToString()
            => $"{nameof(DagWrapEngineFactory)}({GraphEngineFactory})";
    }

    public class MemoizingDagWrapEngine<TValue, TAtom, TNode> : DagWrapEngine<TValue, TAtom, TNode>
    {
        private readonly Dictionary<TNode, TValue> FlattenCache;
        private readonly Dictionary<TNode, TNode> FoldCache;

        public MemoizingDagWrapEngine(IAtomEngine<TValue, TAtom> atomEngine, IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> graphEngine)
            : base(atomEngine, graphEngine)
        {
            FlattenCache = new Dictionary<TNode, TValue>(Comparer);
            FoldCache = new Dictionary<TNode, TNode>(Comparer);
        }

        public override TValue Flatten(TNode wrap)
        {
            TValue result;
            if (!FlattenCache.TryGetValue(wrap, out result))
            {
                result = base.Flatten(wrap);
                FlattenCache.Add(wrap, result);
            }
            return result;
        }

        public override TNode Fold(TNode wrap)
        {
            TNode result;
            if (!FoldCache.TryGetValue(wrap, out result))
            {
                result = base.Fold(wrap);
                FoldCache.Add(wrap, result);
            }
            return result;
        }
    }

    public class MemoizingDagWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IGraphEngineFactory GraphEngineFactory;

        public MemoizingDagWrapEngineFactory(IGraphEngineFactory graphEngineFactory)
        {
            GraphEngineFactory = graphEngineFactory;
        }

        public IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => GraphEngineFactory
                .Create(
                    DagWrapEdge<TValue, TAtom>.EdgeEC(atomEngine),
                    DagWrapEdge<TValue, TAtom>.MergeEC(atomEngine),
                    DagWrapEdge<TValue, TAtom>.Merge(atomEngine),
                    atomEngine.IsIdempotent)
                .Accept(new CreateRequest<TValue, TAtom> { AtomEngine = atomEngine });

        private class CreateRequest<TValue, TAtom> : IGraphEngineVisitor<DagWrapEdge<TValue, TAtom>, IWrapEngine<TAtom>>
        {
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }

            public IWrapEngine<TAtom> Visit<TNode>(IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> engine)
                => new MemoizingDagWrapEngine<TValue, TAtom, TNode>(AtomEngine, engine);
        }

        public override String ToString()
            => $"{nameof(MemoizingDagWrapEngineFactory)}({GraphEngineFactory})";
    }
}

using System;

namespace Parser.Relational
{
    public interface ISubstitutionAdjustment<TWrap> : IEquatable<ISubstitutionAdjustment<TWrap>>
    {
        TWrap Apply(TWrap wrap);
    }

    public class SubstitutionAdjustment<TAtom, TWrap> : ISubstitutionAdjustment<TWrap>
    {
        private readonly ISubstitutionWrapEngine<TAtom, TWrap> SubstitutionWrapEngine;
        private readonly Int32 NewDepth;

        public SubstitutionAdjustment(ISubstitutionWrapEngine<TAtom, TWrap> substitutionWrapEngine, Int32 newDepth)
        {
            SubstitutionWrapEngine = substitutionWrapEngine;
            NewDepth = newDepth;
        }

        public TWrap Apply(TWrap wrap)
            => SubstitutionWrapEngine.SetDepth(wrap, NewDepth);

        public Boolean Equals(ISubstitutionAdjustment<TWrap> other)
            => (other is SubstitutionAdjustment<TAtom, TWrap> adjustment) && adjustment.NewDepth == NewDepth;

        public override Int32 GetHashCode()
            => NewDepth;
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Utilities;

    public class AtomicGraph
    {
        public abstract class Edge
        {
            public sealed class Nulling : Edge
            {
                public IState Prefix { get; }
                public IState Suffix { get; }

                public Nulling(IState prefix, IState suffix)
                {
                    Prefix = prefix;
                    Suffix = suffix;
                }
            }

            public sealed class Single : Edge
            {
                public Transition Transition { get; }

                public Single(Transition transition)
                {
                    Transition = transition;
                }
            }

            private Edge()
            {
            }
        }

        public class Node : Dictionary<Node, Edge> // holds successor nodes
        {
        }

        public Dictionary<Char, Node> TerminalNodes { get; }
        public Dictionary<IState, Node> StateNodes { get; }
        public Dictionary<Transition.Binary, Node> BeforeNodes { get; }
        public Dictionary<Transition.Binary, Node> AfterNodes { get; }
        public Dictionary<Node, (IState, Node)> Derivatives { get; }

        public AtomicGraph(GrammarAnalysis analysis)
        {
            TerminalNodes = new Dictionary<Char, Node>();
            StateNodes = new Dictionary<IState, Node>();
            BeforeNodes = new Dictionary<Transition.Binary, Node>();
            AfterNodes = new Dictionary<Transition.Binary, Node>();
            Derivatives = new Dictionary<Node, (IState, Node)>();
            foreach (var transition in analysis.AllTransitions)
                switch (transition)
                {
                    case Transition.Call call:
                    {
                        var source = StateNodes.GetOrCreate(call.Source);
                        var child = StateNodes.GetOrCreate(call.Child);
                        var next = StateNodes.GetOrCreate(call.Next);
                        var before = BeforeNodes.GetOrCreate(call);
                        var after = AfterNodes.GetOrCreate(call);
                        child.Add(before, new Edge.Nulling(null, null));
                        if (analysis.IsNullable[call.Child])
                            next.Add(after,new Edge.Nulling(null, call.Child));
                        if (analysis.IsNullable[call.Next])
                            before.Add(after, new Edge.Nulling(call.Next, null));
                        after.Add(source, new Edge.Single(call));
                        Derivatives.Add(before, (call.Next, after));
                        break;
                    }
                    case Transition.Shift shift:
                    {
                        var source = StateNodes.GetOrCreate(shift.Source);
                        var terminal = TerminalNodes.GetOrCreate(shift.Terminal);
                        var before = BeforeNodes.GetOrCreate(shift);
                        var after = AfterNodes.GetOrCreate(shift);
                        terminal.Add(before, new Edge.Nulling(null, null));
                        if (analysis.IsNullable[shift.Next])
                            before.Add(after, new Edge.Nulling(shift.Next, null));
                        after.Add(source, new Edge.Single(shift));
                        Derivatives.Add(before, (shift.Next, after));
                        break;
                    }
                }
        }
    }
}

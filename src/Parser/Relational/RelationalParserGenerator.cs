using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;

    public class RelationalParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.ArbitrarySemiring;

        private readonly IAtomEngineFactory AtomEngineFactory;
        private readonly IRelationOperantFactory RelationOperantFactory;
        private readonly Boolean UseLookahead;
        private readonly IReadOnlyCollection<AtomicOptimization> AtomicOptimizations;

        public RelationalParserGenerator(IAtomEngineFactory atomEngineFactory, IRelationOperantFactory relationOperantFactory, Boolean useLookahead, params AtomicOptimization[] atomicOptimizations)
        {
            AtomEngineFactory = atomEngineFactory;
            RelationOperantFactory = relationOperantFactory;
            UseLookahead = useLookahead;
            AtomicOptimizations = atomicOptimizations;
        }

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
            => AtomEngineFactory.Create(semiring).Accept(new CreateRequest<TValue>
            {
                Grammar = grammar,
                RelationOperantFactory = RelationOperantFactory,
                UseLookahead = UseLookahead,
                AtomicOptimizations = AtomicOptimizations,
            });

        private class CreateRequest<TValue> : IAtomEngineVisitor<TValue, IParser<TValue>>
        {
            public IGrammar Grammar { get; set; }
            public IRelationOperantFactory RelationOperantFactory { get; set; }
            public Boolean UseLookahead { get; set; }
            public IReadOnlyCollection<AtomicOptimization> AtomicOptimizations { get; set; }

            public IParser<TValue> Visit<TAtom>(IAtomEngine<TValue, TAtom> engine)
                => RelationOperantFactory.Create(engine).Accept(new CreateRequest<TValue, TAtom>
                {
                    Grammar = Grammar,
                    AtomEngine = engine,
                    UseLookahead = UseLookahead,
                    AtomicOptimizations = AtomicOptimizations,
                });
        }

        private class CreateRequest<TValue, TAtom> : IRelationOperantVisitor<TValue, TAtom, IParser<TValue>>
        {
            public IGrammar Grammar { get; set; }
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }
            public Boolean UseLookahead { get; set; }
            public IReadOnlyCollection<AtomicOptimization> AtomicOptimizations { get; set; }

            public IParser<TValue> Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
            {
                var analysis = new GrammarAnalysis<TValue>(Grammar, AtomEngine.Semiring);
                var relations = ComputeRelations(analysis, AtomEngine);
                IAtomicRelations<TAtom, IState> previous = null;
                while (relations != previous)
                {
                    previous = relations;
                    foreach (var optimization in AtomicOptimizations)
                        relations = optimization.Perform(relations, AtomEngine);
                }
                return new RelationalParser<TValue, TAtom, TRelation>(AtomEngine.Semiring, analysis, relations, operant, UseLookahead);
            }
        }

        private static IAtomicRelations<TAtom, IState> ComputeRelations<TValue, TAtom>(GrammarAnalysis<TValue> analysis, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var relations = new AtomicRelations<TAtom, IState>();
            var nullingAtoms = new Dictionary<IState, TAtom>();
            foreach (var (state, value) in analysis.NullingValues)
                nullingAtoms.Add(state, atomEngine.Embed(value));
            relations.NullingAtoms = nullingAtoms;
            var shiftAtoms = new Dictionary<Transition, TAtom>();
            foreach (var (transition, value) in analysis.ShiftValues)
                shiftAtoms.Add(transition, atomEngine.Embed(value));
            relations.ShiftAtoms = shiftAtoms;
            var values = new Dictionary<GrammarAnalysis<TValue>.EdgeRelation, TAtom>();
            var inProgress = new HashSet<GrammarAnalysis<TValue>.EdgeRelation>();
            TAtom Evaluate(GrammarAnalysis<TValue>.EdgeRelation relation)
            {
                TAtom result;
                if (!values.TryGetValue(relation, out result))
                {
                    if (!inProgress.Add(relation))
                        throw new NotImplementedException("Support for infinitely ambiguous grammars has not yet been implemented");
                    var items = relation.Recursive.Select(r => atomEngine.Multiply(Evaluate(r.Inner), (r.Prefix, r.Suffix))).ToList();
                    if (relation.HasBase)
                        items.Add(atomEngine.Embed((atomEngine.Semiring.One, relation.Base)));
                    result = atomEngine.Add(items);
                    values.Add(relation, result);
                }
                return result;
            }
            var noEdge = new GrammarAnalysis<TValue>.EdgeRelation();
            var startEpsilon = analysis.NullingValues.TryGetValue(analysis.Grammar.StartState, out var nulling) ? new GrammarAnalysis<TValue>.EdgeRelation(nulling) : noEdge;
            var startRelation = new AtomicRelation<TAtom>("(init)", Evaluate(startEpsilon));
            relations.StartRelation = startRelation;
            foreach (var state in analysis.ProductiveStates)
            {
                var byTarget = new Dictionary<IState, AtomicRelation<TAtom>>();
                var queue = new Queue<(IState, IAtomicRelation<TAtom>)>();
                foreach (var (target, edge) in analysis.NodeRelations[state])
                {
                    var final = new AtomicRelation<TAtom>($"({state}:{target})", Evaluate(edge));
                    byTarget.Add(target, final);
                    queue.Enqueue((target, final));
                }
                while (queue.Count > 0)
                {
                    var (target, current) = queue.Dequeue();
                    foreach (var move in analysis.ProductiveStates)
                        if (analysis.EdgeRelations.TryGetValue((move, target), out var edges))
                            foreach (var (source, edge) in edges)
                            {
                                AtomicRelation<TAtom> next;
                                if (!byTarget.TryGetValue(source, out next))
                                {
                                    next = new AtomicRelation<TAtom>($"({state}.{source})", Evaluate(noEdge));
                                    byTarget.Add(source, next);
                                    queue.Enqueue((source, next));
                                }
                                next.Derivatives.Add((move, current), Evaluate(edge));
                            }
                }
                var noTarget = new AtomicRelation<TAtom>($"({state}.$\\epsilon$)", Evaluate(noEdge));
                foreach (var (target, relation) in byTarget)
                    if (analysis.ProductiveStates.Contains(target))
                        noTarget.Derivatives.Add((target, relation), atomEngine.One);
                relations.StateRelations.Add(state, noTarget);
                if (state == analysis.Grammar.StartState)
                    foreach (var kvp in noTarget.Derivatives)
                        startRelation.Derivatives.Add(kvp.Key, kvp.Value);
            }
            return relations;
        }

        public override String ToString() =>
            $"{nameof(RelationalParserGenerator)}({AtomEngineFactory},{RelationOperantFactory},{UseLookahead}{String.Join("", AtomicOptimizations.Select(o => $",{o}"))})";
    }
}

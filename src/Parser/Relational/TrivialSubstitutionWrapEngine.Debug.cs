using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using Debug;

    public partial class TrivialSubstitutionWrapEngine<TValue, TAtom, TWrap> : IForwardingAnalyzer<SubstitutionWrap<TAtom>, TWrap>
    {
        public Func<SubstitutionWrap<TAtom>, TWrap> Forward
            => wrap => ((Inner) wrap).Value;
        public IAnalyzer<TWrap> TargetAnalyzer
            => (IAnalyzer<TWrap>) InnerEngine;
        TResult IForwardingAnalyzer<SubstitutionWrap<TAtom>>.Accept<TResult>(IForwardingAnalyzerVisitor<SubstitutionWrap<TAtom>, TResult> visitor)
            => visitor.Visit(this);

        IEnumerable<IAnalyzable> IAnalyzer<SubstitutionWrap<TAtom>>.Constituents(SubstitutionWrap<TAtom> value)
            => TargetAnalyzer.Constituents(Forward(value));
    }
}

﻿using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Algebra;

    public class CommutativeAtomEngine<TValue> : AtomEngine<TValue, TValue>
    {
        public override IValuationSemiring<TValue> Semiring { get; }

        public CommutativeAtomEngine(IValuationSemiring<TValue> semiring)
        {
            if (!semiring.IsCommutative)
                throw new ArgumentException("commutative semiring required", nameof(semiring));
            Semiring = semiring;
        }

        public override IEqualityComparer<TValue> Comparer
            => Semiring.Comparer;

        public override TValue Zero
            => Semiring.Zero;

        public override Boolean IsZero(TValue value)
            => Semiring.IsZero(value);

        public override TValue Add(TValue left, TValue right)
            => Semiring.Add(left, right);

        public override TValue Add(IEnumerable<TValue> values)
            => Semiring.Add(values);

        public override TValue One
            => Semiring.One;

        public override Boolean IsOne(TValue value)
            => Semiring.IsOne(value);

        public override Boolean IsIdempotent
            => Semiring.IsIdempotent;

        public override TValue Multiply(TValue left, (TValue, TValue) right)
            => Semiring.Multiply(right.Item1, left, right.Item2);

        public override TValue Wrap(TValue value, TValue atom)
            => Semiring.Multiply(value, atom);

        public override TValue Embed(TValue value)
            => value;
    }

    public class CommutativeAtomEngineFactory : IAtomEngineFactory
    {
        private readonly IAtomEngineFactory Fallback;

        public CommutativeAtomEngineFactory(IAtomEngineFactory fallback)
        {
            Fallback = fallback;
        }

        public IAtomEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring)
            => semiring.IsCommutative ? new CommutativeAtomEngine<TValue>(semiring) : Fallback.Create(semiring);
    }
}

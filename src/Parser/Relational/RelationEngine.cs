﻿using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Algebra;

    public interface IRelationEngineFactory
    {
        IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine);
    }

    public abstract class RelationEngineFactory : IRelationEngineFactory
    {
        public abstract IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine);
    }

    public interface IRelationEngine<TAtom>
    {
        IRelationOperant<TValue, TAtom> GetOperant<TValue>();
        TResult Accept<TResult>(IRelationEngineVisitor<TAtom, TResult> visitor);
    }

    public interface IRelationEngineVisitor<TAtom, out TResult>
    {
        TResult Visit<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine);
    }

    public interface IRelationEngine<TAtom, TWrap, TRelation> : ILeftDistributive<TRelation, TAtom>, IRelationEngine<TAtom>
    {
        IZero<TAtom> AtomEngine { get; }
        IWrapEngine<TAtom, TWrap> WrapEngine { get; }
        IEnumerable<(IState, TRelation)> Derivatives(TRelation relation, Func<IState, Boolean> filter);
        Boolean HasEpsilon(TRelation relation);
        TWrap Epsilon(TRelation relation);
        TRelation Multiply(IAtomicRelation<TAtom> left, TRelation right);
        TRelation Concat(TRelation left, TRelation right);
        IReadOnlyCollection<TRelation> Factorize(TRelation relation);
        (TRelation, ISubstitutionList<TWrap>) Substitute(TRelation relation, Func<Int32, TWrap> substitution);
        TRelation AdjustSubstitution(TRelation relation, ISubstitutionAdjustment<TWrap> adjustment);
    }

    public abstract partial class RelationEngine<TAtom, TWrap, TRelation, TOuterRelation> : LeftDistributive<TRelation, TAtom>, IRelationEngine<TAtom, TWrap, TRelation>
    {
        public abstract IZero<TAtom> AtomEngine { get; }
        public virtual TRelation Concat(TRelation left, TRelation right)
            => throw new NotSupportedException();
        public abstract IEnumerable<(IState, TRelation)> Derivatives(TRelation relation, Func<IState, Boolean> filter);
        public abstract Boolean HasEpsilon(TRelation relation);
        public abstract TWrap Epsilon(TRelation relation);
        public IRelationOperant<TValue, TAtom> GetOperant<TValue>()
            => new Operant<TValue>(this, ((IWrapEngine<TValue, TAtom, TWrap>)WrapEngine).Flatten);
        public virtual IReadOnlyCollection<TRelation> Factorize(TRelation relation)
            => throw new NotSupportedException();
        public abstract TRelation Multiply(IAtomicRelation<TAtom> left, TRelation right);
        protected abstract IEqualityComparer<TOuterRelation> OuterComparer { get; }
        protected abstract TWrap OuterEpsilon(TOuterRelation relation);
        protected abstract TOuterRelation OuterOne { get; }
        protected abstract void Perform(IRelationalOperation<TAtom> operation, ref TOuterRelation relation);
        public abstract IWrapEngine<TAtom, TWrap> WrapEngine { get; }
        public virtual (TRelation, ISubstitutionList<TWrap>) Substitute(TRelation relation, Func<Int32, TWrap> substitution)
            => (relation, NoSubstitutionList<TWrap>.Instance);
        public virtual TRelation AdjustSubstitution(TRelation relation, ISubstitutionAdjustment<TWrap> adjustment)
            => relation;

        TResult IRelationEngine<TAtom>.Accept<TResult>(IRelationEngineVisitor<TAtom, TResult> visitor)
            => visitor.Visit(this);

        private partial class Operant<TValue> : RelationOperant<TValue, TAtom, TOuterRelation>
        {
            private readonly RelationEngine<TAtom, TWrap, TRelation, TOuterRelation> RelationEngine;
            private readonly Func<TWrap, TValue> Flatten;

            public Operant(RelationEngine<TAtom, TWrap, TRelation, TOuterRelation> relationEngine, Func<TWrap, TValue> flatten)
            {
                RelationEngine = relationEngine;
                Flatten = flatten;
            }

            public override IEqualityComparer<TOuterRelation> Comparer
                => RelationEngine.OuterComparer;

            public override TOuterRelation One
                => RelationEngine.OuterOne;

            public override TValue FlatEpsilon(TOuterRelation relation)
                => Flatten(RelationEngine.OuterEpsilon(relation));

            public override void Perform(IRelationalOperation<TAtom> operation, ref TOuterRelation relation)
                => RelationEngine.Perform(operation, ref relation);
        }
    }
}

using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Debug;

    public partial class DagRelationEngine<TAtom, TWrap, TNode> : IAnalyzer<TNode>
    {
        IEnumerable<IAnalyzable> IAnalyzer<TNode>.Constituents(TNode value)
        {
            foreach (var (edge, target) in ResolveNeighbors(value).OrderBy(_ => _.Item1.Atomic?.GetHashCode() ?? 0))
            {
                yield return Analyzable.CreateLeaf(edge.Atomic);
                if (WrapEngine is IAnalyzer<TWrap> analyzer)
                    yield return Analyzable.Create(analyzer, edge.Prefix);
                yield return Analyzable.Create(this, target);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using static EqualityComparerFactories;

    public abstract class AtomicOptimization
    {
        public abstract IAtomicRelations<TAtom, TKey> Perform<TValue, TAtom, TKey>(IAtomicRelations<TAtom, TKey> relations, IAtomEngine<TValue, TAtom> atomEngine);

        protected class GroupedRelation<TAtom> : List<IAtomicRelation<TAtom>>, IAtomicRelation<TAtom>
        {
            private static Int32 NextId = 0;
            private Int32 Id = NextId++;

            public TAtom Epsilon { get; set; }

            IEnumerable<(IState, TAtom, IAtomicRelation<TAtom>)> IAtomicRelation<TAtom>.Derivatives
                => Derivatives;

            public readonly List<(IState, TAtom, IAtomicRelation<TAtom>)> Derivatives
                = new List<(IState, TAtom, IAtomicRelation<TAtom>)>();

            public override String ToString()
                => $"a{Id}";
        }

        protected static IEqualityComparer<GroupedRelation<TAtom>> GroupedRelationEC<TAtom>()
            => UniqueHashCodeEC(IdentityEC<GroupedRelation<TAtom>>());
    }
}

using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public interface IRelationalOperation<TAtom> : IEquatable<IRelationalOperation<TAtom>>
    {
        TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation);
    }

    public interface IRelationOperantFactory
    {
        IRelationOperant<TValue, TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine);
    }

    public class RelationOperantFactory : IRelationOperantFactory
    {
        private readonly IRelationEngineFactory RelationEngineFactory;

        public RelationOperantFactory(IRelationEngineFactory relationEngineFactory)
        {
            RelationEngineFactory = relationEngineFactory;
        }

        public IRelationOperant<TValue, TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => RelationEngineFactory.Create(atomEngine).GetOperant<TValue>();
    }

    public interface IRelationOperant<TValue, TAtom>
    {
        TResult Accept<TResult>(IRelationOperantVisitor<TValue, TAtom, TResult> visitor);
    }

    public interface IRelationOperantVisitor<TValue, TAtom, out TResult>
    {
        TResult Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant);
    }

    public interface IRelationOperant<TValue, TAtom, TRelation> : IRelationOperant<TValue, TAtom>
    {
        IEqualityComparer<TRelation> Comparer { get; }
        TRelation One { get; }
        TValue FlatEpsilon(TRelation relation);
        void Perform(IRelationalOperation<TAtom> operation, ref TRelation relation);
    }

    public abstract class RelationOperant<TValue, TAtom, TRelation> : IRelationOperant<TValue, TAtom, TRelation>
    {
        public abstract IEqualityComparer<TRelation> Comparer { get; }
        public abstract TRelation One { get; }
        public abstract TValue FlatEpsilon(TRelation relation);
        public abstract void Perform(IRelationalOperation<TAtom> operation, ref TRelation relation);

        TResult IRelationOperant<TValue, TAtom>.Accept<TResult>(IRelationOperantVisitor<TValue, TAtom, TResult> visitor)
            => visitor.Visit(this);
    }

    public class MemoizingRelationOperant<TValue, TAtom, TRelation> : RelationOperant<TValue, TAtom, TRelation>
    {
        private readonly IRelationOperant<TValue, TAtom, TRelation> Inner;
        private readonly Dictionary<(IRelationalOperation<TAtom>, TRelation), TRelation> PerformCache;

        public MemoizingRelationOperant(IRelationOperant<TValue, TAtom, TRelation> inner)
        {
            Inner = inner;
            var operationEC = EqualityComparer<IRelationalOperation<TAtom>>.Default;
            PerformCache = new Dictionary<(IRelationalOperation<TAtom>, TRelation), TRelation>(ValueTupleEC(operationEC, Inner.Comparer));
        }

        public override IEqualityComparer<TRelation> Comparer
            => Inner.Comparer;
        public override TRelation One
            => Inner.One;
        public override TValue FlatEpsilon(TRelation relation)
            => Inner.FlatEpsilon(relation);

        private readonly Counter PerformCalls = new Counter();
        private readonly Counter PerformCacheSize = new Counter();
        public override void Perform(IRelationalOperation<TAtom> operation, ref TRelation relation)
        {
            PerformCalls.Increment();
            var key = (operation, relation);
            if (PerformCache.TryGetValue(key, out var result))
            {
                relation = result;
            }
            else
            {
                Inner.Perform(operation, ref relation);
                PerformCache.Add(key, relation);
                PerformCacheSize.Increment();
            }
        }
    }

    public class MemoizingRelationOperantFactory : IRelationOperantFactory
    {
        private readonly IRelationOperantFactory Inner;

        public MemoizingRelationOperantFactory(IRelationOperantFactory inner)
        {
            Inner = inner;
        }

        public IRelationOperant<TValue, TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => Inner.Create(atomEngine).Accept(new CreateRequest<TValue, TAtom>());

        private class CreateRequest<TValue, TAtom> : IRelationOperantVisitor<TValue, TAtom, IRelationOperant<TValue, TAtom>>
        {
            public IRelationOperant<TValue, TAtom> Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
                => new MemoizingRelationOperant<TValue, TAtom, TRelation>(operant);
        }
    }
}

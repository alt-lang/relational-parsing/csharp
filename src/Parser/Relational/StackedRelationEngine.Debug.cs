using System.Collections.Generic;

namespace Parser.Relational
{
    using Debug;

    public partial class StackedRelationEngine<TAtom, TWrap, TInner> : IAnalyzer<StackedRelationEngine<TAtom, TWrap, TInner>.Stack>
    {
        IEqualityComparer<Stack> IAnalyzer<Stack>.Comparer
            => OuterComparer;

        IEnumerable<IAnalyzable> IAnalyzer<Stack>.Constituents(Stack value)
        {
            if (InnerEngine is IAnalyzer<TInner> analyzer)
                for (; value != null; value = value.Parent)
                    yield return Analyzable.Create(analyzer, value.Inner);
        }
    }
}

using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public partial class StackedRelationEngine<TAtom, TWrap, TInner> : RelationEngine<TAtom, TWrap, StackedRelationEngine<TAtom, TWrap, TInner>.Tip, StackedRelationEngine<TAtom, TWrap, TInner>.Stack>
    {
        public struct StackData
        {
            public TInner Inner;
            public TInner Shape;
            public ISubstitutionList<TWrap> Content;

            public override String ToString()
                => Inner.ToString();
        }

        private static readonly Counter ResolveCalls_ = new Counter();
        private readonly Counter ResolveCalls = ResolveCalls_;
        private static readonly Histogram ResolveDependencyDepth_ = new Histogram();
        private readonly Histogram ResolveDependencyDepth = ResolveDependencyDepth_;
        private static readonly Histogram ResolveJumpLength_ = new Histogram();
        private readonly Histogram ResolveJumpLength = ResolveJumpLength_;

        [System.Diagnostics.DebuggerDisplay("[{Id}]")]
        public class Stack : ISubstitutionContext<TWrap, Stack>
        {
            private static Int32 LastId = 0;
            public readonly Int32 Id = ++LastId;

            public Stack Parent { get; }
            public Int32 Depth { get; }
            public StackData Data { get; }
            public TInner Inner
                => Data.Inner;
            public TInner Shape
                => Data.Shape;
            public ISubstitutionList<TWrap> Content
                => Data.Content;
            public Stack Origin { get; }
            private readonly List<Stack> Jumps;

            private Stack FindJump(Int32 count, Boolean allowNull)
            {
                // find youngest stack with IdentityCount <= count
                var jumpLength = 0;
                var current = this;
                for (var rank = Jumps.Count - 1; rank >= 0; rank--)
                    if (current.Jumps.Count > rank)
                    {
                        var previous = current.Jumps[rank];
                        if (previous.Content.IdentityCount > count)
                        {
                            current = previous;
                            jumpLength += 1 << rank;
                        }
                    }
                ResolveJumpLength_.Add(jumpLength);
                if (current.Content.IdentityCount <= count)
                    return current;
                if (current.Jumps.Count > 0)
                    return current.Jumps[0];
                if (allowNull)
                    return null;
                return current;
            }

            public Stack(Stack parent, StackData data, Stack origin)
            {
                Parent = parent;
                Depth = (parent?.Depth ?? 0) + 1;
                Data = data;
                Origin = origin;
                for (var d = 0; d < Content.IdentityDepth; d++)
                    origin = origin?.Parent;
                var previous = (origin?.Content?.IdentityDepth == Content.IdentityDepth) ? origin.FindJump(Content.IdentityCount - 1, true) : null;
                Jumps = new List<Stack>();
                for (var rank = 0; previous != null; rank++)
                {
                    Jumps.Add(previous);
                    previous = (previous.Jumps.Count > rank) ? previous.Jumps[rank] : null;
                }
            }

            public (TWrap, Stack) Resolve(Int32 dependencyDepth, Int32 contentIndex)
            {
                ResolveCalls_.Increment();
                ResolveDependencyDepth_.Add(dependencyDepth);
                var context = this;
                for (var d = 0; d < dependencyDepth; d++)
                    context = context.Parent;
                if (dependencyDepth == context?.Content?.IdentityDepth)
                    context = context.FindJump(contentIndex, false);
                return (context.Content[contentIndex], context.Origin);
            }

            public override String ToString()
                => Id.ToString();

            private class JumpComparer : IComparer<(Int32, Stack)>
            {
                public Int32 Compare((Int32, Stack) x, (Int32, Stack) y)
                    => x.Item1.CompareTo(y.Item1);
            }
        }

        public struct Tip
        {
            public Stack Stack;
            public TInner Inner;
            public Stack Origin;
        }

        public override IZero<TAtom> AtomEngine { get; }
        public override IWrapEngine<TAtom, TWrap> WrapEngine { get; }
        protected readonly IRelationEngine<TAtom, TWrap, TInner> InnerEngine;
        protected override IEqualityComparer<Stack> OuterComparer { get; }
        public override IEqualityComparer<Tip> Comparer { get; }
        private readonly Stack Guard;

        public StackedRelationEngine(IZero<TAtom> atomEngine, IWrapEngine<TAtom, TWrap> wrapEngine, IRelationEngine<TAtom, TWrap, TInner> innerEngine)
        {
            AtomEngine = atomEngine;
            WrapEngine = wrapEngine;
            InnerEngine = innerEngine;
            OuterComparer = IdentityEC<Stack>();
            Comparer = new ComponentsEqualityComparer<Tip>
            {
                { _ => _.Inner, InnerEngine.Comparer },
                { _ => _.Stack, OuterComparer },
            };
            InnerSet = new HashSet<TInner>(InnerEngine.Comparer);
            Guard = MakeStack(null, InnerEngine.Zero, null);
            PerformRootEntries = new Dictionary<IRelationalOperation<TAtom>, PerformEntry>(EquatableEC<IRelationalOperation<TAtom>>());
            PerformChildEntries = new Dictionary<(PerformEntry, TInner), PerformEntry>(ValueTupleEC(IdentityEC<PerformEntry>(), InnerEngine.Comparer));
        }

        protected virtual (TInner, ISubstitutionList<TWrap>) Substitute(TInner relation)
            => (relation, NoSubstitutionList<TWrap>.Instance);

        protected virtual TWrap Resolve(TWrap value, Stack context)
            => value;

        protected virtual TInner AdjustSubstitutionDepth(TInner relation, Int32 depth)
            => relation;

        private readonly HashSet<TInner> InnerSet;
        private readonly Counter InnerCount = new Counter();

        private Stack MakeStack(Stack parent, TInner inner, Stack origin)
        {
            if (InnerSet.Add(inner))
                InnerCount.Increment();
            var data = new StackData { Inner = inner };
            (data.Shape, data.Content) = Substitute(data.Inner);
            return new Stack(parent, data, origin);
        }

        public override Tip One
            => new Tip { Inner = InnerEngine.One };

        protected override Stack OuterOne
            => MakeStack(Guard, InnerEngine.One, null);

        public override Tip Zero
            => new Tip { Inner = InnerEngine.Zero };

        public override Boolean IsZero(Tip value)
            => InnerEngine.IsZero(value.Inner);

        protected Tip ToTip(Stack stack)
            => new Tip
            {
                Stack = stack.Parent,
                Inner = stack.Shape,
                Origin = stack,
            };

        protected Stack FromTip(Tip node)
            => MakeStack(node.Stack, node.Inner, node.Origin);

        private void MergeUp(ref Tip node)
        {
            var dependencyDepth = node.Origin.Depth - (node.Stack?.Depth ?? 0);
            node.Inner = InnerEngine.Concat(node.Inner, AdjustSubstitutionDepth(node.Stack.Shape, dependencyDepth));
            node.Stack = node.Stack.Parent;
        }

        private void Fix(ref Tip node)
        {
            while (InnerEngine.HasEpsilon(node.Inner))
            {
                if (node.Stack == Guard)
                    node.Stack = null;
                if (node.Stack == null)
                    break;
                MergeUp(ref node);
            }
        }

        public override IEnumerable<(IState, Tip)> Derivatives(Tip relation, Func<IState, Boolean> filter)
        {
            Fix(ref relation);
            foreach (var (state, inner) in InnerEngine.Derivatives(relation.Inner, filter))
            {
                relation.Inner = inner;
                yield return (state, relation);
            }
        }

        protected override TWrap OuterEpsilon(Stack relation)
            => Epsilon(ToTip(relation));

        public override Boolean HasEpsilon(Tip relation)
        {
            if (!InnerEngine.HasEpsilon(relation.Inner))
                return false;
            for (var stack = relation.Stack; stack != null && stack != Guard; stack = stack.Parent)
                if (!InnerEngine.HasEpsilon(stack.Inner))
                    return false;
            return true;
        }

        public override TWrap Epsilon(Tip relation)
        {
            if (!InnerEngine.HasEpsilon(relation.Inner))
                return WrapEngine.Zero;
            var values = new List<TWrap>();
            values.Add(Resolve(InnerEngine.Epsilon(relation.Inner), relation.Origin));
            for (var stack = relation.Stack; stack != null && stack != Guard; stack = stack.Parent)
            {
                if (!InnerEngine.HasEpsilon(stack.Inner))
                    return WrapEngine.Zero;
                values.Add(Resolve(InnerEngine.Epsilon(stack.Inner), stack.Origin));
            }
            return WrapEngine.Multiply(values);
        }

        public override Tip Multiply(IAtomicRelation<TAtom> left, Tip right)
        {
            right.Inner = InnerEngine.Multiply(left, right.Inner);
            return right;
        }

        public override Tip Multiply(TAtom left, Tip right)
        {
            right.Inner = InnerEngine.Multiply(left, right.Inner);
            return right;
        }

        private Stack Lca(Stack one, Stack two)
        {
            var seen = new List<Stack>();
            while (one != null || two != null)
            {
                if (one != null)
                {
                    if (seen.Contains(one))
                        return one;
                    seen.Add(one);
                    one = one.Parent;
                }
                if (two != null)
                {
                    if (seen.Contains(two))
                        return two;
                    seen.Add(two);
                    two = two.Parent;
                }
            }
            return null;
        }

        public override Tip Add(Tip left, Tip right)
        {
            if (InnerEngine.IsZero(left.Inner))
                return right;
            if (InnerEngine.IsZero(right.Inner))
                return left;
            Tip result = new Tip { Stack = Lca(left.Stack, right.Stack) };
            while (left.Stack != result.Stack)
                MergeUp(ref left);
            while (right.Stack != result.Stack)
                MergeUp(ref right);
            result.Inner = InnerEngine.Add(left.Inner, right.Inner);
            System.Diagnostics.Debug.Assert(left.Origin == right.Origin);
            result.Origin = left.Origin;
            return result;
        }

        private class PerformEntry
        {
            public StackData[] Result;
        }

        private readonly Dictionary<IRelationalOperation<TAtom>, PerformEntry> PerformRootEntries;
        private readonly Dictionary<(PerformEntry, TInner), PerformEntry> PerformChildEntries;

        private readonly Counter PerformCalls = new Counter();
        private readonly Histogram PerformLookupDepth = new Histogram();
        private readonly Histogram PerformInsertDepth = new Histogram();
        private readonly Histogram PerformInsertResult = new Histogram();
        private readonly Counter PerformContentTotalSize = new Counter();
        private readonly Histogram PerformContentSize = new Histogram();

        private Boolean Lookup(PerformEntry entry, Stack origin, ref Stack stack)
        {
            var depth = 0;
            while (entry.Result == null)
            {
                var shape = stack.Shape;
                stack = stack.Parent;
                if (!PerformChildEntries.TryGetValue((entry, shape), out entry))
                    return false;
                depth++;
            }
            stack = stack ?? Guard;
            foreach (var data in entry.Result)
                stack = new Stack(stack, data, origin);
            PerformLookupDepth.Add(depth);
            return true;
        }

        private void Insert(PerformEntry entry, Stack stack, ref Stack result)
        {
            var depth = 0;
            while (stack != result.Parent)
            {
                var shape = stack.Shape;
                stack = stack.Parent;
                entry = PerformChildEntries.GetOrCreate((entry, shape));
                depth++;
            }
            stack = stack ?? Guard;
            var items = new List<StackData>();
            foreach (var factor in InnerEngine.Factorize(result.Inner))
            {
                stack = MakeStack(stack, factor, result.Origin);
                items.Add(stack.Data);
            }
            result = stack;
            entry.Result = items.ToArray();
            PerformInsertDepth.Add(depth);
            PerformInsertResult.Add(items.Count);
        }

        private readonly Counter PerformSeparateContent = new Counter();
        protected override void Perform(IRelationalOperation<TAtom> operation, ref Stack relation)
        {
            PerformCalls.Increment();
            var root = PerformRootEntries.GetOrCreate(operation);
            Stack result = relation;
            if (!Lookup(root, relation, ref result))
            {
                result = FromTip(operation.Perform(this, ToTip(relation)));
                Insert(root, relation, ref result);
            }
            PerformSeparateContent.Increment(result.Content.Count - result.Content.IdentityCount);
            relation = result;
            PerformContentTotalSize.Increment(relation.Content?.Count ?? 0);
            PerformContentSize.Add(relation.Content?.Count ?? 0);
        }
    }

    public class StackedRelationEngineFactory : RelationEngineFactory
    {
        private readonly IRelationEngineFactory InnerFactory;

        public StackedRelationEngineFactory(IRelationEngineFactory innerFactory)
        {
            InnerFactory = innerFactory;
        }

        public override IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => InnerFactory
                .Create(atomEngine)
                .Accept(new CreateRequest<TAtom>());

        private class CreateRequest<TAtom> : IRelationEngineVisitor<TAtom, IRelationEngine<TAtom>>
        {
            public IRelationEngine<TAtom> Visit<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine) =>
                new StackedRelationEngine<TAtom, TWrap, TRelation>(engine.AtomEngine, engine.WrapEngine, engine);
        }

        public override String ToString() =>
            $"{nameof(StackedRelationEngineFactory)}({InnerFactory})";
    }
}

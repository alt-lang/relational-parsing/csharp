using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Common;
    using Utilities;
    using static EqualityComparerFactories;

    public class BackwardAtomicOptimization : AtomicOptimization
    {
        public override IAtomicRelations<TAtom, TKey> Perform<TValue, TAtom, TKey>(IAtomicRelations<TAtom, TKey> relations, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var classification = new Classifier<TAtom, TKey>(relations, atomEngine.Comparer).Run();
            if (classification == Classifier<TAtom, TKey>.NoClassification)
                return relations;
            var expectedDerivatives = new Dictionary<(GroupedRelation<TAtom>, IState, GroupedRelation<TAtom>), Int32>();
            foreach (var (source, newSource) in classification)
            {
                newSource.Epsilon = atomEngine.Add(newSource.Epsilon, source.Epsilon);
                foreach (var (state, atom, target) in source.Derivatives)
                {
                    var newTarget = classification[target];
                    var key = (newSource, state, newTarget);
                    if (expectedDerivatives.GetOrCreate(key) == 0)
                    {
                        expectedDerivatives[key] = newTarget.Count;
                        newSource.Derivatives.Add((state, atom, newTarget));
                    }
                    expectedDerivatives[key] -= 1;
                }
            }
            var newRelations = new AtomicRelations<TAtom, TKey>();
            newRelations.StartRelation = classification[relations.StartRelation];
            foreach (var (key, relation) in relations.StateRelations)
                newRelations.StateRelations.Add(key, classification[relation]);
            return newRelations;
        }

        private class Signature<TAtom, TKey>
        {
            public Boolean IsStart = false;
            public List<TKey> Entries = new List<TKey>();
            public List<(GroupedRelation<TAtom>, IState, TAtom)> Derivatives = new List<(GroupedRelation<TAtom>, IState, TAtom)>();
        }

        private class Classifier<TAtom, TKey> : Classifier<IAtomicRelation<TAtom>, Signature<TAtom, TKey>, GroupedRelation<TAtom>>
        {
            private readonly IAtomicRelations<TAtom, TKey> Relations;
            protected override IEnumerable<IAtomicRelation<TAtom>> Elements
                => Relations.AllRelations;
            protected override IEqualityComparer<Signature<TAtom, TKey>> SignatureEC { get; }

            public Classifier(IAtomicRelations<TAtom, TKey> relations, IEqualityComparer<TAtom> atomEC)
            {
                Relations = relations;
                var stateEC = UniqueHashCodeEC(IdentityEC<IState>());
                SignatureEC = new ComponentsEqualityComparer<Signature<TAtom, TKey>>
                {
                    { _ => _.IsStart, OperatorEC<Boolean>() },
                    { _ => _.Entries, MultiSetEC(EqualityComparer<TKey>.Default) },
                    { _ => _.Derivatives, MultiSetEC(ValueTupleEC(GroupedRelationEC<TAtom>(), stateEC, atomEC)) },
                };
            }

            protected override IEnumerable<(IAtomicRelation<TAtom>, Signature<TAtom, TKey>)> Signatures(IReadOnlyDictionary<IAtomicRelation<TAtom>, GroupedRelation<TAtom>> classification)
            {
                var result = new Dictionary<IAtomicRelation<TAtom>, Signature<TAtom, TKey>>();
                result.GetOrCreate(Relations.StartRelation).IsStart = true;
                foreach (var (key, entry) in Relations.StateRelations)
                    result.GetOrCreate(entry).Entries.Add(key);
                foreach (var (source, sourceClass) in classification)
                    foreach (var (state, atom, target) in source.Derivatives)
                        result.GetOrCreate(target).Derivatives.Add((sourceClass, state, atom));
                foreach (var (relation, signature) in result)
                    yield return (relation, signature);
            }

            public override String ToString()
                => $"{nameof(BackwardAtomicOptimization)}";
        }

        public override String ToString()
            => $"{nameof(BackwardAtomicOptimization)}";
    }
}

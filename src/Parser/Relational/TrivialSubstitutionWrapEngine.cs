using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Caching;
    using Utilities;
    using Valuations;
    using static EqualityComparerFactories;

    public partial class TrivialSubstitutionWrapEngine<TValue, TAtom, TWrap> : SubstitutionWrapEngine<TValue, TAtom>
    {
        private readonly IWrapEngine<TValue, TAtom, TWrap> InnerEngine;
        private readonly IEqualityComparer<(SubstitutionWrap<TAtom>, Object)> ResolveComparer;

        public TrivialSubstitutionWrapEngine(IZero<TAtom> atomEngine, IWrapEngine<TValue, TAtom, TWrap> innerEngine, ICacheStrategy cacheStrategy)
            : base(atomEngine, innerEngine.IsCommutative, innerEngine.IsIdempotent, cacheStrategy)
        {
            InnerEngine = innerEngine;
            NormalizeCache = new Dictionary<SubstitutionWrap<TAtom>, SubstitutionWrap<TAtom>>(Comparer);
            DependenciesCache = new Dictionary<SubstitutionWrap<TAtom>, IEnumerable<SubstitutionWrap<TAtom>.Substitute>>(Comparer);
            SignatureRootEntries = new Dictionary<SubstitutionWrap<TAtom>, SignatureEntry>(Comparer);
            SignatureChildEntries = new Dictionary<(SignatureEntry, SignatureEntry), SignatureEntry>(ValueTupleEC(IdentityEC<SignatureEntry>(), IdentityEC<SignatureEntry>()));
            SignatureValueCache = new Dictionary<SignatureEntry, TWrap>(IdentityEC<SignatureEntry>());
            ResolveComparer = ValueTupleEC(Comparer, IdentityEC<Object>());
        }

        public override ISemiring<TValue> Semiring
            => InnerEngine.Semiring;

        private class Inner : SubstitutionWrap<TAtom>
        {
            public TWrap Value { get; }

            public Inner(TWrap value)
            {
                Value = value;
            }

            public override String ToString()
                => Value.ToString();

            public static new IEqualityComparer<Inner> Comparer(IEqualityComparer<TWrap> wrapComparer)
                => new ComponentsEqualityComparer<Inner>
                {
                    { _ => _.Value, wrapComparer },
                };
        }

        private readonly Dictionary<SubstitutionWrap<TAtom>, SubstitutionWrap<TAtom>> NormalizeCache;
        private SubstitutionWrap<TAtom> Normalize(SubstitutionWrap<TAtom> wrap)
        {
            SubstitutionWrap<TAtom> result;
            if (!NormalizeCache.TryGetValue(wrap, out result))
            {
                switch (wrap)
                {
                    case SubstitutionWrap<TAtom>.Add add:
                        result = Add(add.Children.Select(Normalize));
                        break;
                    case SubstitutionWrap<TAtom>.Fold fold:
                        result = Fold(Normalize(fold.Child));
                        break;
                    case SubstitutionWrap<TAtom>.Multiply multiply:
                        result = Multiply(multiply.Children.Select(Normalize));
                        break;
                    case SubstitutionWrap<TAtom>.Substitute substitute:
                        result = Substitute(0);
                        break;
                    default:
                        result = wrap;
                        break;
                }
                NormalizeCache.Add(wrap, result);
            }
            return result;
        }

        private readonly Dictionary<SubstitutionWrap<TAtom>, IEnumerable<SubstitutionWrap<TAtom>.Substitute>> DependenciesCache;
        private readonly Histogram DependenciesSize = new Histogram();
        private readonly Histogram DependenciesResultSize = new Histogram();
        private IEnumerable<SubstitutionWrap<TAtom>.Substitute> Dependencies(SubstitutionWrap<TAtom> wrap)
        {
            while (wrap is SubstitutionWrap<TAtom>.Fold fold)
                wrap = fold.Child;
            IEnumerable<SubstitutionWrap<TAtom>.Substitute> result;
            if (!DependenciesCache.TryGetValue(wrap, out result))
            {
                /*
                var dependencies = new List<SubstitutionWrap<TAtom>.Substitute>();
                var visited = new HashSet<SubstitutionWrap<TAtom>>(Comparer);
                var queue = new Queue<SubstitutionWrap<TAtom>>();
                if (visited.Add(wrap))
                    queue.Enqueue(wrap);
                while (queue.Count > 0)
                    switch (queue.Dequeue())
                    {
                        case SubstitutionWrap<TAtom>.Add add:
                            foreach (var child in add.Children)
                                if (visited.Add(child))
                                    queue.Enqueue(child);
                            break;
                        case SubstitutionWrap<TAtom>.Fold fold:
                            if (visited.Add(fold.Child))
                                queue.Enqueue(fold.Child);
                            break;
                        case SubstitutionWrap<TAtom>.Multiply multiply:
                            foreach (var child in multiply.Children)
                                if (visited.Add(child))
                                    queue.Enqueue(child);
                            break;
                        case SubstitutionWrap<TAtom>.Substitute substitute:
                            dependencies.Add(substitute);
                            break;
                    }
                result = dependencies;
                DependenciesSize.Add(visited.Count);
                DependenciesResultSize.Add(dependencies.Count);
                */
                switch (wrap)
                {
                    case SubstitutionWrap<TAtom>.Add add:
                        foreach (var child in add.Children.Select(Dependencies))
                            if (child != null)
                                result = (result == null) ? child : result.Concat(child);
                        break;
                    case SubstitutionWrap<TAtom>.Atom atom:
                        break;
                    case SubstitutionWrap<TAtom>.Multiply multiply:
                        foreach (var child in multiply.Children.Select(Dependencies))
                            if (child != null)
                                result = (result == null) ? child : result.Concat(child);
                        break;
                    case SubstitutionWrap<TAtom>.Substitute substitute:
                        result = new[] { substitute };
                        break;
                    default:
                        throw new InvalidProgramException();
                }
                DependenciesCache.Add(wrap, result);
            }
            return result;
        }

        private class SignatureEntry
        {
            public Int32 Depth;
            private static Int32 NextId = 0;
            private readonly Int32 Id = NextId++;
            public override String ToString()
                => $"${Id}";
        }

        private readonly Dictionary<SubstitutionWrap<TAtom>, SignatureEntry> SignatureRootEntries;
        private readonly Dictionary<(SignatureEntry, SignatureEntry), SignatureEntry> SignatureChildEntries;
        private readonly Dictionary<SignatureEntry, TWrap> SignatureValueCache;

        private readonly Counter ComputeSignatureCalls = new Counter();
        private readonly Counter ComputeSignatureDistinctCalls = new Counter();
        private readonly Counter ComputeSignatureExtendCalls = new Counter();
        private readonly Histogram FindValueSignatureDepth = new Histogram();
        private readonly Histogram ComputeSignatureSize = new Histogram();
        private readonly Histogram FindValueSignatureDepthHit = new Histogram();
        private readonly Counter FindValueFoldCount = new Counter();
        private readonly Counter FindValueFoldHit = new Counter();
        private readonly Counter ResolveFoldCalls = new Counter();
        private readonly Histogram ResolveFoldDepth = new Histogram();

        public override SubstitutionWrap<TAtom> Resolve<TContext>(SubstitutionWrap<TAtom> topWrap, TContext topContext)
        {
            Dictionary<(SubstitutionWrap<TAtom>, Object), SignatureEntry> signatures = new Dictionary<(SubstitutionWrap<TAtom>, Object), SignatureEntry>(ResolveComparer);
            SignatureEntry ComputeSignature((SubstitutionWrap<TAtom>, TContext) key)
            {
                ComputeSignatureCalls.Increment();
                while (key.Item1 is SubstitutionWrap<TAtom>.Substitute substitute)
                    key = key.Item2.Resolve(substitute.DependencyDepth, substitute.ContentIndex);
                SignatureEntry signature;
                if (!signatures.TryGetValue(key, out signature))
                {
                    ComputeSignatureDistinctCalls.Increment();
                    var wrap = key.Item1;
                    signature = SignatureRootEntries.GetOrCreate(Normalize(wrap));
                    var depth = 0;
                    var size = 0;
                    var dependencies = Dependencies(wrap);
                    if (dependencies != null)
                        foreach (var dependency in dependencies)
                        {
                            var child = ComputeSignature((dependency, key.Item2));
                            if (child != null && child.Depth < 20)
                            {
                                ComputeSignatureExtendCalls.Increment();
                                signature = SignatureChildEntries.GetOrCreate((signature, child));
                                depth = Math.Max(depth, child.Depth + 1);
                                signature.Depth = depth;
                                size++;
                            }
                            else
                            {
                                signature = null;
                                break;
                            }
                        }
                    ComputeSignatureSize.Add(size);
                    signatures.Add(key, signature);
                }
                return signature;
            }
            Dictionary<(SubstitutionWrap<TAtom>, Object), TWrap> values = new Dictionary<(SubstitutionWrap<TAtom>, Object), TWrap>(ResolveComparer);
            TWrap FindValue(SubstitutionWrap<TAtom> wrap, TContext context)
            {
                while (wrap is SubstitutionWrap<TAtom>.Substitute substitute)
                    (wrap, context) = context.Resolve(substitute.DependencyDepth, substitute.ContentIndex);
                TWrap value;
                if (!values.TryGetValue((wrap, context), out value))
                {
                    switch (wrap)
                    {
                        case SubstitutionWrap<TAtom>.Add add:
                        {
                            var childValues = new List<TWrap>();
                            foreach (var child in add.Children)
                                childValues.Add(FindValue(child, context));
                            value = InnerEngine.Add(childValues);
                            break;
                        }
                        case SubstitutionWrap<TAtom>.Atom atom:
                            value = InnerEngine.Atom(atom.Value);
                            break;
                        case SubstitutionWrap<TAtom>.Fold fold:
                        {
                            /*
                            FindValueFoldCount.Increment();
                            var signature = ComputeSignature((fold.Child, context));
                            if (signature != null)
                                FindValueSignatureDepth.Add(signature.Depth);
                            if (signature == null || !SignatureValueCache.TryGetValue(signature, out value))
                            {
                                value = InnerEngine.Fold(FindValue(fold.Child, context));
                                if (signature != null)
                                    SignatureValueCache.Add(signature, value);
                            }
                            else
                            {
                                FindValueFoldHit.Increment();
                                FindValueSignatureDepthHit.Add(signature.Depth);
                            }
                            */
                            value = InnerEngine.Fold(FindValue(fold.Child, context));
                            break;
                        }
                        case Inner inner:
                            value = inner.Value;
                            break;
                        case SubstitutionWrap<TAtom>.Multiply multiply:
                        {
                            var childValues = new List<TWrap>();
                            foreach (var child in multiply.Children)
                                childValues.Add(FindValue(child, context));
                            value = InnerEngine.Multiply(childValues);
                            break;
                        }
                        default:
                            throw new InvalidProgramException();
                    }
                    values.Add((wrap, context), value);
                }
                return value;
            }
            if (Semiring is BooleanSemiring)
                return new Inner(IsZero(topWrap) ? InnerEngine.Zero : InnerEngine.One);
            return new Inner(FindValue(topWrap, topContext));
        }

        public override TValue Flatten(SubstitutionWrap<TAtom> wrap)
            => InnerEngine.Flatten(((Inner)Resolve(wrap, NoSubstitutionContext<SubstitutionWrap<TAtom>>.Instance)).Value);
    }

    public class TrivialSubstitutionWrapEngineFactory : IWrapEngineFactory
    {
        private static readonly ICacheStrategy CacheStrategy
            = new PermanentCacheStrategy();
        private readonly IWrapEngineFactory Inner;

        public TrivialSubstitutionWrapEngineFactory(IWrapEngineFactory inner)
        {
            Inner = inner;
        }

        public IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => Inner.Create(atomEngine).Accept(new CreateRequest<TAtom> { AtomEngine = atomEngine });

        private class CreateRequest<TAtom> : IWrapEngineVisitor<TAtom, ISubstitutionWrapEngine<TAtom>>
        {
            public IZero<TAtom> AtomEngine { get; set; }

            public ISubstitutionWrapEngine<TAtom> Visit<TValue, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
                => new TrivialSubstitutionWrapEngine<TValue, TAtom, TWrap>(AtomEngine, engine, CacheStrategy);
        }

        public override String ToString()
            => $"{nameof(TrivialSubstitutionWrapEngineFactory)}({Inner})";
    }
}

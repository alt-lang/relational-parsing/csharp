﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    public interface IAtomicRelation<TAtom>
    {
        TAtom Epsilon { get; }
        IEnumerable<(IState, TAtom, IAtomicRelation<TAtom>)> Derivatives { get; }
    }

    public class AtomicRelation<TAtom> : IAtomicRelation<TAtom>
    {
        private static Int32 NextId = 0;
        private Int32 Id = NextId++;
        public override String ToString()
            => $"r{Id}";

        public String Name { get; }
        public TAtom Epsilon { get; }
        public Dictionary<(IState, IAtomicRelation<TAtom>), TAtom> Derivatives { get; }

        IEnumerable<(IState, TAtom, IAtomicRelation<TAtom>)> IAtomicRelation<TAtom>.Derivatives
            => Derivatives.Select(_ => (_.Key.Item1, _.Value, _.Key.Item2));


        public AtomicRelation(String name, TAtom epsilon)
        {
            Name = name;
            Epsilon = epsilon;
            Derivatives = new Dictionary<(IState, IAtomicRelation<TAtom>), TAtom>();
        }
    }

    public interface IAtomicRelations<TAtom, TKey>
    {
        IReadOnlyDictionary<IState, TAtom> NullingAtoms { get; }
        IReadOnlyDictionary<Transition, TAtom> ShiftAtoms { get; }
        IAtomicRelation<TAtom> StartRelation { get; }
        IReadOnlyDictionary<TKey, IAtomicRelation<TAtom>> StateRelations { get; }
        IEnumerable<IAtomicRelation<TAtom>> AllRelations { get; }
    }

    public class AtomicRelations<TAtom, TKey> : IAtomicRelations<TAtom, TKey>
    {
        public IReadOnlyDictionary<IState, TAtom> NullingAtoms { get; set; }
        public IReadOnlyDictionary<Transition, TAtom> ShiftAtoms { get; set; }
        public IAtomicRelation<TAtom> StartRelation { get; set; }
        public Dictionary<TKey, IAtomicRelation<TAtom>> StateRelations { get; }
            = new Dictionary<TKey, IAtomicRelation<TAtom>>();

        IReadOnlyDictionary<TKey, IAtomicRelation<TAtom>> IAtomicRelations<TAtom, TKey>.StateRelations
            => StateRelations;

        IEnumerable<IAtomicRelation<TAtom>> IAtomicRelations<TAtom, TKey>.AllRelations
        {
            get
            {
                var visited = new HashSet<IAtomicRelation<TAtom>>();
                var queue = new Queue<IAtomicRelation<TAtom>>();
                visited.Add(StartRelation);
                queue.Enqueue(StartRelation);
                foreach (var relation in StateRelations.Values)
                    if (visited.Add(relation))
                        queue.Enqueue(relation);
                while (queue.Count > 0)
                    foreach (var (state, value, target) in queue.Dequeue().Derivatives)
                        if (visited.Add(target))
                            queue.Enqueue(target);
                return visited;
            }
        }
    }
}

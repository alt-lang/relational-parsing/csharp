namespace Parser.Relational
{
    using Algebra;

    public interface IAtomEngineFactory
    {
        IAtomEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring);
    }

    public interface IAtomEngine<TValue>
    {
        TResult Accept<TResult>(IAtomEngineVisitor<TValue, TResult> visitor);
    }

    public interface IAtomEngineVisitor<TValue, out TResult>
    {
        TResult Visit<TAtom>(IAtomEngine<TValue, TAtom> engine);
    }

    public interface IAtomEngine<TValue, TAtom> : IRightDistributive<TAtom, (TValue, TValue)>, IAtomEngine<TValue>
    {
        IValuationSemiring<TValue> Semiring { get; }
        TValue Wrap(TValue value, TAtom atom);
        TAtom Embed(TValue value);
    }

    public abstract class AtomEngine<TValue, TAtom> : RightDistributive<TAtom, (TValue, TValue)>, IAtomEngine<TValue, TAtom>
    {
        public abstract IValuationSemiring<TValue> Semiring { get; }
        public abstract TValue Wrap(TValue value, TAtom atom);
        public virtual TAtom Embed(TValue value)
            => Multiply(One, (Semiring.One, value));

        TResult IAtomEngine<TValue>.Accept<TResult>(IAtomEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }
}

namespace Parser.Relational
{
    using Algebra;

    public interface IWrapEngineFactory
    {
        IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine);
    }

    public interface IWrapEngine<TAtom>
    {
        TResult Accept<TResult>(IWrapEngineVisitor<TAtom, TResult> visitor);
    }

    public interface IWrapEngineVisitor<TAtom, out TResult>
    {
        TResult Visit<TValue, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine);
    }

    public interface IWrapEngine<TAtom, TWrap> : ISemiring<TWrap>, IWrapEngine<TAtom>
    {
        TWrap Atom(TAtom atom);
        TWrap Fold(TWrap wrap);
    }

    public interface IWrapEngine<TValue, TAtom, TWrap> : IWrapEngine<TAtom, TWrap>
    {
        ISemiring<TValue> Semiring { get; }
        TValue Flatten(TWrap wrap);
    }

    public abstract class WrapEngine<TValue, TAtom, TWrap> : Semiring<TWrap>, IWrapEngine<TValue, TAtom, TWrap>
    {
        public abstract ISemiring<TValue> Semiring { get; }
        public abstract TWrap Atom(TAtom atom);
        public abstract TValue Flatten(TWrap wrap);
        public abstract TWrap Fold(TWrap wrap);

        TResult IWrapEngine<TAtom>.Accept<TResult>(IWrapEngineVisitor<TAtom, TResult> visitor)
            => visitor.Visit(this);
    }
}

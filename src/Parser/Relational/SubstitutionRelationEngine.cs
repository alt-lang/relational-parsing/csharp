using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    public partial class SubstitutionRelationEngine<TAtom, TWrap, TInner> : StackedRelationEngine<TAtom, TWrap, TInner>
    {
        private ISubstitutionWrapEngine<TAtom, TWrap> SubstitutionWrapEngine
            => (ISubstitutionWrapEngine<TAtom, TWrap>) InnerEngine.WrapEngine;

        public SubstitutionRelationEngine(IRelationEngine<TAtom, TWrap, TInner> innerEngine)
            : base(innerEngine.AtomEngine, innerEngine.WrapEngine, innerEngine)
        {
        }

        protected override (TInner, ISubstitutionList<TWrap>) Substitute(TInner relation)
            => InnerEngine.Substitute(relation, SubstitutionWrapEngine.Substitute);

        protected override TWrap Resolve(TWrap value, Stack context)
            => SubstitutionWrapEngine.Resolve(value, context);

        protected override TInner AdjustSubstitutionDepth(TInner relation, int depth)
            => depth == 0 ? relation : InnerEngine.AdjustSubstitution(relation, new SubstitutionAdjustment<TAtom, TWrap>(SubstitutionWrapEngine, depth));
    }

    public class SubstitutionRelationEngineFactory : RelationEngineFactory
    {
        private readonly IRelationEngineFactory Inner;

        public SubstitutionRelationEngineFactory(IRelationEngineFactory inner)
        {
            Inner = inner;
        }

        public override IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => Inner
                .Create(atomEngine)
                .Accept(new CreateRequest<TAtom>());

        private class CreateRequest<TAtom> : IRelationEngineVisitor<TAtom, IRelationEngine<TAtom>>
        {
            public IRelationEngine<TAtom> Visit<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine)
                => new SubstitutionRelationEngine<TAtom, TWrap, TRelation>(engine);
        }
    }
}

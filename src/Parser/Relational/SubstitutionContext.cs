using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    public interface ISubstitutionContext<TWrap, TContext>
        where TContext : class, ISubstitutionContext<TWrap, TContext>
    {
        (TWrap, TContext) Resolve(Int32 dependencyDepth, Int32 contextIndex);
    }

    public class NoSubstitutionContext<TWrap> : ISubstitutionContext<TWrap, NoSubstitutionContext<TWrap>>
    {
        public (TWrap, NoSubstitutionContext<TWrap>) Resolve(int dependencyDepth, int contextIndex)
            => throw new InvalidOperationException($"Unexpected call to {nameof(NoSubstitutionContext<TWrap>)}.{nameof(Resolve)}().");

        public static NoSubstitutionContext<TWrap> Instance { get; }
            = new NoSubstitutionContext<TWrap>();

        private NoSubstitutionContext()
        {
        }

        public override String ToString()
            => "0";
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Common;
    using Utilities;
    using static EqualityComparerFactories;

    public class ForwardAtomicOptimization : AtomicOptimization
    {
        public override IAtomicRelations<TAtom, TKey> Perform<TValue, TAtom, TKey>(IAtomicRelations<TAtom, TKey> relations, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var classification = new Classifier<TAtom>(relations.AllRelations, atomEngine).Run();
            if (classification == Classifier<TAtom>.NoClassification)
                return relations;
            var processed = new HashSet<GroupedRelation<TAtom>>();
            foreach (var (source, newSource) in classification)
                if (processed.Add(newSource))
                {
                    newSource.Epsilon = source.Epsilon;
                    newSource.Derivatives.AddRange(source.Derivatives
                        .GroupBy(_ => (_.Item1, classification[_.Item3]), _ => _.Item2)
                        .Select(g => (g.Key.Item1, atomEngine.Add(g), g.Key.Item2 as IAtomicRelation<TAtom>)));
                }
            var result = new AtomicRelations<TAtom, TKey>();
            result.NullingAtoms = relations.NullingAtoms;
            result.ShiftAtoms = relations.ShiftAtoms;
            result.StartRelation = classification[relations.StartRelation];
            foreach (var (state, relation) in relations.StateRelations)
                result.StateRelations.Add(state, classification[relation]);
            return result;
        }

        private class Signature<TAtom>
        {
            public Dictionary<Int32, TAtom> Epsilon = new Dictionary<Int32, TAtom>();
            public Dictionary<(IState, GroupedRelation<TAtom>), TAtom> Derivatives = new Dictionary<(IState, GroupedRelation<TAtom>), TAtom>();
        }

        private class Classifier<TAtom> : Classifier<IAtomicRelation<TAtom>, Signature<TAtom>, GroupedRelation<TAtom>>
        {
            protected override IEnumerable<IAtomicRelation<TAtom>> Elements { get; }
            private readonly IAdditive<TAtom> AtomEngine;
            protected override IEqualityComparer<Signature<TAtom>> SignatureEC { get; }

            public Classifier(IEnumerable<IAtomicRelation<TAtom>> allRelations, IAdditive<TAtom> atomEngine)
            {
                Elements = allRelations;
                var stateEC = UniqueHashCodeEC(IdentityEC<IState>());
                AtomEngine = atomEngine;
                SignatureEC = new ComponentsEqualityComparer<Signature<TAtom>>
                {
                    { _ => _.Epsilon, DictionaryEC(OperatorEC<Int32>(), AtomEngine.Comparer) },
                    { _ => _.Derivatives, DictionaryEC(ValueTupleEC(stateEC, GroupedRelationEC<TAtom>()), AtomEngine.Comparer) },
                };
            }

            protected override IEnumerable<(IAtomicRelation<TAtom>, Signature<TAtom>)> Signatures(IReadOnlyDictionary<IAtomicRelation<TAtom>, GroupedRelation<TAtom>> classification)
            {
                var result = new Dictionary<IAtomicRelation<TAtom>, Signature<TAtom>>();
                foreach (var (source, sourceClass) in classification)
                {
                    var signature = result.GetOrCreate(source);
                    {
                        var key = 0;
                        signature.Epsilon[key] = signature.Epsilon.TryGetValue(key, out var current) ? AtomEngine.Add(current, source.Epsilon) : source.Epsilon;
                    }
                    foreach (var (state, value, target) in source.Derivatives)
                    {
                        var key = (state, classification[target]);
                        signature.Derivatives[key] = signature.Derivatives.TryGetValue(key, out var current) ? AtomEngine.Add(current, value) : value;
                    }
                }
                foreach (var (relation, signature) in result)
                    yield return (relation, signature);
            }

            public override String ToString()
                => $"{nameof(ForwardAtomicOptimization)}";
        }

        public override String ToString()
            => $"{nameof(ForwardAtomicOptimization)}";
    }
}

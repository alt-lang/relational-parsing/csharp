using System.Collections.Generic;

namespace Parser.Relational
{
    using Debug;

    public partial class SubstitutionRelationEngine<TAtom, TWrap, TInner> : IAnalyzer<StackedRelationEngine<TAtom, TWrap, TInner>.Stack>
    {
        IEnumerable<IAnalyzable> IAnalyzer<Stack>.Constituents(Stack value)
        {
            if (InnerEngine is IAnalyzer<TInner> analyzer)
                for (; value != null; value = value.Parent)
                {
                    var origin = value.Origin;
                    IAnalyzable Resolve(IAnalyzable analyzable)
                        => (analyzable is IAnalyzable<TWrap> wrap) ? Analyzable.Create(wrap.Analyzer, SubstitutionWrapEngine.Resolve(wrap.Value, origin)) : analyzable;
                    yield return Analyzable.Create(analyzer.Map(Resolve), value.Inner);
                }
        }
    }
}

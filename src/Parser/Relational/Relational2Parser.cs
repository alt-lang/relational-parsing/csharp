using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Utilities;

    public sealed class Relational2Initialization<TAtom> : IRelationalOperation<TAtom>
    {
        private readonly IAtomicRelations<TAtom, (IState, Char)> AtomicRelations;

        public Relational2Initialization(IAtomicRelations<TAtom, (IState, Char)> atomicRelations)
        {
            AtomicRelations = atomicRelations;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            relation = engine.Multiply(AtomicRelations.StartRelation, relation);
            return relation;
        }

        public Boolean Equals(IRelationalOperation<TAtom> other)
            => other is Relational2Initialization<TAtom>;

        public override Int32 GetHashCode()
            => -1;

        public override String ToString()
            => "init";
    }

    public sealed class Relational2Phase<TAtom> : IRelationalOperation<TAtom>
    {
        private readonly IAtomicRelations<TAtom, (IState, Char)> AtomicRelations;
        private readonly Char Symbol;
        private readonly Func<IState, Boolean> LookaheadFilter;

        public Relational2Phase(IAtomicRelations<TAtom, (IState, Char)> atomicRelations, Char symbol, Func<IState, Boolean> lookaheadFilter)
        {
            AtomicRelations = atomicRelations;
            Symbol = symbol;
            LookaheadFilter = lookaheadFilter;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            var byAtomic = new Dictionary<IAtomicRelation<TAtom>, List<TRelation>>();
            foreach (var (state, relation1) in engine.Derivatives(relation, LookaheadFilter))
                if (AtomicRelations.StateRelations.TryGetValue((state, Symbol), out var atomic))
                    byAtomic.GetOrCreate(atomic).Add(relation1);
            return engine.Add(byAtomic.Select(_ => engine.Multiply(_.Key, engine.Add(_.Value))));
        }

        public Boolean Equals(IRelationalOperation<TAtom> other)
            => other is Relational2Phase<TAtom> phase && phase.Symbol == Symbol;

        public override Int32 GetHashCode()
            => Symbol.GetHashCode();

        public override String ToString()
            => Symbol.ToString();
    }

    public class Relational2Parser<TValue, TAtom, TRelation> : IParser<TValue>
    {
        public IValuationSemiring<TValue> Semiring { get; }
        private readonly IAtomicRelations<TAtom, (IState, Char)> AtomicRelations;
        private readonly IRelationOperant<TValue, TAtom, TRelation> RelationOperant;
        private readonly Dictionary<Char, HashSet<IState>> LookaheadSets;

        public Relational2Parser(IValuationSemiring<TValue> semiring, IAtomicRelations<TAtom, (IState, Char)> atomicRelations, IRelationOperant<TValue, TAtom, TRelation> relationOperant, Dictionary<Char, HashSet<IState>> lookaheadSets)
        {
            Semiring = semiring;
            AtomicRelations = atomicRelations;
            RelationOperant = relationOperant;
            LookaheadSets = lookaheadSets;
        }

        private static Func<IState, Boolean> NoFilter
            = _ => true;

        public TValue Run(IEnumerable<Char> input, Action step)
        {
            TRelation current = RelationOperant.One;
            RelationOperant.Perform(new Relational2Initialization<TAtom>(AtomicRelations), ref current);
            step();
            foreach (var symbol in input)
            {
                RelationOperant.Perform(new Relational2Phase<TAtom>(AtomicRelations, symbol, LookaheadSets != null ? LookaheadSets.GetOrCreate(symbol).Contains : NoFilter), ref current);
                step();
            }
            return RelationOperant.FlatEpsilon(current);
        }
    }
}

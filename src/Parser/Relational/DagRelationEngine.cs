using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Graphs;
    using Utilities;
    using static EqualityComparerFactories;

    public struct DagRelationEdge<TAtom, TWrap>
    {
        public TWrap Prefix;
        public IAtomicRelation<TAtom> Atomic;
        public Int32 Offset;

        public static IEqualityComparer<DagRelationEdge<TAtom, TWrap>> EdgeEC(IWrapEngine<TAtom, TWrap> wrapEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            => new ComponentsEqualityComparer<DagRelationEdge<TAtom, TWrap>>
            {
                { _ => _.Prefix, wrapEngine.Comparer },
                { _ => _.Atomic, atomicEC },
                { _ => _.Offset, OperatorEC<Int32>() },
            };

        public static IEqualityComparer<DagRelationEdge<TAtom, TWrap>> MergeEC(IWrapEngine<TAtom, TWrap> wrapEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            => new ComponentsEqualityComparer<DagRelationEdge<TAtom, TWrap>>
            {
                { _ => _.Atomic, atomicEC },
                { _ => _.Offset, OperatorEC<Int32>() },
            };

        public static Func<DagRelationEdge<TAtom, TWrap>, DagRelationEdge<TAtom, TWrap>, DagRelationEdge<TAtom, TWrap>> MergeEdges(IWrapEngine<TAtom, TWrap> wrapEngine)
            => (x, y) => new DagRelationEdge<TAtom, TWrap>
            {
                Prefix = wrapEngine.Add(x.Prefix, y.Prefix),
                Atomic = x.Atomic,
                Offset = x.Offset,
            };

        public override String ToString()
            => Offset == 0 ? $"({Prefix},{Atomic})" : $"+{Offset}";
    }

    public partial class DagRelationEngine<TAtom, TWrap, TNode> : RelationEngine<TAtom, TWrap, TNode, TNode>
    {
        public override IZero<TAtom> AtomEngine { get; }
        public override IWrapEngine<TAtom, TWrap> WrapEngine { get; }
        private ISubstitutionWrapEngine<TAtom, TWrap> SubstitutionWrapEngine
            => (ISubstitutionWrapEngine<TAtom, TWrap>)WrapEngine;
        protected readonly IGraphEngine<DagRelationEdge<TAtom, TWrap>, TNode> GraphEngine;
        private readonly IEqualityComparer<(DagRelationEdge<TAtom, TWrap>, TNode)> EdgeComparer;
        public override IEqualityComparer<TNode> Comparer
            => GraphEngine.Comparer;
        protected override IEqualityComparer<TNode> OuterComparer
            => Comparer;

        public DagRelationEngine(IZero<TAtom> atomEngine, IWrapEngine<TAtom, TWrap> wrapEngine, IGraphEngine<DagRelationEdge<TAtom, TWrap>, TNode> graphEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
        {
            AtomEngine = atomEngine;
            WrapEngine = wrapEngine;
            GraphEngine = graphEngine;
            EdgeComparer = ValueTupleEC(GraphEngine.MergeEC, GraphEngine.Comparer);
            Zero = GraphEngine.Zero;
            One = GraphEngine.Create((MakeEdge(WrapEngine.One, null), Zero));
            SubstituteRootEntry = new SubstituteEntry(GraphEngine.Comparer, GraphEngine.Zero);
            SubstituteChildEntries = new Dictionary<(SubstituteEntry, TNode), SubstituteEntry>(ValueTupleEC(IdentityEC<SubstituteEntry>(), GraphEngine.Comparer));
        }

        protected DagRelationEdge<TAtom, TWrap> MakeEdge(TWrap prefix, IAtomicRelation<TAtom> atomic, Int32 offset = 0)
            => new DagRelationEdge<TAtom, TWrap> { Prefix = prefix, Atomic = atomic, Offset = offset };

        private DagRelationEdge<TAtom, TWrap> MakeOffsetEdge(Int32 offset)
            => new DagRelationEdge<TAtom, TWrap> { Prefix = WrapEngine.One, Atomic = null, Offset = offset };

        private IEnumerable<(DagRelationEdge<TAtom, TWrap>, TNode)> ResolveNeighbors(TNode node)
        {
            foreach (var (edge, target) in GraphEngine.Neighbors(node))
                if (edge.Offset == 0)
                    yield return (edge, target);
                else foreach (var (edge2, target2) in GraphEngine.Neighbors(target))
                {
                    if (edge2.Offset != 0)
                        throw new InvalidProgramException();
                    yield return (MakeEdge(WrapEngine.Multiply(edge.Prefix, SubstitutionWrapEngine.ShiftIndex(edge2.Prefix, edge.Offset)), edge2.Atomic), target2);
                }
        }

        private IEnumerable<TNode> ResolveTargets(TNode node)
        {
            foreach (var (edge, target) in GraphEngine.Neighbors(node))
                if (edge.Offset == 0)
                    yield return target;
                else foreach (var (edge2, target2) in GraphEngine.Neighbors(target))
                    yield return target2;
        }

        protected virtual TNode Prepend(TWrap wrap, TNode node)
            => GraphEngine.Map(node, edge => MakeEdge(WrapEngine.Multiply(wrap, edge.Prefix), edge.Atomic, edge.Offset));

        public override TNode Zero { get; }
        public override Boolean IsZero(TNode relation)
            => GraphEngine.IsZero(relation);

        public override TNode One { get; }
        protected override TNode OuterOne
            => One;

        public override TNode Add(IEnumerable<TNode> relations)
            => GraphEngine.Add(relations);

        public override Boolean HasEpsilon(TNode relation)
            => ResolveTargets(relation).Any(GraphEngine.IsZero);

        public override TWrap Epsilon(TNode relation)
        {
            var values = new List<TWrap>();
            foreach (var (edge, target) in ResolveNeighbors(relation))
                if (GraphEngine.IsZero(target))
                    values.Add(edge.Prefix);
            return WrapEngine.Add(values);
        }
        protected override TWrap OuterEpsilon(TNode relation)
            => Epsilon(relation);

        public override IEnumerable<(IState, TNode)> Derivatives(TNode relation, Func<IState, Boolean> filter)
        {
            var neighbors = new Dictionary<IState, List<TNode>>();
            var finals = new Dictionary<IState, List<(DagRelationEdge<TAtom, TWrap>, TNode)>>();
            foreach (var (edge, target) in ResolveNeighbors(relation))
                if (!GraphEngine.IsZero(target))
                {
                    var foldPrefix = default(TWrap);
                    var foldPrefixCalculated = false;
                    foreach (var (state, prefix, atomic) in edge.Atomic.Derivatives)
                        if (filter(state))
                        {
                            if (!foldPrefixCalculated)
                            {
                                foldPrefix = WrapEngine.Fold(edge.Prefix);
                                foldPrefixCalculated = true;
                            }
                            var leading = WrapEngine.Fold(WrapEngine.Multiply(foldPrefix, WrapEngine.Atom(prefix)));
                            if (!AtomEngine.IsZero(atomic.Epsilon))
                                neighbors.GetOrCreate(state).Add(Prepend(leading, Prepend(WrapEngine.Atom(atomic.Epsilon), target)));
                            if (atomic.Derivatives.Any())
                                finals.GetOrCreate(state).Add((MakeEdge(leading, atomic), target));
                        }
                }
            foreach (var (state, final) in finals)
                neighbors.GetOrCreate(state).Add(GraphEngine.Create(final));
            var result = new List<(IState, TNode)>();
            foreach (var (state, targets) in neighbors)
                result.Add((state, GraphEngine.Add(targets)));
            return result;
        }

        public override TNode Multiply(TAtom atom, TNode relation)
            => GraphEngine.IsZero(relation) ? relation : Prepend(WrapEngine.Atom(atom), relation);

        public override TNode Multiply(IAtomicRelation<TAtom> left, TNode right)
        {
            if (GraphEngine.IsZero(right))
                return right;
            var nodes = new List<TNode>();
            if (left.Derivatives.Any())
                nodes.Add(GraphEngine.Create((MakeEdge(WrapEngine.One, left), right)));
            if (!AtomEngine.IsZero(left.Epsilon))
                nodes.Add(Multiply(left.Epsilon, right));
            return GraphEngine.Add(nodes);
        }

        public override TNode Concat(TNode left, TNode right)
        {
            var nodes = new List<TNode>();
            var final = new List<(DagRelationEdge<TAtom, TWrap>, TNode)>();
            foreach (var (edge, target) in ResolveNeighbors(left))
                if (GraphEngine.IsZero(target))
                    nodes.Add(Prepend(edge.Prefix, right));
                else
                    final.Add((edge, Concat(target, right)));
            if (final.Count > 0)
                nodes.Add(GraphEngine.Create(final));
            return GraphEngine.Add(nodes);
        }

        protected virtual TNode Dominator(TNode node)
        {
            var candidates = new HashSet<TNode>(ResolveTargets(node), GraphEngine.Comparer);
            if (candidates.Count == 0)
                return node;
            while (candidates.Count > 1)
            {
                if (candidates.Contains(Zero))
                    return Zero;
                var two = candidates.Take(2).ToArray();
                candidates.Remove(two[0]);
                candidates.Remove(two[1]);
                if (GraphEngine.Rank(two[0]) < GraphEngine.Rank(two[1]))
                    Array.Reverse(two);
                candidates.Add(Dominator(two[0]));
                candidates.Add(two[1]);
            }
            return candidates.Single();
        }

        protected virtual TNode FactorOut(TNode concat, TNode right)
        {
            if (Comparer.Equals(concat, right))
                return One;
            var finals = new List<(DagRelationEdge<TAtom, TWrap>, TNode)>();
            foreach (var (edge, neighbor) in GraphEngine.Neighbors(concat))
            {
                if (GraphEngine.IsZero(neighbor))
                    throw new InvalidOperationException();
                var target = FactorOut(neighbor, right);
                finals.Add((edge, target));
            }
            return GraphEngine.Create(finals);
        }

        public override IReadOnlyCollection<TNode> Factorize(TNode relation)
        {
            var factors = new Stack<TNode>();
            while (true)
            {
                var dominator = Dominator(relation);
                if (GraphEngine.IsZero(dominator) || HasEpsilon(dominator))
                    break;
                factors.Push(FactorOut(relation, dominator));
                relation = dominator;
            }
            factors.Push(relation);
            return factors;
        }

        protected override void Perform(IRelationalOperation<TAtom> operation, ref TNode relation)
            => relation = operation.Perform(this, relation);

        private class SubstituteEntry
        {
            public ImmutableDictionary<TNode, TNode> Nodes;
            public SubstitutionList<TWrap> Values;

            public SubstituteEntry(IEqualityComparer<TNode> nodeEC, TNode zero)
            {
                Nodes = ImmutableDictionary.Create<TNode, TNode>(nodeEC);
                Nodes.Add(zero, zero);
                Values = null;
            }

            public SubstituteEntry(SubstituteEntry original)
            {
                Nodes = original.Nodes;
                Values = original.Values;
            }
        }

        private readonly SubstituteEntry SubstituteRootEntry;
        private readonly Dictionary<(SubstituteEntry, TNode), SubstituteEntry> SubstituteChildEntries;
        private readonly Counter SubstituteCalls = new Counter();
        private readonly Counter SubstituteIncludeCalls = new Counter();
        private readonly Counter SubstituteIncludeEntries = new Counter();
        private readonly Histogram SubstituteIncludeNodes = new Histogram();
        private readonly Histogram SubstituteIncludeValues = new Histogram();
        private readonly Histogram SubstituteIdentityValues = new Histogram();

        public override (TNode, ISubstitutionList<TWrap>) Substitute(TNode relation, Func<Int32, TWrap> substitution)
        {
            SubstituteCalls.Increment();
            var current = SubstituteRootEntry;
            current = Include(current, relation);
            return (current.Nodes[relation], current.Values);
            SubstituteEntry Include(SubstituteEntry entry, TNode node)
            {
                SubstituteIncludeCalls.Increment();
                if (entry.Nodes.ContainsKey(node))
                    return entry;
                SubstituteEntry nextEntry;
                var key = (entry, node);
                if (!SubstituteChildEntries.TryGetValue(key, out nextEntry))
                {
                    SubstituteIncludeEntries.Increment();
                    nextEntry = entry;
                    foreach (var neighbor in ResolveTargets(node).OrderByDescending(GraphEngine.Rank).ThenBy(GraphEngine.Ordinal))
                        nextEntry = Include(nextEntry, neighbor);
                    nextEntry = new SubstituteEntry(nextEntry);
                    var nextEdges = new List<(DagRelationEdge<TAtom, TWrap>, TNode)>();
                    var valuesByEdge = new Dictionary<(DagRelationEdge<TAtom, TWrap>, TNode), List<TWrap>>(EdgeComparer);
                    foreach (var (edge, target) in ResolveNeighbors(node).OrderBy(_ => GraphEngine.Ordinal(_.Item2)))
                    {
                        var newTarget = nextEntry.Nodes[target];
                        if (edge.Atomic == null && !GraphEngine.IsZero(newTarget))
                            throw new InvalidProgramException();
                        valuesByEdge.GetOrCreate((edge, newTarget)).Add(edge.Prefix);
                    }
                    var values = new List<TWrap>();
                    foreach (var ((edge, target), wraps) in valuesByEdge)
                    {
                        nextEdges.Add((MakeEdge(substitution(values.Count), edge.Atomic), target));
                        values.Add(WrapEngine.Add(wraps));
                    }
                    var newNode = GraphEngine.Create(nextEdges);
                    if (nextEntry.Values?.Count > 0)
                        newNode = GraphEngine.Create((MakeOffsetEdge(nextEntry.Values.Count), newNode));
                    nextEntry.Nodes = nextEntry.Nodes.Add(node, newNode);
                    nextEntry.Values = new SubstitutionList<TWrap>(nextEntry.Values, values);
                    SubstituteChildEntries.Add(key, nextEntry);
                    SubstituteIncludeNodes.Add(nextEntry.Nodes.Count);
                    SubstituteIncludeValues.Add(nextEntry.Values.Count);
                    SubstituteIdentityValues.Add(nextEntry.Values.IdentityCount);
                }
                return nextEntry;
            }
        }

        public override TNode AdjustSubstitution(TNode relation, ISubstitutionAdjustment<TWrap> adjustment)
            => GraphEngine.Create(GraphEngine.Neighbors(relation).Select(_ => (MakeEdge(adjustment.Apply(_.Item1.Prefix), _.Item1.Atomic, _.Item1.Offset), AdjustSubstitution(_.Item2, adjustment))));
    }

    public class DagRelationEngineFactory : RelationEngineFactory
    {
        private readonly IWrapEngineFactory WrapEngineFactory;
        private readonly IGraphEngineFactory GraphEngineFactory;

        public DagRelationEngineFactory(IWrapEngineFactory wrapEngineFactory, IGraphEngineFactory graphEngineFactory)
        {
            WrapEngineFactory = wrapEngineFactory;
            GraphEngineFactory = graphEngineFactory;
        }

        public override IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => WrapEngineFactory
                .Create(atomEngine)
                .Accept(new CreateRequest<TAtom>
                {
                    AtomEngine = atomEngine,
                    GraphEngineFactory = GraphEngineFactory,
                });

        private class CreateRequest<TAtom> : IWrapEngineVisitor<TAtom, IRelationEngine<TAtom>>
        {
            public IZero<TAtom> AtomEngine { get; set; }
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public IRelationEngine<TAtom> Visit<TValue, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
            {
                var atomicEC = IdentityEC<IAtomicRelation<TAtom>>();
                return GraphEngineFactory
                    .Create(
                        DagRelationEdge<TAtom, TWrap>.EdgeEC(engine, atomicEC),
                        DagRelationEdge<TAtom, TWrap>.MergeEC(engine, atomicEC),
                        DagRelationEdge<TAtom, TWrap>.MergeEdges(engine),
                        engine.IsIdempotent)
                    .Accept(new CreateRequest<TAtom, TWrap>
                    {
                        AtomEngine = AtomEngine,
                        WrapEngine = engine,
                        AtomicEC = atomicEC,
                    });
            }
        }

        private class CreateRequest<TAtom, TWrap> : IGraphEngineVisitor<DagRelationEdge<TAtom, TWrap>, IRelationEngine<TAtom>>
        {
            public IZero<TAtom> AtomEngine { get; set; }
            public IWrapEngine<TAtom, TWrap> WrapEngine { get; set; }
            public IEqualityComparer<IAtomicRelation<TAtom>> AtomicEC { get; set; }

            public IRelationEngine<TAtom> Visit<TNode>(IGraphEngine<DagRelationEdge<TAtom, TWrap>, TNode> engine)
                => new DagRelationEngine<TAtom, TWrap, TNode>(AtomEngine, WrapEngine, engine, AtomicEC);
        }

        public override String ToString() =>
            $"{nameof(DagRelationEngineFactory)}({GraphEngineFactory})";
    }

    public class MemoizingDagRelationEngine<TAtom, TWrap, TNode> : DagRelationEngine<TAtom, TWrap, TNode>
    {
        private readonly Dictionary<(TWrap, TNode), TNode> PrependCache;
        private readonly Dictionary<(TNode, Func<IState, Boolean>), IEnumerable<(IState, TNode)>> DerivativesCache;
        private readonly Dictionary<(TNode, TNode), TNode> ConcatCache;
        private readonly Dictionary<TNode, TNode> DominatorCache;
        private readonly Dictionary<(TNode, TNode), TNode> FactorOutCache;
        private readonly Dictionary<(TNode, ISubstitutionAdjustment<TWrap>), TNode> AdjustSubstitutionCache;

        public MemoizingDagRelationEngine(IZero<TAtom> atomEngine, IWrapEngine<TAtom, TWrap> wrapEngine, IGraphEngine<DagRelationEdge<TAtom, TWrap>, TNode> graphEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            : base(atomEngine, wrapEngine, graphEngine, atomicEC)
        {
            var filterEC = UniqueHashCodeEC(IdentityEC<Func<IState, Boolean>>());
            PrependCache = new Dictionary<(TWrap, TNode), TNode>(ValueTupleEC(WrapEngine.Comparer, GraphEngine.Comparer));
            DerivativesCache = new Dictionary<(TNode, Func<IState, Boolean>), IEnumerable<(IState, TNode)>>(ValueTupleEC(GraphEngine.Comparer, filterEC));
            ConcatCache = new Dictionary<(TNode, TNode), TNode>(ValueTupleEC(GraphEngine.Comparer, Comparer));
            DominatorCache = new Dictionary<TNode, TNode>(GraphEngine.Comparer);
            FactorOutCache = new Dictionary<(TNode, TNode), TNode>(ValueTupleEC(GraphEngine.Comparer, GraphEngine.Comparer));
            AdjustSubstitutionCache = new Dictionary<(TNode, ISubstitutionAdjustment<TWrap>), TNode>(ValueTupleEC(GraphEngine.Comparer, EquatableEC<ISubstitutionAdjustment<TWrap>>()));
        }

        protected override TNode Prepend(TWrap wrap, TNode node)
        {
            TNode result;
            if (!PrependCache.TryGetValue((wrap, node), out result))
            {
                result = base.Prepend(wrap, node);
                PrependCache.Add((wrap, node), result);
            }
            return result;
        }

        public override IEnumerable<(IState, TNode)> Derivatives(TNode relation, Func<IState, Boolean> filter)
        {
            IEnumerable<(IState, TNode)> result;
            if (!DerivativesCache.TryGetValue((relation, filter), out result))
            {
                result = base.Derivatives(relation, filter);
                DerivativesCache.Add((relation, filter), result);
            }
            return result;
        }

        private readonly Counter ConcatCalls = new Counter();
        private readonly Counter ConcatCacheSize = new Counter();

        public override TNode Concat(TNode left, TNode right)
        {
            ConcatCalls.Increment();
            TNode result;
            if (!ConcatCache.TryGetValue((left, right), out result))
            {
                result = base.Concat(left, right);
                ConcatCache.Add((left, right), result);
                ConcatCacheSize.Increment();
            }
            return result;
        }

        protected override TNode Dominator(TNode node)
        {
            TNode result;
            if (!DominatorCache.TryGetValue(node, out result))
            {
                result = base.Dominator(node);
                DominatorCache.Add(node, result);
            }
            return result;
        }

        protected override TNode FactorOut(TNode concat, TNode right)
        {
            TNode result;
            if (!FactorOutCache.TryGetValue((concat, right), out result))
            {
                result = base.FactorOut(concat, right);
                FactorOutCache.Add((concat, right), result);
            }
            return result;
        }

        public override TNode AdjustSubstitution(TNode relation, ISubstitutionAdjustment<TWrap> adjustment)
        {
            TNode result;
            if (!AdjustSubstitutionCache.TryGetValue((relation, adjustment), out result))
            {
                result = base.AdjustSubstitution(relation, adjustment);
                AdjustSubstitutionCache.Add((relation, adjustment), result);
            }
            return result;
        }
    }

    public class MemoizingDagRelationEngineFactory : RelationEngineFactory
    {
        private readonly IWrapEngineFactory WrapEngineFactory;
        private readonly IGraphEngineFactory GraphEngineFactory;

        public MemoizingDagRelationEngineFactory(IWrapEngineFactory wrapEngineFactory, IGraphEngineFactory graphEngineFactory)
        {
            WrapEngineFactory = wrapEngineFactory;
            GraphEngineFactory = graphEngineFactory;
        }

        public override IRelationEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
            => WrapEngineFactory
                .Create(atomEngine)
                .Accept(new CreateRequest<TAtom>
                {
                    AtomEngine = atomEngine,
                    GraphEngineFactory = GraphEngineFactory,
                });

        private class CreateRequest<TAtom> : IWrapEngineVisitor<TAtom, IRelationEngine<TAtom>>
        {
            public IZero<TAtom> AtomEngine { get; set; }
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public IRelationEngine<TAtom> Visit<TValue, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
            {
                var atomicEC = IdentityEC<IAtomicRelation<TAtom>>();
                return GraphEngineFactory
                    .Create(
                        DagRelationEdge<TAtom, TWrap>.EdgeEC(engine, atomicEC),
                        DagRelationEdge<TAtom, TWrap>.MergeEC(engine, atomicEC),
                        DagRelationEdge<TAtom, TWrap>.MergeEdges(engine),
                        engine.IsIdempotent)
                    .Accept(new CreateRequest<TAtom, TWrap>
                    {
                        AtomEngine = AtomEngine,
                        WrapEngine = engine,
                        AtomicEC = atomicEC,
                    });
            }
        }

        private class CreateRequest<TAtom, TWrap> : IGraphEngineVisitor<DagRelationEdge<TAtom, TWrap>, IRelationEngine<TAtom>>
        {
            public IZero<TAtom> AtomEngine { get; set; }
            public IWrapEngine<TAtom, TWrap> WrapEngine { get; set; }
            public IEqualityComparer<IAtomicRelation<TAtom>> AtomicEC { get; set; }

            public IRelationEngine<TAtom> Visit<TNode>(IGraphEngine<DagRelationEdge<TAtom, TWrap>, TNode> engine)
                => new MemoizingDagRelationEngine<TAtom, TWrap, TNode>(AtomEngine, WrapEngine, engine, AtomicEC);
        }

        public override String ToString() =>
            $"{nameof(MemoizingDagRelationEngineFactory)}({GraphEngineFactory})";
    }
}

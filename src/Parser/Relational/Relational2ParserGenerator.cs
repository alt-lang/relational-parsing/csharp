using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;

    public class Relational2ParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.ArbitrarySemiring;

        private readonly IAtomEngineFactory AtomEngineFactory;
        private readonly IRelationOperantFactory RelationOperantFactory;
        private readonly Boolean UseLookahead;
        private readonly IReadOnlyCollection<AtomicOptimization> AtomicOptimizations;

        public Relational2ParserGenerator(IAtomEngineFactory atomEngineFactory, IRelationOperantFactory relationOperantFactory, Boolean useLookahead, params AtomicOptimization[] atomicOptimizations)
        {
            AtomEngineFactory = atomEngineFactory;
            RelationOperantFactory = relationOperantFactory;
            UseLookahead = useLookahead;
            AtomicOptimizations = atomicOptimizations;
        }

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
            => AtomEngineFactory.Create(semiring).Accept(new CreateRequest<TValue>
            {
                Grammar = grammar,
                RelationOperantFactory = RelationOperantFactory,
                UseLookahead = UseLookahead,
                AtomicOptimizations = AtomicOptimizations,
            });

        private class CreateRequest<TValue> : IAtomEngineVisitor<TValue, IParser<TValue>>
        {
            public IGrammar Grammar { get; set; }
            public IRelationOperantFactory RelationOperantFactory { get; set; }
            public Boolean UseLookahead { get; set; }
            public IReadOnlyCollection<AtomicOptimization> AtomicOptimizations { get; set; }

            public IParser<TValue> Visit<TAtom>(IAtomEngine<TValue, TAtom> engine)
                => RelationOperantFactory.Create(engine).Accept(new CreateRequest<TValue, TAtom>
                {
                    Grammar = Grammar,
                    AtomEngine = engine,
                    UseLookahead = UseLookahead,
                    AtomicOptimizations = AtomicOptimizations,
                });
        }

        private class CreateRequest<TValue, TAtom> : IRelationOperantVisitor<TValue, TAtom, IParser<TValue>>
        {
            public IGrammar Grammar { get; set; }
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }
            public Boolean UseLookahead { get; set; }
            public IReadOnlyCollection<AtomicOptimization> AtomicOptimizations { get; set; }

            public IParser<TValue> Visit<TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
            {
                var analysis = new GrammarAnalysis<TValue>(Grammar, AtomEngine.Semiring);
                var relations = ComputeRelations(analysis, AtomEngine);
                var lookaheadSets = new Dictionary<Char, HashSet<IState>>();
                foreach (var (state, symbol) in relations.StateRelations.Keys)
                    lookaheadSets.GetOrCreate(symbol).Add(state);
                IAtomicRelations<TAtom, (IState, Char)> previous = null;
                while (relations != previous)
                {
                    previous = relations;
                    foreach (var optimization in AtomicOptimizations)
                        relations = optimization.Perform(relations, AtomEngine);
                }
                return new Relational2Parser<TValue, TAtom, TRelation>(AtomEngine.Semiring, relations, operant, UseLookahead ? lookaheadSets : null);
            }
        }

        private static IAtomicRelations<TAtom, (IState, Char)> ComputeRelations<TValue, TAtom>(GrammarAnalysis<TValue> analysis, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var relations = new AtomicRelations<TAtom, (IState, Char)>();
            // handle start relation
            var epsilon = new AtomicRelation<TAtom>("", atomEngine.One);
            var startNulling = atomEngine.Zero;
            if (analysis.IsNullable[analysis.Grammar.StartState])
                startNulling = atomEngine.Embed((analysis.NullingValues[analysis.Grammar.StartState], atomEngine.Semiring.One));
            var start = new AtomicRelation<TAtom>("", startNulling);
            start.Derivatives.Add((analysis.Grammar.StartState, epsilon), atomEngine.One);
            relations.StartRelation = start;
            // form the atomic graph
            var graph = new AtomicGraph(analysis);
            // enumerate interesting nodes
            var sourceNodes = new HashSet<AtomicGraph.Node>();
            var targetNodes = new HashSet<AtomicGraph.Node>();
            foreach (var kvp in graph.TerminalNodes)
                sourceNodes.Add(kvp.Value);
            foreach (var kvp in graph.StateNodes)
                targetNodes.Add(kvp.Value);
            foreach (var kvp in graph.Derivatives)
            {
                sourceNodes.Add(kvp.Value.Item2);
                targetNodes.Add(kvp.Key);
            }
            // find path values between interesting nodes
            var pathValues = new Dictionary<AtomicGraph.Node, Dictionary<AtomicGraph.Node, TAtom>>();
            foreach (var sourceNode in sourceNodes)
            {
                // TODO: detect and report cycles
                var topological = new Stack<AtomicGraph.Node>();
                var visited = new HashSet<AtomicGraph.Node>();
                void SortReachable(AtomicGraph.Node node)
                {
                    if (visited.Add(node))
                    {
                        foreach (var neighbor in node.Keys)
                            SortReachable(neighbor);
                        topological.Push(node);
                    }
                }
                var values = new Dictionary<AtomicGraph.Node, List<TAtom>>();
                values.GetOrCreate(sourceNode).Add(atomEngine.One);
                SortReachable(sourceNode);
                foreach (var node in topological)
                {
                    var value = atomEngine.Add(values[node]);
                    if (targetNodes.Contains(node))
                        pathValues.GetOrCreate(node).Add(sourceNode, value);
                    foreach (var (neighbor, edge) in node)
                    {
                        var list = values.GetOrCreate(neighbor);
                        switch (edge)
                        {
                            case AtomicGraph.Edge.Nulling nulling:
                                var prefix = nulling.Prefix != null ? analysis.NullingValues[nulling.Prefix] : analysis.Semiring.One;
                                var suffix = nulling.Suffix != null ? analysis.NullingValues[nulling.Suffix] : analysis.Semiring.One;
                                list.Add(atomEngine.Multiply(value, (prefix, suffix)));
                                break;
                            case AtomicGraph.Edge.Single single:
                                list.Add(atomEngine.Multiply(value, (analysis.Semiring.One, analysis.Semiring.Atomic(single.Transition))));
                                break;
                        }
                    }
                }
            }
            // gather dependencies between relevant nodes
            var nodeState = new Dictionary<AtomicGraph.Node, IState>();
            foreach (var kvp in graph.StateNodes)
                nodeState.Add(kvp.Value, kvp.Key);
            var implied = new Dictionary<AtomicGraph.Node, List<AtomicGraph.Node>>();
            var relevant = new Dictionary<IState, HashSet<AtomicGraph.Node>>();
            foreach (var (target, sources) in pathValues)
            {
                if (graph.Derivatives.TryGetValue(target, out var pair))
                    implied.GetOrCreate(pair.Item2).AddRange(sources.Keys);
                if (nodeState.TryGetValue(target, out var state))
                    foreach (var source in sources.Keys)
                        relevant.GetOrCreate(state).Add(source);
            }
            var relevantStates = new Dictionary<AtomicGraph.Node, List<IState>>();
            foreach (var (state, nodes) in relevant)
            {
                nodes.CloseUnder(implied);
                foreach (var node in nodes)
                    relevantStates.GetOrCreate(node).Add(state);
            }
            // prepare automata for each final state
            var partial = new Dictionary<IState, Dictionary<AtomicGraph.Node, AtomicRelation<TAtom>>>();
            // create epsilon nodes
            foreach (var (target, sources) in pathValues)
                if (nodeState.TryGetValue(target, out var state))
                    foreach (var (source, atom) in sources)
                        partial.GetOrCreate(state).Add(source, new AtomicRelation<TAtom>("", atom));
            // create epsilon-free nodes
            foreach (var (state, nodes) in relevant)
                foreach (var node in nodes)
                    if (!partial[state].ContainsKey(node))
                        partial[state].Add(node, new AtomicRelation<TAtom>("", atomEngine.Zero));
            // add derivative entries
            foreach (var (target, sources) in pathValues)
                if (graph.Derivatives.TryGetValue(target, out var pair) && analysis.ProductiveStates.Contains(pair.Item1))
                    foreach (var (source, atom) in sources)
                        foreach (var state in relevantStates[source])
                            if (partial[state].TryGetValue(pair.Item2, out var next))
                                partial[state][source].Derivatives.Add((pair.Item1, next), atom);
            // record entry points
            var nodeTerminals = new Dictionary<AtomicGraph.Node, Char>();
            foreach (var (terminal, node) in graph.TerminalNodes)
                nodeTerminals.Add(node, terminal);
            foreach (var (state, part) in partial)
                foreach (var (node, relation) in part)
                    if (nodeTerminals.TryGetValue(node, out var terminal))
                        relations.StateRelations.Add((state, terminal), relation);
            return relations;
        }

        public override String ToString() =>
            $"{nameof(Relational2ParserGenerator)}({AtomEngineFactory},{RelationOperantFactory},{UseLookahead}{String.Join("", AtomicOptimizations.Select(o => $",{o}"))})";
    }
}

using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    public interface ISubstitutionList<TWrap>
    {
        Int32 Count { get; }
        Int32 IdentityDepth { get; }
        Int32 IdentityCount { get; }
        TWrap this[Int32 index] { get; }
    }

    public class NoSubstitutionList<TWrap> : ISubstitutionList<TWrap>
    {
        public Int32 Count
            => 0;
        public Int32 IdentityDepth
            => -1;
        public Int32 IdentityCount
            => 0;
        public TWrap this[Int32 index]
            => throw new IndexOutOfRangeException();

        public static readonly ISubstitutionList<TWrap> Instance
            = new NoSubstitutionList<TWrap>();
    }

    public class SubstitutionList<TWrap> : ISubstitutionList<TWrap>
    {
        public Int32 Count { get; }
        public Int32 IdentityDepth { get; }
        public Int32 IdentityCount { get; }
        private readonly List<SubstitutionList<TWrap>> Jumps;
        private readonly IReadOnlyList<TWrap> Suffix;

        public SubstitutionList(SubstitutionList<TWrap> previous, IReadOnlyList<TWrap> suffix)
        {
            Count = previous?.Count ?? 0;
            IdentityDepth = previous?.IdentityDepth ?? -1;
            IdentityCount = previous?.IdentityCount ?? 0;
            Jumps = new List<SubstitutionList<TWrap>>();
            for (var rank = 0; previous != null; rank++)
            {
                Jumps.Add(previous);
                previous = (previous.Jumps.Count > rank) ? previous.Jumps[rank] : null;
            }
            Suffix = suffix;
            if (IdentityCount == Count)
                foreach (var wrap in Suffix)
                {
                    if (!(wrap is ISubstituteWrap substitute))
                        break;
                    if (IdentityDepth < 0)
                        IdentityDepth = substitute.DependencyDepth;
                    else if (IdentityDepth != substitute.DependencyDepth)
                        break;
                    if (IdentityCount != substitute.ContentIndex)
                        break;
                    IdentityCount++;
                }
            Count += Suffix.Count;
        }

        public TWrap this[Int32 index]
        {
            get
            {
                var current = this;
                for (var rank = Jumps.Count - 1; rank >= 0; rank--)
                    if (current.Jumps.Count > rank)
                    {
                        var previous = current.Jumps[rank];
                        if (previous.Count > index)
                            current = previous;
                    }
                return current.Suffix[index - (current.Count - current.Suffix.Count)];
            }
        }
    }
}

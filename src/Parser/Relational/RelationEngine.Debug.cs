namespace Parser.Relational
{
    using System.Collections.Generic;
    using Debug;

    partial class RelationEngine<TAtom, TWrap, TRelation, TOuterRelation>
    {
        partial class Operant<TValue> : IAnalyzer<TOuterRelation>
        {
            IEqualityComparer<TOuterRelation> IAnalyzer<TOuterRelation>.Comparer
                => ((IAnalyzer<TOuterRelation>) RelationEngine).Comparer;

            IEnumerable<IAnalyzable> IAnalyzer<TOuterRelation>.Constituents(TOuterRelation value)
                => ((IAnalyzer<TOuterRelation>) RelationEngine).Constituents(value);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Caching;
    using Utilities;
    using static EqualityComparerFactories;

    public interface ISubstituteWrap
    {
        Int32 DependencyDepth { get; }
        Int32 ContentIndex { get; }
    }

    public interface ISubstitutionWrapEngine<TAtom> : IWrapEngine<TAtom>
    {
        TResult Accept<TResult>(ISubstitutionWrapEngineVisitor<TAtom, TResult> visitor);
    }

    public interface ISubstitutionWrapEngineVisitor<TAtom, out TResult>
    {
        TResult Visit<TValue, TWrap>(ISubstitutionWrapEngine<TValue, TAtom, TWrap> engine);
    }

    public interface ISubstitutionWrapEngine<TAtom, TWrap> : IWrapEngine<TAtom, TWrap>, ISubstitutionWrapEngine<TAtom>
    {
        TWrap Substitute(Int32 contentIndex);
        TWrap SetDepth(TWrap substitution, Int32 depth);
        TWrap ShiftIndex(TWrap substitution, Int32 offset);
        TWrap Resolve<TContext>(TWrap wrap, TContext context)
            where TContext : class, ISubstitutionContext<TWrap, TContext>;
    }

    public interface ISubstitutionWrapEngine<TValue, TAtom, TWrap> : IWrapEngine<TValue, TAtom, TWrap>, ISubstitutionWrapEngine<TAtom, TWrap>
    {
    }

    public abstract class SubstitutionWrap<TAtom>
    {
        public static IEqualityComparer<SubstitutionWrap<TAtom>> Comparer { get; }
            = IdentityEC<SubstitutionWrap<TAtom>>();

        public abstract override String ToString();

        public class Add : SubstitutionWrap<TAtom>
        {
            public IReadOnlyCollection<SubstitutionWrap<TAtom>> Children { get; }

            public Add(IReadOnlyCollection<SubstitutionWrap<TAtom>> children)
            {
                Children = children;
            }

            public override String ToString()
                => Children.Count > 0 ? $"({String.Join("+", Children)})" : "(0)";

            public static new IEqualityComparer<Add> Comparer
                => new ComponentsEqualityComparer<Add>
                {
                    { _ => _.Children, MultiSetEC(SubstitutionWrap<TAtom>.Comparer) },
                };
        }

        public class Atom : SubstitutionWrap<TAtom>
        {
            public TAtom Value { get; }

            public Atom(TAtom value)
            {
                Value = value;
            }

            public override String ToString()
                => Value.ToString();

            public static new IEqualityComparer<Atom> Comparer(IEqualityComparer<TAtom> atomComparer)
                => new ComponentsEqualityComparer<Atom>
                {
                    { _ => _.Value, atomComparer },
                };
        }

        public class Fold : SubstitutionWrap<TAtom>
        {
            public SubstitutionWrap<TAtom> Child { get; }

            public Fold(SubstitutionWrap<TAtom> child)
            {
                Child = child;
            }

            public override String ToString()
                => $"({Child})^";

            public static new IEqualityComparer<Fold> Comparer
                => new ComponentsEqualityComparer<Fold>
                {
                    { _ => _.Child, SubstitutionWrap<TAtom>.Comparer },
                };
        }

        public class Multiply : SubstitutionWrap<TAtom>
        {
            public IReadOnlyList<SubstitutionWrap<TAtom>> Children { get; }

            public Multiply(IReadOnlyList<SubstitutionWrap<TAtom>> children)
            {
                Children = children;
            }

            public override String ToString()
                => Children.Count > 0 ? $"({String.Join("*", Children)})" : "(1)";

            public static new IEqualityComparer<Multiply> Comparer(Boolean isCommutative)
                => new ComponentsEqualityComparer<Multiply>
                {
                    { _ => _.Children, false ? MultiSetEC(SubstitutionWrap<TAtom>.Comparer) : SequenceEC(SubstitutionWrap<TAtom>.Comparer) },
                };
        }

        public class Substitute : SubstitutionWrap<TAtom>, ISubstituteWrap
        {
            public Int32 DependencyDepth { get; }
            public Int32 ContentIndex { get; }

            public Substitute(Int32 dependencyDepth, Int32 contentIndex)
            {
                DependencyDepth = dependencyDepth;
                ContentIndex = contentIndex;
            }

            public override String ToString()
                => $"S({DependencyDepth}, {ContentIndex})";

            public static new IEqualityComparer<Substitute> Comparer
                => new ComponentsEqualityComparer<Substitute>
                {
                    { _ => _.DependencyDepth, OperatorEC<Int32>() },
                    { _ => _.ContentIndex, OperatorEC<Int32>() },
                };
        }
    }

    public abstract class SubstitutionWrapEngine<TValue, TAtom> : WrapEngine<TValue, TAtom, SubstitutionWrap<TAtom>>, ISubstitutionWrapEngine<TValue, TAtom, SubstitutionWrap<TAtom>>
    {
        private readonly IZero<TAtom> AtomEngine;
        public sealed override Boolean IsCommutative { get; }
        public sealed override Boolean IsIdempotent { get; }
        private readonly ICache<SubstitutionWrap<TAtom>.Add> AddCache;
        private readonly ICache<SubstitutionWrap<TAtom>.Atom> AtomCache;
        private readonly ICache<SubstitutionWrap<TAtom>.Fold> FoldCache;
        private readonly ICache<SubstitutionWrap<TAtom>.Multiply> MultiplyCache;
        private readonly ICache<SubstitutionWrap<TAtom>.Substitute> SubstituteCache;

        public SubstitutionWrapEngine(IZero<TAtom> atomEngine, Boolean isCommutative, Boolean isIdempotent, ICacheStrategy cacheStrategy)
        {
            AtomEngine = atomEngine;
            IsCommutative = isCommutative;
            IsIdempotent = isIdempotent;
            AddCache = cacheStrategy.Create(SubstitutionWrap<TAtom>.Add.Comparer);
            AtomCache = cacheStrategy.Create(SubstitutionWrap<TAtom>.Atom.Comparer(atomEngine.Comparer));
            FoldCache = cacheStrategy.Create(SubstitutionWrap<TAtom>.Fold.Comparer);
            MultiplyCache = cacheStrategy.Create(SubstitutionWrap<TAtom>.Multiply.Comparer(IsCommutative));
            SubstituteCache = cacheStrategy.Create(SubstitutionWrap<TAtom>.Substitute.Comparer);
        }

        public override Boolean IsZero(SubstitutionWrap<TAtom> value)
            => value is SubstitutionWrap<TAtom>.Add add && add.Children.Count == 0;

        public override Boolean IsOne(SubstitutionWrap<TAtom> value)
            => value is SubstitutionWrap<TAtom>.Multiply multiply && multiply.Children.Count == 0;

        public override SubstitutionWrap<TAtom> Add(IEnumerable<SubstitutionWrap<TAtom>> values)
        {
            var children = IsIdempotent ? new HashSet<SubstitutionWrap<TAtom>>(SubstitutionWrap<TAtom>.Comparer) : new List<SubstitutionWrap<TAtom>>() as ICollection<SubstitutionWrap<TAtom>>;
            foreach (var value in values)
            {
                if (IsZero(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 1)
                return children.Single();
            return AddCache.Add(new SubstitutionWrap<TAtom>.Add(children as IReadOnlyCollection<SubstitutionWrap<TAtom>>));
        }

        public override SubstitutionWrap<TAtom> Atom(TAtom atom)
        {
            System.Diagnostics.Debug.Assert(!AtomEngine.IsZero(atom));
            return AtomCache.Add(new SubstitutionWrap<TAtom>.Atom(atom));
        }

        public override SubstitutionWrap<TAtom> Fold(SubstitutionWrap<TAtom> wrap)
        {
            if (IsZero(wrap) || IsOne(wrap) || wrap is SubstitutionWrap<TAtom>.Fold)
                return wrap;
            return FoldCache.Add(new SubstitutionWrap<TAtom>.Fold(wrap));
        }

        public override SubstitutionWrap<TAtom> One
            => MultiplyCache.Add(new SubstitutionWrap<TAtom>.Multiply(Array.Empty<SubstitutionWrap<TAtom>>()));

        public override SubstitutionWrap<TAtom> Multiply(SubstitutionWrap<TAtom> left, SubstitutionWrap<TAtom> right)
        {
            if (IsZero(left) || IsZero(right))
                return Zero;
            if (IsOne(left))
                return right;
            if (IsOne(right))
                return left;
            return MultiplyCache.Add(new SubstitutionWrap<TAtom>.Multiply(new[] { left, right }));
        }

        private SubstitutionWrap<TAtom> Substitute(Int32 dependencyDepth, Int32 contentIndex)
            => SubstituteCache.Add(new SubstitutionWrap<TAtom>.Substitute(dependencyDepth, contentIndex));

        public SubstitutionWrap<TAtom> Substitute(Int32 contentIndex)
            => Substitute(0, contentIndex);

        public SubstitutionWrap<TAtom> SetDepth(SubstitutionWrap<TAtom> wrap, Int32 depth)
        {
            switch (wrap)
            {
                case SubstitutionWrap<TAtom>.Add add:
                    return Add(add.Children.Select(_ => SetDepth(_, depth)));
                case SubstitutionWrap<TAtom>.Fold fold:
                    return Fold(SetDepth(fold.Child, depth));
                case SubstitutionWrap<TAtom>.Multiply multiply:
                    return Multiply(multiply.Children.Select(_ => SetDepth(_, depth)));
                case SubstitutionWrap<TAtom>.Substitute substitute:
                    if (substitute.DependencyDepth != 0)
                        throw new InvalidProgramException();
                    return Substitute(depth, substitute.ContentIndex);
                default:
                    return wrap;
            }
        }

        public SubstitutionWrap<TAtom> ShiftIndex(SubstitutionWrap<TAtom> wrap, Int32 offset)
            => (wrap is SubstitutionWrap<TAtom>.Substitute substitute) ?
                Substitute(substitute.DependencyDepth, substitute.ContentIndex + offset) :
                throw new InvalidProgramException();

        public abstract SubstitutionWrap<TAtom> Resolve<TContext>(SubstitutionWrap<TAtom> wrap, TContext context)
            where TContext : class, ISubstitutionContext<SubstitutionWrap<TAtom>, TContext>;

        TResult ISubstitutionWrapEngine<TAtom>.Accept<TResult>(ISubstitutionWrapEngineVisitor<TAtom, TResult> visitor)
            => visitor.Visit(this);
    }
}

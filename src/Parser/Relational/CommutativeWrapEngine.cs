using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Algebra;

    public partial class CommutativeWrapEngine<TValue> : IWrapEngine<TValue, TValue, TValue>
    {
        public ISemiring<TValue> Semiring {get; }

        public CommutativeWrapEngine(ISemiring<TValue> semiring)
        {
            if (!semiring.IsCommutative)
                throw new ArgumentException($"{nameof(CommutativeWrapEngine<TValue>)} requires a commutative semiring", nameof(semiring));
            Semiring = semiring;
        }

        public Boolean IsCommutative
            => true;

        public Boolean IsIdempotent
            => Semiring.IsIdempotent;

        public IEqualityComparer<TValue> Comparer
            => Semiring.Comparer;

        public TValue Zero
            => Semiring.Zero;

        public Boolean IsZero(TValue value)
            => Semiring.IsZero(value);

        public TValue Add(TValue left, TValue right)
            => Semiring.Add(left, right);

        public TValue Add(IEnumerable<TValue> values)
            => Semiring.Add(values);

        public TValue One
            => Semiring.One;

        public Boolean IsOne(TValue value)
            => Semiring.IsOne(value);

        public TValue Multiply(TValue left, TValue right)
            => Semiring.Multiply(left, right);

        public TValue Multiply(IEnumerable<TValue> values)
            => Semiring.Multiply(values);

        public TValue Atom(TValue atom)
            => atom;

        public TValue Flatten(TValue wrap)
            => wrap;

        public TValue Fold(TValue wrap)
            => wrap;

        TResult IWrapEngine<TValue>.Accept<TResult>(IWrapEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }

    public class CommutativeWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IWrapEngineFactory Fallback;

        public CommutativeWrapEngineFactory(IWrapEngineFactory fallback)
        {
            Fallback = fallback;
        }

        public IWrapEngine<TAtom> Create<TValue, TAtom>(IAtomEngine<TValue, TAtom> atomEngine)
        {
            if (atomEngine is CommutativeAtomEngine<TValue> commutative)
                return (IWrapEngine<TAtom>) new CommutativeWrapEngine<TValue>(commutative.Semiring);
            return Fallback.Create(atomEngine);
        }
    }
}

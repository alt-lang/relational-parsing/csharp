using System;

namespace Parser
{
    public interface IParserGenerator
    {
        ParserFeatures Features { get; }
        IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring);
        IParser<Int64> CreateCounter(IGrammar grammar);
        IParser<Boolean> CreateRecognizer(IGrammar grammar);
    }

    public abstract class ParserGenerator : IParserGenerator
    {
        public abstract ParserFeatures Features { get; }

        public virtual IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring) =>
            throw new NotSupportedException($"Parsing with semiring {typeof(TValue).Name} not supported.");

        public virtual IParser<Int64> CreateCounter(IGrammar grammar) =>
            CreateParser(grammar, new Valuations.CountingSemiring());

        public virtual IParser<Boolean> CreateRecognizer(IGrammar grammar) =>
            CreateParser(grammar, new Valuations.BooleanSemiring());
    }
}

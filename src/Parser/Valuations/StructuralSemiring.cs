using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Valuations
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public class StructuralSemiring : FreeSemiring, IValuationSemiring<FreeValue>
    {
        public override Boolean IsIdempotent
            => true;

        public StructuralSemiring()
        {
            AtomicMemoizer = new Memoizer<AtomicValue>(new ComponentsEqualityComparer<AtomicValue>
            {
                { _ => _.Transition, Transition.Comparer },
            });
        }

        public class AtomicValue : FreeValue
        {
            public Transition Transition;

            public override String Description
                => Transition.ToString();
        }

        private readonly Memoizer<AtomicValue> AtomicMemoizer;

        public FreeValue Atomic(Transition transition)
            => AtomicMemoizer.Add(new AtomicValue { Transition = transition });
    }
}

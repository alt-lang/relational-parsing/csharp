using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Valuations
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public class ShapeSemiring : ValuationSemiring<ShapeSemiring.Shape>
    {
        public class Shape
        {
            public class Add : Shape
            {
                public IReadOnlyCollection<Shape> Children;
            }

            public class Multiply : Shape
            {
                public IReadOnlyCollection<Shape> Children;
            }

            public static readonly Shape Call = new Shape();
            public static readonly Shape Shift = new Shape();
            public static readonly Shape Reduce = new Shape();
        }

        private Int32 MemoizationLimit;
        private readonly Memoizer<Shape.Add> AddMemoizer;
        private readonly Memoizer<Shape.Multiply> MultiplyMemoizer;

        public ShapeSemiring(Int32 memoizationLimit = 1000)
        {
            MemoizationLimit = memoizationLimit;
            AddMemoizer = new Memoizer<Shape.Add>(new ComponentsEqualityComparer<Shape.Add>
            {
                { _ => _.Children, MultiSetEC(Comparer) },
            }, _ => MemoizationLimit--);
            MultiplyMemoizer = new Memoizer<Shape.Multiply>(new ComponentsEqualityComparer<Shape.Multiply>
            {
                { _ => _.Children, SequenceEC(Comparer) },
            }, _ => MemoizationLimit--);
            Zero = new Shape.Add { Children = Array.Empty<Shape>() };
            One = new Shape.Multiply { Children = Array.Empty<Shape>() };
        }

        public override Boolean IsIdempotent
            => true;

        public override Shape Atomic(Transition transition)
        {
            switch (transition)
            {
                case Transition.Call call:
                    return Shape.Call;
                case Transition.Shift shift:
                    return Shape.Shift;
                case Transition.Reduce reduce:
                    return Shape.Reduce;
            }
            throw new InvalidOperationException();
        }

        public override Shape Zero { get; }

        public override Shape Add(IEnumerable<Shape> values)
        {
            var children = new HashSet<Shape>(Comparer);
            foreach (var value in values)
            {
                if (IsZero(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 0)
                return Zero;
            if (children.Count == 1)
                return children.Single();
            var result = new Shape.Add { Children = children };
            return MemoizationLimit > 0 ? AddMemoizer.Add(result) : result;
        }

        public override Shape One { get; }

        public override Shape Multiply(IEnumerable<Shape> values)
        {
            var children = new List<Shape>();
            foreach (var value in values)
            {
                if (IsZero(value))
                    return value;
                if (IsOne(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 0)
                return One;
            if (children.Count == 1)
                return children[0];
            var result = new Shape.Multiply { Children = children };
            return MemoizationLimit > 0 ? MultiplyMemoizer.Add(result) : result;
        }
    }
}

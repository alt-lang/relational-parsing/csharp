using System;
using System.Collections.Generic;

namespace Parser
{
    public interface IParser<TValue>
    {
        IValuationSemiring<TValue> Semiring { get; }
        TValue Run(IEnumerable<Char> input, Action step);
    }

    public static class ParserExtensions
    {
        public static TValue Run<TValue>(this IParser<TValue> parser, IEnumerable<Char> input) =>
            parser.Run(input, () => {});
    }
}

using System;
using System.Collections.Generic;

namespace Parser.Utilities
{
    public static class DictionaryExtensions
    {
        public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> factory)
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = factory(key);
                dictionary.Add(key, value);
            }
            return value;
        }

        public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
            where TValue : new()
            => dictionary.GetOrCreate(key, _ => new TValue());
    }
}

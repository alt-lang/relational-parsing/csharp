using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Parser.Utilities
{
    public abstract class Measurement
    {
        protected abstract Boolean HasValue { get; }
        protected abstract String Value { get; }
        protected abstract void Reset();

        private String Name;

        protected Measurement()
        {
            All.Add(this);
        }

        private static readonly HashSet<Measurement> All
            = new HashSet<Measurement>();

        [Conditional("DEBUG")]
        public static void AssignNames(Object root, String rootName)
        {
            var visited = new HashSet<Object>();
            var queue = new Queue<(Object, String)>();
            if (visited.Add(root))
                queue.Enqueue((root, rootName));
            while (queue.Count > 0)
            {
                var (node, name) = queue.Dequeue();
                if (node is Measurement measurement)
                    measurement.Name = name;
                var fields = new HashSet<String>();
                for (var type = node.GetType(); type != null; type = type.BaseType)
                    foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                        if (field.FieldType.Namespace.StartsWith("Parser") && fields.Add(field.Name))
                        {
                            var fieldName = field.Name;
                            if (fieldName.EndsWith("k__BackingField"))
                                fieldName = fieldName.Substring(1, fieldName.Length - 17);
                            var target = field.GetValue(node);
                            if (target != null && visited.Add(target))
                                queue.Enqueue((target, $"{name}({type.Name}).{fieldName}"));
                        }
            }
        }

        [Conditional("DEBUG")]
        public static void PrintAll(TextWriter writer = null)
            => (writer ?? Console.Out).WriteLine(String.Join("\n", All.Where(_ => _.Name != null && _.HasValue).Select(_ => $"{_.Name} : {_.Value}")));

        [Conditional("DEBUG")]
        public static void ResetAll()
        {
            foreach (var counter in All)
                counter.Reset();
        }
    }

    public sealed class Counter : Measurement
    {
        private Int64 value;

        public Counter()
        {
            value = 0L;
        }

        protected override Boolean HasValue
            => value != 0;

        protected override String Value
            => value.ToString();

        protected override void Reset()
            => value = 0;

        [Conditional("DEBUG")]
        public void Increment(Int64 change = 1L)
        {
            Cancellation.Check();
            Interlocked.Add(ref value, change);
        }
    }

    public sealed class Histogram : Measurement
    {
        private readonly Dictionary<Int64, Int64> counts;

        public Histogram()
        {
            counts = new Dictionary<Int64, Int64>();
        }

        protected override Boolean HasValue
            => counts.Count > 0;

        protected override String Value
            => "[" + String.Join(", ", counts.OrderBy(_ => _.Key).Select(_ => $"{_.Key}: {_.Value}")) + "]";

        protected override void Reset()
            => counts.Clear();

        [Conditional("DEBUG")]
        public void Add(Int64 value, Int64 change = 1)
        {
            Cancellation.Check();
            counts[value] = (counts.TryGetValue(value, out var count) ? count : 0) + change;
        }
    }
}

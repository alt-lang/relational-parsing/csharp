using System;
using System.Collections.Generic;

namespace Parser.Utilities
{
    public static class HashSetExtensions
    {
        public static Int32 CloseUnder<TNode, TNeighbors>(this HashSet<TNode> nodes, IReadOnlyDictionary<TNode, TNeighbors> implied)
            where TNeighbors : IEnumerable<TNode>
        {
            var queue = new Queue<TNode>(nodes);
            while (queue.Count > 0)
                if (implied.TryGetValue(queue.Dequeue(), out var neighbors))
                    foreach (var neighbor in neighbors)
                        if (nodes.Add(neighbor))
                            queue.Enqueue(neighbor);
            return nodes.Count;
        }
    }
}

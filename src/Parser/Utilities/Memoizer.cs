using System;
using System.Collections.Generic;

namespace Parser.Utilities
{
    public class Memoizer<T> : Dictionary<T, T>
    {
        private readonly Action<T> WithNew;

        public Memoizer(IEqualityComparer<T> comparer, Action<T> withNew = null)
            : base(comparer)
        {
            WithNew = withNew ?? (_ => { });
        }

        public T Add(T value)
        {
            if (TryGetValue(value, out var previous))
                return previous;
            base.Add(value, value);
            WithNew(value);
            return value;
        }
    }
}

using System;
using System.Collections.Generic;

namespace Parser.Common
{
    using Valuations;

    public sealed class OptimizingParserGenerator : ParserGenerator
    {
        private readonly ParserGenerator Inner;
        private readonly IReadOnlyCollection<GrammarOptimization> Optimizations;

        public OptimizingParserGenerator(ParserGenerator inner, params GrammarOptimization[] optimizations)
        {
            if (optimizations.Length == 0)
                throw new ArgumentException("Please specify at least one optimization", nameof(optimizations));
            Inner = inner;
            Optimizations = optimizations;
        }

        public override ParserFeatures Features
            => Inner.Features;

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
            => Inner.CreateParser(Optimize(grammar, semiring), semiring);

        public override IParser<Int64> CreateCounter(IGrammar grammar)
            => Inner.CreateCounter(Optimize(grammar, new CountingSemiring()));

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar)
            => Inner.CreateRecognizer(Optimize(grammar, new BooleanSemiring()));

        private IGrammar Optimize<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
        {
            IGrammar previous = null;
            while (grammar != previous)
            {
                previous = grammar;
                foreach (var optimization in Optimizations)
                    grammar = optimization.Perform(grammar, semiring);
            }
            return grammar;
        }

        public override String ToString()
            => $"{nameof(OptimizingParserGenerator)}({Inner},{String.Join(",", Optimizations)})";
    }
}

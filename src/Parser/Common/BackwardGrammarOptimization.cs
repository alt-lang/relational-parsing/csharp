using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Common
{
    using Utilities;
    using static EqualityComparerFactories;

    public class BackwardGrammarOptimization : GrammarOptimization
    {
        public override IGrammar Perform<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
        {
            var classification = new Classifier<TValue>(grammar, semiring).Run();
            if (classification == Classifier<TValue>.NoClassification)
                return grammar;
            var expectedCalls = new Dictionary<(GroupedState, GroupedState, GroupedState, TValue), Int32>(ValueTupleEC(GroupedStateEC, GroupedStateEC, GroupedStateEC, semiring.Comparer));
            var expectedShifts = new Dictionary<(GroupedState, Char, GroupedState, TValue), Int32>(ValueTupleEC(GroupedStateEC, OperatorEC<Char>(), GroupedStateEC, semiring.Comparer));
            foreach (var (source, newSource) in classification)
                foreach (var transition in source.Transitions)
                    switch (transition)
                    {
                        case Transition.Call call:
                        {
                            var newChild = classification[call.Child];
                            var newNext = classification[call.Next];
                            var key = (newSource, newChild, newNext, semiring.Atomic(call));
                            if (expectedCalls.GetOrCreate(key) == 0)
                            {
                                System.Diagnostics.Debug.Assert(newChild.Count == 1 || newNext.Count == 1);
                                expectedCalls[key] = newChild.Count * newNext.Count;
                                newSource.Transitions.Add(new Transition.Call(newSource, newChild, newNext));
                            }
                            expectedCalls[key] -= 1;
                            break;
                        }
                        case Transition.Shift shift:
                        {
                            var newNext = classification[shift.Next];
                            var key = (newSource, shift.Terminal, newNext, semiring.Atomic(shift));
                            if (expectedShifts.GetOrCreate(key) == 0)
                            {
                                expectedShifts[key] = newNext.Count;
                                newSource.Transitions.Add(new Transition.Shift(newSource, shift.Terminal, newNext));
                            }
                            expectedShifts[key] -= 1;
                            break;
                        }
                        case Transition.Reduce reduce:
                        {
                            newSource.Transitions.Add(new Transition.Reduce(newSource));
                            break;
                        }
                    }
            if (expectedCalls.Values.Concat(expectedShifts.Values).Any(_ => _ > 0))
                throw new InvalidProgramException();
            return new GroupedGrammar { StartState = classification[grammar.StartState], StopState = classification[grammar.StopState] };
        }

        private class Signature<TValue>
        {
            public Boolean IsStart = false;
            public List<(GroupedState, IState, TValue)> CallChild = new List<(GroupedState, IState, TValue)>();
            public List<(GroupedState, IState, TValue)> CallNext = new List<(GroupedState, IState, TValue)>();
            public List<(GroupedState, Char, TValue)> ShiftNext = new List<(GroupedState, Char, TValue)>();
        }

        private class Classifier<TValue> : Classifier<IState, Signature<TValue>, GroupedState>
        {
            private readonly GrammarAnalysis Analysis;
            private readonly IValuationSemiring<TValue> Semiring;
            protected override IEqualityComparer<Signature<TValue>> SignatureEC { get; }

            public Classifier(IGrammar grammar, IValuationSemiring<TValue> semiring)
            {
                Analysis = new GrammarAnalysis(grammar);
                Semiring = semiring;
                var stateEC = IdentityEC<IState>();
                SignatureEC = new ComponentsEqualityComparer<Signature<TValue>>
                {
                    { _ => _.IsStart, OperatorEC<Boolean>() },
                    { _ => _.CallChild, MultiSetEC(ValueTupleEC(GroupedStateEC, stateEC, Semiring.Comparer)) },
                    { _ => _.CallNext, MultiSetEC(ValueTupleEC(GroupedStateEC, stateEC, Semiring.Comparer)) },
                    { _ => _.ShiftNext, MultiSetEC(ValueTupleEC(GroupedStateEC, OperatorEC<Char>(), Semiring.Comparer)) },
                };
            }

            protected override IEnumerable<IState> Elements
                => Analysis.AllStates;


            protected override IEnumerable<(IState, Signature<TValue>)> Signatures(IReadOnlyDictionary<IState, GroupedState> classification)
            {
                var result = new Dictionary<IState, Signature<TValue>>();
                result.GetOrCreate(Analysis.Grammar.StartState).IsStart = true;
                result.GetOrCreate(Analysis.Grammar.StopState);
                foreach (var (source, sourceClass) in classification)
                    foreach (var transition in source.Transitions)
                        switch (transition)
                        {
                            case Transition.Call call:
                            {
                                var childClass = classification[call.Child];
                                var nextClass = classification[call.Next];
                                var value = Semiring.Atomic(call);
                                result.GetOrCreate(call.Child).CallChild.Add((sourceClass, call.Next, value));
                                result.GetOrCreate(call.Next).CallNext.Add((sourceClass, call.Child, value));
                                break;
                            }
                            case Transition.Shift shift:
                            {
                                var value = Semiring.Atomic(shift);
                                result.GetOrCreate(shift.Next).ShiftNext.Add((sourceClass, shift.Terminal, value));
                                break;
                            }
                        }
                foreach (var (state, signature) in result)
                    yield return (state, signature);
            }

            public override String ToString()
                => $"{nameof(BackwardGrammarOptimization)}";
        }

        public override String ToString()
            => $"{nameof(BackwardGrammarOptimization)}";
    }
}

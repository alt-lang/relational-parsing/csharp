using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Common
{
    using Relational;
    using Utilities;

    public class EpsilonGrammarOptimization : GrammarOptimization
    {
        public override IGrammar Perform<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
        {
            var analysis = new GrammarAnalysis<TValue>(grammar, semiring);
            var pureEpsilon = new HashSet<IState>(analysis.AllStates);
            pureEpsilon.ExceptWith(analysis.ProductiveStates);
            pureEpsilon.RemoveWhere(state => !analysis.NullingValues.TryGetValue(state, out var value) || !semiring.IsOne(value));
            if (!analysis.CallTransitions.Any(call => pureEpsilon.Contains(call.Child)))
                return grammar;
            var stateMap = new Dictionary<IState, State>();
            foreach (var state in analysis.AllStates)
                stateMap.Add(state, new State(state.Name));
            void AddTransitions(IState source, State target)
            {
                foreach (var transition in source.Transitions)
                    switch (transition)
                    {
                        case Transition.Call call:
                            if (pureEpsilon.Contains(call.Child))
                                AddTransitions(call.Next, target);
                            else
                                target.Transitions.Add(new Transition.Call(target, stateMap[call.Child], stateMap[call.Next]));
                            break;
                        case Transition.Shift shift:
                            target.Transitions.Add(new Transition.Shift(target, shift.Terminal, stateMap[shift.Next]));
                            break;
                        case Transition.Reduce reduce:
                            target.Transitions.Add(new Transition.Reduce(target, reduce.Label));
                            break;
                    }
            };
            foreach (var (source, target) in stateMap)
                AddTransitions(source, target);
            var newGrammar = new Grammar { StartState = stateMap[grammar.StartState], StopState = grammar.StopState };
            var newAnalysis = new GrammarAnalysis(newGrammar);
            Console.Write($"[{nameof(EpsilonGrammarOptimization)}: {analysis.AllStates.Count} -> {newAnalysis.AllStates.Count}] ");
            return newGrammar;
        }

        private class State : IState
        {
            public String Name { get; }

            public State(String name)
            {
                Name = name;
            }

            public readonly List<Transition> Transitions
                = new List<Transition>();
            IEnumerable<Transition> IState.Transitions
                => Transitions;

            public Boolean Equals(IState other)
                => ReferenceEquals(this, other);
        }

        private class Grammar : IGrammar
        {
            public IState StartState { get; set; }
            public IState StopState { get; set; }
        }

        public override String ToString()
            => nameof(EpsilonGrammarOptimization);
    }
}

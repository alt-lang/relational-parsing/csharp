﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parser.Grammars
{
    public class SampleGrammar : SimpleGrammar
    {
        public String Name { get; }
        public List<(String, Int64?)> Inputs { get; }

        private SampleGrammar(String name)
        {
            Name = name;
            Inputs = new List<(String, Int64?)>();
        }

        public void Add(String input, Int64? result) =>
            Inputs.Add((input, result));

        public override String ToString() =>
            Name;

        public static SampleGrammar Trivial { get; } =
            new SampleGrammar(nameof(Trivial))
            {
                R(0),
                { "", 1 },
                { "a", 0 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 0 },
            };

        public static SampleGrammar FlatIteration { get; } =
            new SampleGrammar(nameof(FlatIteration))
            {
                R(0), S(0,'a',0),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar LeftRecursion { get; } =
            new SampleGrammar(nameof(LeftRecursion))
            {
                C(0,0,1), R(0), S(1,'a',2), R(2),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar RightRecursion { get; } =
            new SampleGrammar(nameof(RightRecursion))
            {
                S(0,'a',1), R(0), C(1,0,2), R(2),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar DualRecursion { get; } =
            new SampleGrammar(nameof(DualRecursion))
            {
                C(0,0,1), S(0,'a',3), S(1,'b',2), C(2,0,3), R(3),
                { "", 0 },
                { "a", 1 },
                { "aba", 1 },
                { "ababa", 2 },
                { "abababa", 5 },
                { "ababababa", 14 },
                { DualRecursionInput(1025) , null },
            };

        private static String DualRecursionInput(Int32 length)
        {
            var builder = new StringBuilder();
            for (Int32 i = 0; i < length; i += 2)
                builder.Append("ab");
            builder.Append("a");
            return builder.ToString();
        }

        public static SampleGrammar Parentheses { get; } =
            new SampleGrammar(nameof(Parentheses))
            {
                R(0), S(0,'(',1), C(1,0,2), S(2,')',3), C(3,0,4), R(4),
                { "", 1 },
                { "()", 1 },
                { "(()(()))()", 1 },
                { "(()()))(()", 0 },
                { ParenthesesInput(1024), 1 },
            };

        public static SampleGrammar Parentheses2 { get; } =
            new SampleGrammar(nameof(Parentheses2))
            {
                C(0,0,1), C(1,0,2), R(2), S(0,'(',3), S(3,')',2), C(3,0,4), S(4,')',2),
                { "", 0 },
                { "()", 1 },
                { "(()(()))()", 1 },
                { "()()()", 2 },
                { "()()()()", 5 },
                { "(((()()())))", 2 },
                { "(((()()()())))", 5 },
                { "(((()()()()))()())", 10 },
                { "(()()))(()", 0 },
                { ParenthesesInput(128), 400 },
                { ParenthesesInput(1024), null },
            };

        private static String ParenthesesInput(Int32 length)
        {
            var part = (Int32)(length * 0.3);
            return (length < 2) ? "" : "(" + ParenthesesInput(length - part - 2) + ")" + ParenthesesInput(part);
        }

        public static SampleGrammar HighlyAmbiguous { get; } =
            new SampleGrammar(nameof(HighlyAmbiguous))
            {
                S(0,'a',1), R(1), C(0,0,2), C(2,0,3), R(3), C(3,0,4), R(4),
                { "", 0 },
                { "a", 1 },
                { "aa", 1 },
                { "aaa", 3 },
                { "aaaa", 10 },
                { String.Join("", Enumerable.Repeat('a', 64)), null },
            };
    }
}

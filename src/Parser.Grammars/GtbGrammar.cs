using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Parser.Grammars
{
    using Utilities;

    public class GtbGrammar : SimpleGrammar
    {
        private IReadOnlyDictionary<String, Char> Translation { get; }

        public GtbGrammar(TextReader reader)
        {
            var (rules, start) = Parse(Tokenize(reader));
            Translation = GetTranslation(rules);
            MakeStates(rules, start, Translation);
        }

        public String Input(TextReader reader)
        {
            var builder = new StringBuilder();
            String line;
            while ((line = reader.ReadLine()) != null)
                foreach (var terminal in line.Split(' ', '\t', '\r', '\n'))
                    if (terminal != "")
                        builder.Append(Translation[terminal]);
            return builder.ToString();
        }

        public static GtbGrammar AnsiC { get; }
        public static String AnsiCInput { get; }

        static GtbGrammar()
        {
            using (var stream = EmbeddedStream("ansi_c.gtb"))
            using (var reader = new StreamReader(stream))
                AnsiC = new GtbGrammar(reader);
            using (var stream = EmbeddedStream("gtb_src.tok"))
            using (var reader = new StreamReader(stream))
                AnsiCInput = AnsiC.Input(reader);
        }

        private static Stream EmbeddedStream(String path) =>
            Assembly.GetExecutingAssembly().GetManifestResourceStream("Parser.Grammars." + path);

        private abstract class Token
        {
            public sealed class Dot : Token
            {
            }

            public sealed class Equal : Token
            {
            }

            public sealed class Nonterminal : Token
            {
                public String Name { get; }

                public Nonterminal(String name)
                {
                    Name = name;
                }
            }

            public sealed class Or : Token
            {
            }

            public sealed class Terminal : Token
            {
                public String Text { get; }

                public Terminal(String text)
                {
                    Text = text;
                }
            }

            public sealed class Tilde : Token
            {
            }

            private Token()
            {
            }
        }

        private static IEnumerable<Token> Tokenize(TextReader reader)
        {
            var constants = new Dictionary<String, Token>
            {
                { ".", new Token.Dot() },
                { "::=", new Token.Equal() },
                { "|", new Token.Or() },
                { "~", new Token.Tilde() },
            };
            String line;
            var inComment = 0;
            while ((line = reader.ReadLine()) != null)
            {
                var start = 0;
                while (start < line.Length)
                {
                    if (line[start] == '(')
                    {
                        start += 1;
                        inComment += 1;
                        continue;
                    }
                    if (inComment > 0)
                    {
                        if (line[start] == ')')
                            inComment -= 1;
                        start += 1;
                        continue;
                    }
                    var constantFound = false;
                    foreach (var (expected, token) in constants)
                        if (start + expected.Length <= line.Length && line.Substring(start, expected.Length) == expected)
                        {
                            yield return token;
                            start += expected.Length;
                            constantFound = true;
                            break;
                        }
                    if (constantFound)
                        continue;
                    if (line[start] == '\'')
                    {
                        start += 1;
                        var end = line.IndexOf('\'', start);
                        yield return new Token.Terminal(line.Substring(start, end-start));
                        start = end + 1;
                        continue;
                    }
                    if (Char.IsLetterOrDigit(line[start]) || line[start] == '_')
                    {
                        var end = start;
                        while (end < line.Length && (Char.IsLetterOrDigit(line[end]) || line[end] == '_'))
                            end++;
                        yield return new Token.Nonterminal(line.Substring(start, end-start));
                        start = end;
                        continue;
                    }
                    if (Char.IsWhiteSpace(line[start]))
                    {
                        start += 1;
                        continue;
                    }
                    throw new Exception("invalid token: " + line.Substring(start));
                }
            }
        }

        private static (IReadOnlyDictionary<String, IReadOnlyCollection<IReadOnlyList<(Boolean, String)>>>, String) Parse(IEnumerable<Token> tokens)
        {
            var result = new Dictionary<String, IReadOnlyCollection<IReadOnlyList<(Boolean, String)>>>();
            List<(Boolean, String)> alt = null;
            List<IReadOnlyList<(Boolean, String)>> rhs = null;
            String lhs = null;
            String first = null;
            foreach (var token in tokens)
            {
                switch (token)
                {
                    case Token.Dot dot:
                        rhs.Add(alt);
                        result[lhs] = rhs;
                        alt = null;
                        rhs = null;
                        lhs = null;
                        continue;
                    case Token.Equal equal:
                        rhs = new List<IReadOnlyList<(Boolean, String)>>();
                        alt = new List<(Boolean, String)>();
                        continue;
                    case Token.Nonterminal nonterminal:
                        if (lhs == null)
                        {
                            lhs = nonterminal.Name;
                            first = first ?? lhs;
                        }
                        else
                        {
                            alt.Add((false, nonterminal.Name));
                        }
                        continue;
                    case Token.Or or:
                        rhs.Add(alt);
                        alt = new List<(Boolean, String)>();
                        continue;
                    case Token.Terminal terminal:
                        alt.Add((true, terminal.Text));
                        continue;
                    case Token.Tilde tilde:
                        continue;
                    default:
                        throw new InvalidOperationException();
                }
            }
            return (result, first);
        }

        private static IReadOnlyDictionary<String, Char> GetTranslation(IReadOnlyDictionary<String, IReadOnlyCollection<IReadOnlyList<(Boolean, String)>>> rules)
        {
            var result = new Dictionary<String, Char>();
            var next = (Char)1;
            foreach (var (_, alternatives) in rules)
                foreach (var alternative in alternatives)
                    foreach (var (isTerminal, text) in alternative)
                        if (isTerminal && !result.ContainsKey(text))
                            result.Add(text, next++);
            return result;
        }

        private void MakeStates(IReadOnlyDictionary<String, IReadOnlyCollection<IReadOnlyList<(Boolean, String)>>> rules, String start, IReadOnlyDictionary<String, Char> translation)
        {
            var indices = new Dictionary<String, Int32>();
            Int32 Dfs(String nonterminal)
                => indices.GetOrCreate(nonterminal, n =>
                    {
                        var index = NewState();
                        indices.Add(nonterminal, index);
                        foreach (var alternative in rules[nonterminal])
                        {
                            var current = index;
                            foreach (var (isTerminal, text) in alternative)
                            {
                                var next = NewState();
                                if (isTerminal)
                                    Add(S(current, translation[text], next));
                                else
                                    Add(C(current, Dfs(text), next));
                                current = next;
                            }
                            Add(R(current));
                        }
                        return index;
                    });
            Dfs(start);
        }
    }
}

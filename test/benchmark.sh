#!/bin/bash

framework=netcoreapp2.1

benchmark_antlr4="NativeBenchmark(antlr4/#/bin/Release/${framework}/Benchmark.Antlr4.#.dll)"
benchmark_elkhound="NativeBenchmark(elkhound/#/bin/Release/${framework}/Benchmark.Elkhound.#.dll)"
benchmark_iguana="NativeBenchmark(iguana/#/bin/Release/${framework}/Benchmark.Iguana.#.dll)"
benchmark_yaep="NativeBenchmark(yaep/#/bin/Release/${framework}/Benchmark.Yaep.#.dll)"
parser_native="NativeParserGenerator"
parser_relational_full="OptimizingParserGenerator(Relational2ParserGenerator(CommutativeAtomEngineFactory(DagAtomEngineFactory(OrderedGraphEngineFactory)),RelationOperantFactory(SubstitutionRelationEngineFactory(MemoizingDagRelationEngineFactory(TrivialSubstitutionWrapEngineFactory(CommutativeWrapEngineFactory(DagWrapEngineFactory(TrivialGraphEngineFactory))),OrderedGraphEngineFactory))),true,ForwardAtomicOptimization),EpsilonGrammarOptimization,BackwardGrammarOptimization,ForwardGrammarOptimization)"
parser_relational_recognizer="OptimizingParserGenerator(Relational2ParserGenerator(CommutativeAtomEngineFactory(DagAtomEngineFactory(OrderedGraphEngineFactory)),RelationOperantFactory(StackedRelationEngineFactory(MemoizingDagRelationEngineFactory(CommutativeWrapEngineFactory(DagWrapEngineFactory(DictionaryGraphEngineFactory)),OrderedGraphEngineFactory))),true,ForwardAtomicOptimization),EpsilonGrammarOptimization,BackwardGrammarOptimization,ForwardGrammarOptimization)"


benchmark=${benchmark_antlr4}
configuration=Release
command=exec
environment=
grammar=Java
grammar_suffix=
inputs="inputs/**/*.java"
mode=recognition
parser=${parser_relational_full}

while [ "$#" -ne 0 ]; do
    case $1 in
        # test cases
        Java )
            grammar=Java
            inputs="inputs/**/*.java"
            ;;
        Java8 )
            grammar=Java8
            inputs="inputs/**/*.java"
            ;;
        Python )
            grammar=Python
            inputs="inputs/**/*.py"
            ;;
        # grammar variants
        original )
            grammar_suffix=
            ;;
        transformed )
            grammar_suffix=.Antlr
            ;;
        # parser generators
        antlr4 )
            benchmark=${benchmark_antlr4}
            parser=${parser_native}
            ;;
        elkhound )
            benchmark=${benchmark_elkhound}
            parser=${parser_native}
            ;;
        iguana )
            benchmark=${benchmark_iguana}
            parser=${parser_native}
            ;;
        relational_full )
            benchmark=${benchmark_antlr4}
            parser=${parser_relational_full}
            ;;
        relational_recognizer )
            benchmark=${benchmark_antlr4}
            parser=${parser_relational_recognizer}
            ;;
        yaep )
            benchmark=${benchmark_yaep}
            parser=${parser_native}
            ;;
        # modes
        counting )
            mode=counting
            ;;
        recognition )
            mode=recognition
            ;;
        # additional flags
        debug )
            configuration=Debug
            ;;
        memory )
            shift
            environment="memlimit $1 /usr/bin/time"
            ;;
        perf )
            command=perf
            ;;
        help )
            echo "Usage: benchmark.sh [options]"
            echo ""
            echo "Options specifying which parser generator to use (mutually exclusive):"
            echo "antlr4 | elkhound | yaep | relational_recognizer | relational_full"
            echo ""
            echo "Options specifying which test suite (grammar + inputs) to run (mutually exclusive):"
            echo "Java | Java8 | Python"
            echo ""
            echo "Options specifying whether to use the grammar as written or as transformed by ANTLR4 (mutually exclusive):"
            echo "original | transformed"
            echo ""
            echo "Options specifying parsing mode (mutually exclusive):"
            echo "recognition | counting"
            echo ""
            echo "Additional options (independent):"
            echo "debug -- use Debug build (instead of Release)"
            echo "memory <limit> -- measure and limit memory usage to <limit> (requires additional tool)"
            echo "perf -- generate performance graphs (requires additional tool)"
            exit 0
            ;;
        * )
            echo "unrecognized argument: $1" >&2
            exit 1
    esac
    shift
done

full_command=(${environment} dotnet "${command}" Parser.Benchmarks/bin/"${configuration}"/"${framework}"/Parser.Benchmarks.dll -b "${benchmark//\#/${grammar}${grammar_suffix}}" "${inputs}" -p "${parser}" -m "${mode}")
( cd $(dirname $0) && "${full_command[@]}" )

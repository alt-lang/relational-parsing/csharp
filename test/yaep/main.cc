#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <unordered_map>

#include <yaep/yaep.h>
#include "grammar.h"

#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif

extern "C" { MODULE_API bool parse(int, int const *, int, long *); }

int const * input_data;
int input_length;
int input_index;

int read_token(void**) {
    return (input_index < input_length) ? input_data[input_index++] : -1;
}

void* alloc_node(int size) {
    return malloc(size);
}

void free_node(void* ptr) {
    free(ptr);
}

void syntax_error (int, void*, int, void*, int, void*) {
}

class Parser : public yaep {
    public:
        Parser(const char * description) : yaep() {
            assert(parse_grammar(0, description) == 0);
        }
};

// we never delete them because YAEP has messed up global data structures :(
Parser & recognizer = *new Parser(recognizer_grammar_description);
Parser & parser = *new Parser(parser_grammar_description);

typedef struct yaep_tree_node node;

long compute_count(node * n, std::unordered_map<node *, long> & cache) {
    auto it = cache.find(n);
    if (it != cache.end())
        return it->second;
    long value;
    switch (n->type) {
        case YAEP_ERROR:
            value = 0;
            break;
        case YAEP_NIL:
        case YAEP_TERM:
            value = 1;
            break;
        case YAEP_ALT:
            value = compute_count(n->val.alt.node, cache);
            if (n->val.alt.next != nullptr)
                value += compute_count(n->val.alt.next, cache);
            break;
        case YAEP_ANODE: {
            value = 1;
            int count = n->val.anode.name[0] - 'A';
            for (int i = 0; i < count; i++)
                value *= compute_count(n->val.anode.children[i], cache);
            break;
        }
        default:
            assert(!"invalid node type");
    }
    cache.emplace(n, value);
    return value;
}

bool parse(int mode, int const * data, int length, long * result) {
    input_data = data;
    input_length = length;
    input_index = 0;
    node* root;
    int ambiguous;
    switch (mode) {
        case 0: {
            recognizer.set_error_recovery_flag(0);
            recognizer.set_lookahead_level(1);
            recognizer.set_one_parse_flag(0);
            assert(recognizer.parse(&read_token, &syntax_error, &alloc_node, &free_node, &root, &ambiguous) == 0);
            if (root == nullptr || root->type == YAEP_ERROR)
                return false;
            return true;
        }
        case 1: {
            parser.set_error_recovery_flag(0);
            parser.set_lookahead_level(1);
            parser.set_one_parse_flag(0);
            assert(parser.parse(&read_token, &syntax_error, &alloc_node, &free_node, &root, &ambiguous) == 0);
            if (root == nullptr || root->type == YAEP_ERROR)
                return false;
            std::unordered_map<node*, long> counts;
            *result = compute_count(root, counts);
            for (auto p : counts)
                free_node(p.first);
            return true;
        }
    }
    assert(!"unsupported mode");
    return false;
}

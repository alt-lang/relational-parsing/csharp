extern alias rp;

using rp::Parser;

public class YaepBenchmark : Antlr4Benchmark
{
    public override IGrammar Grammar =>
        new YaepGrammar(base.Grammar);
}

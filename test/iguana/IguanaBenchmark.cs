extern alias rp;

using rp::Parser;

public class IguanaBenchmark : Antlr4Benchmark
{
    public override IGrammar Grammar =>
        new IguanaGrammar(base.Grammar);
}

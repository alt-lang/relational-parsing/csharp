extern alias rp;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

using rp.Parser;
using rp.Parser.Valuations;

public class IguanaParserAdaptor<TValue> : IParser<TValue>
{
    private static String ParserJar;

    static IguanaParserAdaptor()
    {
        ParserJar = Path.ChangeExtension(typeof(IguanaParserAdaptor<>).Assembly.Location, ".jar");
        if (!File.Exists(ParserJar))
            throw new FileNotFoundException("Could not find the JAR file containing the corresponging Iguana parser.");
    }

    public IValuationSemiring<TValue> Semiring { get; }
    private readonly Func<Int64, TValue> Success;
    private readonly Func<TValue> Failure;
    private readonly Process Process;

    public IguanaParserAdaptor(String mode, IValuationSemiring<TValue> semiring, Func<Int64, TValue> success, Func<TValue> failure)
    {
        Semiring = semiring;
        Success = success;
        Failure = failure;
        Process = new Process();
        Process.StartInfo.FileName = "java";
        Process.StartInfo.Arguments = $"-jar {ParserJar} {mode}";
        Process.StartInfo.UseShellExecute = false;
        Process.StartInfo.RedirectStandardInput = true;
        Process.StartInfo.RedirectStandardOutput = true;
        Process.Start();
        var ready = Process.StandardOutput.ReadLine();
        if (ready != "ready")
            throw new InvalidOperationException($"Iguana parser invalid -- reported '{ready}'");
    }

    public TValue Run(IEnumerable<Char> input, Action step)
    {
        const Int32 OFFSET = 32; // needed because Iguana skips Java whitespace characters...
        foreach (var terminal in input)
        {
            Process.StandardInput.Write($"{((Int32) terminal) + OFFSET} ");
            step();
        }
        Process.StandardInput.WriteLine("-1");
        var words = Process.StandardOutput.ReadLine().Split(null);
        Int64 result;
        return (words.Length > 0 && Int64.TryParse(words[0], out result)) ? Success(result) : Failure();
    }
}

extern alias rp;

using System;

using rp.Parser;
using rp.Parser.Benchmarks;
using rp.Parser.Grammars;
using rp.Parser.Valuations;

public class IguanaGrammar : INativeGrammar
{
    private readonly IGrammar InnerGrammar;

    public IguanaGrammar(IGrammar innerGrammar)
    {
        InnerGrammar = innerGrammar;
    }

    IState IGrammar.StartState => InnerGrammar.StartState;
    IState IGrammar.StopState => InnerGrammar.StopState;

    IParser<TValue> INativeGrammar.CreateParser<TValue>(IValuationSemiring<TValue> semiring)
    {
        Delegate success;
        Delegate failure;
        String mode;
        if (semiring is BooleanSemiring)
        {
            success = new Func<Int64, Boolean>(x => x != 0);
            failure = new Func<Boolean>(() => false);
            mode = "recognition";
        }
        else
            throw new NotSupportedException();
        return new IguanaParserAdaptor<TValue>(mode, semiring, (Func<Int64, TValue>) success, (Func<TValue>) failure);
    }
}

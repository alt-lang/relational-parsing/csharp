using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser.Benchmarks.Export
{
    using Utilities;

    public class ElkhoundOutput : IOutput
    {
        private readonly String Path;
        private readonly String Mode;

        public ElkhoundOutput(String path, String mode)
        {
            Path = path;
            Mode = mode;
        }

        public void Export(IGrammar grammar)
        {
            var analysis = new GrammarAnalysis(grammar);
            using (var writer = new StreamWriter(Path, false))
            {
                writer.WriteLine("option useGCDefaults;");
                writer.WriteLine($"context_class {Mode} : public UserActions {{");
                if (Mode == "Parser")
                {
                    writer.WriteLine("  public:");
                    writer.WriteLine("    struct Node {");
                    writer.WriteLine("      int type;");
                    writer.WriteLine("      Node * left;");
                    writer.WriteLine("      Node * right;");
                    writer.WriteLine("      int nrefs;");
                    writer.WriteLine("      void acquire() { nrefs++; }");
                    writer.WriteLine("      void release() { if (--nrefs) return; if (left) left->release(); if (right) right->release(); delete this; }");
                    writer.WriteLine("    };");
                }
                writer.WriteLine("};");
                writer.WriteLine("terminals {");
                writer.WriteLine("  0: T_EOF;");
                var terminalMap = new Dictionary<Char, Int32>();
                foreach (var terminal in analysis.ShiftOrigins.Keys.OrderBy(_ => _))
                {
                    var value = terminalMap.Count + 1;
                    terminalMap.Add(terminal, value);
                    writer.WriteLine($"  {value}: T_{value};");
                }
                writer.WriteLine("}");
                var stateMap = new Dictionary<IState, Int32>();
                var states = new List<IState>();
                Int32 NewState(IState state) { states.Add(state); return stateMap.Count; };
                stateMap.GetOrCreate(grammar.StartState, NewState);
                foreach (var state in analysis.AllStates)
                    stateMap.GetOrCreate(state, NewState);
                String typeCode, dupCode, delCode, mergeCode, callCode, shiftCode, reduceCode;
                switch (Mode)
                {
                    case "Recognizer":
                        typeCode = delCode = String.Empty;
                        dupCode = mergeCode = "return x;";
                        callCode = shiftCode = reduceCode = ";";
                        break;
                    case "Counter":
                        typeCode = "(long)";
                        dupCode = "return x;";
                        delCode = String.Empty;
                        mergeCode = "return x + y;";
                        callCode = "{ return x0 * x1; }";
                        shiftCode = "{ return x; }";
                        reduceCode = "{ return 1; }";
                        break;
                    case "Parser":
                        typeCode = "(Parser::Node*)";
                        dupCode = "x->acquire(); return x;";
                        delCode = "x->release();";
                        mergeCode = "return new Parser::Node { 3, x, y, 1 };";
                        callCode = "{ return new Parser::Node { 2, x0, x1, 1 }; }";
                        shiftCode = "{ return new Parser::Node { 1, nullptr, x, 1 }; }";
                        reduceCode = "{ return new Parser::Node { 0, nullptr, nullptr, 1 }; }";
                        break;
                    default:
                        throw new NotSupportedException($"invalid mode {Mode}");
                }
                foreach (var state in states)
                {
                    writer.WriteLine($"nonterm{typeCode} S_{stateMap[state]} {{");
                    writer.WriteLine($"  fun dup(x) {{ {dupCode} }}");
                    writer.WriteLine($"  fun del(x) {{ {delCode} }}");
                    writer.WriteLine($"  fun merge(x, y) {{ {mergeCode} }}");
                    foreach (var transition in state.Transitions)
                        switch (transition)
                        {
                            case Transition.Call call:
                                writer.WriteLine($"  -> x0:S_{stateMap[call.Child]} x1:S_{stateMap[call.Next]} {callCode}");
                                break;
                            case Transition.Shift shift:
                                writer.WriteLine($"  -> T_{terminalMap[shift.Terminal]} x:S_{stateMap[shift.Next]} {shiftCode}");
                                break;
                            case Transition.Reduce reduce:
                                writer.WriteLine($"  -> {reduceCode}");
                                break;
                        }
                    writer.WriteLine("}");
                }
            }
        }
    }
}

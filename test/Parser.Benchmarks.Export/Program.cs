﻿using System;
using System.Linq;
using CommandLine;

namespace Parser.Benchmarks.Export
{
    using Common;
    using Utilities;
    using Valuations;

    public class Program
    {
        private class Options
        {
            [Option('b', "benchmark", Required = true, HelpText = "Benchmark to export.")]
            public String Benchmark { get; set; }

            [Option('o', "output", Required = true, HelpText = "Output format and location.")]
            public String Output { get; set; }

            [Option('e', "epsilon", HelpText = "Optimize epsilon transitions (valid for recognition and counting, might reshape parse trees).")]
            public Boolean OptimizeEpsilon { get; set; }
        }

        private static Int32 RealMain(Options options)
        {
            var instantiator = new Instantiator();
            void ReportError(Exception ex) => Console.Error.WriteLine($"Error: instantiation failed: {ex}");

            Console.WriteLine("Instantiating benchmark:");
            var benchmark = instantiator.InstantiateAll<IBenchmark>(options.Benchmark, ReportError).Single();
            Console.WriteLine($"  {benchmark}");

            Console.WriteLine("Instantiating output:");
            var output = instantiator.InstantiateAll<IOutput>(options.Output, ReportError).Single();
            Console.WriteLine($"  {output}");

            var grammar = benchmark.Grammar;
            if (options.OptimizeEpsilon)
            {
                Console.WriteLine("Optimizing epsilon transitions...");
                grammar = new EpsilonGrammarOptimization().Perform(grammar, new BooleanSemiring());
            }

            Console.WriteLine("Exporting:");
            output.Export(grammar);
            Console.WriteLine("Done.");

            return 0;
        }

        public static Int32 Main(String[] args) =>
            CommandLine.Parser.Default.ParseArguments<Options>(args).MapResult(RealMain, _ => 1);
    }
}

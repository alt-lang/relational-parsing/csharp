using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser.Benchmarks.Export
{
    using Utilities;

    public class IguanaOutput : IOutput
    {
        private readonly String Path;
        private readonly Boolean Linearize;

        public IguanaOutput(String path, Boolean linearize)
        {
            Path = path;
            Linearize = linearize;
        }

        public void Export(IGrammar grammar)
        {
            const Int32 OFFSET = 32; // needed because Iguana skips Java whitespace characters...
            var analysis = new GrammarAnalysis(grammar);
            using (var writer = new StreamWriter(Path, false))
            {
                var stateMap = new Dictionary<IState, Int32>();
                var states = new List<IState>();
                Int32 NewState(IState state) { states.Add(state); return stateMap.Count; };
                stateMap.GetOrCreate(grammar.StartState, NewState);
                foreach (var state in analysis.AllStates)
                    stateMap.GetOrCreate(state, NewState);
                String stateFormula(IState state)
                    => $"{{ \"kind\": \"Nonterminal\", \"name\": \"S_{stateMap[state]}\" }}";
                String terminalFormula(Char terminal)
                    => $"{{ \"kind\": \"Terminal\", \"name\": \"T_{(Int32) terminal}\", \"regex\": {{ \"kind\": \"Char\", \"val\": {((Int32) terminal) + OFFSET} }} }}";
                void WritePathFrom(IState state)
                {
                    while (Linearize)
                        using (var enumerator = state.Transitions.GetEnumerator())
                        {
                            if (!enumerator.MoveNext())
                                break;
                            var transition = enumerator.Current;
                            if (enumerator.MoveNext())
                                break;
                            switch (transition)
                            {
                                case Transition.Call call:
                                    writer.WriteLine(",");
                                    writer.Write($"        {stateFormula(call.Child)}");
                                    state = call.Next;
                                    continue;
                                case Transition.Shift shift:
                                    writer.WriteLine(",");
                                    writer.Write($"        {terminalFormula(shift.Terminal)}");
                                    state = shift.Next;
                                    continue;
                                case Transition.Reduce reduce:
                                    writer.WriteLine();
                                    writer.WriteLine("      ]");
                                    return;
                            }
                        }
                    writer.WriteLine(",");
                    writer.WriteLine($"        {stateFormula(state)}");
                    writer.WriteLine("      ]");
                }
                var startFormula = $"{{ \"kind\": \"Start\", \"name\": \"Start(S_{stateMap[grammar.StartState]})\", \"nonterminal\": {stateFormula(grammar.StartState)} }}";
                writer.WriteLine("{");
                writer.WriteLine("  \"kind\": \"Grammar\",");
                writer.WriteLine($"  \"startSymbol\": {startFormula},");
                writer.WriteLine("  \"rules\": [");
                writer.WriteLine("    {");
                writer.WriteLine("      \"kind\": \"Rule\",");
                writer.WriteLine($"      \"head\": {{ \"kind\": \"Nonterminal\", \"name\": \"Start(S_{stateMap[grammar.StartState]})\", \"nodeType\": \"Start\" }},");
                writer.WriteLine($"      \"body\": [ {stateFormula(grammar.StartState)} ],");
                writer.WriteLine($"      \"definition\": {startFormula}");
                writer.Write("    }");
                foreach (var transition in analysis.AllTransitions)
                {
                    writer.WriteLine(",");
                    writer.WriteLine("    {");
                    writer.WriteLine("      \"kind\": \"Rule\",");
                    writer.WriteLine($"      \"head\": {stateFormula(transition.Source)},");
                    writer.WriteLine("      \"body\": [");
                    switch (transition)
                    {
                        case Transition.Call call:
                            writer.Write($"        {stateFormula(call.Child)}");
                            WritePathFrom(call.Next);
                            break;
                        case Transition.Shift shift:
                            writer.Write($"        {terminalFormula(shift.Terminal)}");
                            WritePathFrom(shift.Next);
                            break;
                        case Transition.Reduce reduce:
                            writer.WriteLine("      ]");
                            break;
                    }
                    writer.Write("    }");
                }
                writer.WriteLine("");
                writer.WriteLine("  ]");
                writer.WriteLine("}");
            }
        }
    }
}

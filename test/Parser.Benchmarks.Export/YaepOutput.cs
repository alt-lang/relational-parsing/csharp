using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser.Benchmarks.Export
{
    using Utilities;

    public class YaepOutput : IOutput
    {
        private readonly String Path;
        private readonly Boolean Linearize;

        public YaepOutput(String path, Boolean linearize)
        {
            Path = path;
            Linearize = linearize;
        }

        private void WriteGrammar(IGrammar grammar, TextWriter writer, String name, Boolean emitNodes)
        {
            var analysis = new GrammarAnalysis(grammar);
            writer.WriteLine($"const char * {name} =");
            writer.WriteLine("    \"TERM\\n\"");
            foreach (var terminal in analysis.ShiftOrigins.Keys.OrderBy(_ => _))
            {
                var value = (Int32) terminal;
                writer.WriteLine($"    \"T_{value} = {value}\\n\"");
            }
            writer.WriteLine("    \"\\n\"");
            var stateMap = new Dictionary<IState, Int32>();
            var states = new List<IState>();
            Int32 NewState(IState state) { states.Add(state); return stateMap.Count; };
            stateMap.GetOrCreate(grammar.StartState, NewState);
            foreach (var state in analysis.AllStates)
                stateMap.GetOrCreate(state, NewState);
            Int32 WritePathFrom(IState state)
            {
                var length = 0;
                while (Linearize)
                    using (var enumerator = state.Transitions.GetEnumerator())
                    {
                        if (!enumerator.MoveNext())
                            break;
                        var transition = enumerator.Current;
                        if (enumerator.MoveNext())
                            break;
                        switch (transition)
                        {
                            case Transition.Call call:
                                writer.Write($" S_{stateMap[call.Child]}");
                                state = call.Next;
                                length++;
                                continue;
                            case Transition.Shift shift:
                                writer.Write($" T_{(Int32) shift.Terminal}");
                                state = shift.Next;
                                length++;
                                continue;
                            case Transition.Reduce reduce:
                                return length;
                        }
                    }
                writer.Write($" S_{stateMap[state]}");
                return length + 1;
            }
            void WriteTranslation(Int32 length)
            {
                if (!emitNodes)
                    return;
                const Int32 MAX_LENGTH = 'Z' - 'A';
                if (length > MAX_LENGTH)
                    throw new NotSupportedException($"Cannot export rules longer than {MAX_LENGTH}.");
                writer.Write($" # {(Char) ('A' + length)}");
                if (length > 0)
                    writer.Write($" ({String.Join(' ', Enumerable.Range(0, length))})");
            }
            foreach (var state in states)
            {
                writer.Write($"    \"S_{stateMap[state]}");
                var first = true;
                foreach (var transition in state.Transitions)
                {
                    writer.Write(first ? " : " : " | ");
                    first = false;
                    switch (transition)
                    {
                        case Transition.Call call:
                            writer.Write($"S_{stateMap[call.Child]}");
                            WriteTranslation(WritePathFrom(call.Next) + 1);
                            break;
                        case Transition.Shift shift:
                            writer.Write($"T_{(Int32) shift.Terminal}");
                            WriteTranslation(WritePathFrom(shift.Next) + 1);
                            break;
                        case Transition.Reduce reduce:
                            WriteTranslation(0);
                            break;
                    }
                }
                writer.WriteLine(first ? " : ;\\n\"" : " ;\\n\"");
            }
            writer.WriteLine("    ;");
        }

        public void Export(IGrammar grammar)
        {
            using (var writer = new StreamWriter(Path, false))
            {
                WriteGrammar(grammar, writer, "recognizer_grammar_description", false);
                WriteGrammar(grammar, writer, "parser_grammar_description", true);
            }
        }
    }
}

namespace Parser.Benchmarks.Export
{
    public interface IOutput
    {
        void Export(IGrammar grammar);
    }
}

#!/usr/bin/env python3

import ast
import glob
import sys
import time

sources = []
for pattern in sys.argv[1:]:
    for path in glob.glob(pattern, recursive=True):
        with open(path) as f:
            sources.append(f.read())

passed = 0
start = time.process_time()
for source in sources:
    try:
        ast.parse(source)
        passed += 1
    except SyntaxError:
        pass
end = time.process_time()
print('%d/%d files recognized, %.3f s' % (passed, len(sources), end - start))

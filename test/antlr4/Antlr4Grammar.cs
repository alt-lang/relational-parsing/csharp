extern alias rp;

using System;
using System.Collections.Generic;
using Antlr4.Runtime.Atn;

using rp::Parser;
using rp::Parser.Benchmarks;
using rp::Parser.Grammars;
using rp::Parser.Valuations;

public class Antlr4Grammar : SimpleGrammar, INativeGrammar
{
    public Antlr4Grammar()
    {
        var atn = (ATN) Antlr4ParserAdaptor.ParserType.GetField("_ATN").GetValue(null);
        var stateMap = new Dictionary<ATNState, Int32>();
        var queue = new Queue<ATNState>();
        Int32 GetState(ATNState state) {
            Int32 id;
            if (!stateMap.TryGetValue(state, out id))
            {
                queue.Enqueue(state);
                id = NewState();
                stateMap.Add(state, id);
            }
            return id;
        };
        foreach (var state in atn.ruleToStartState)
            GetState(state);
        var epsilon = NewState();
        Add(R(epsilon));
        while (queue.Count > 0)
        {
            var state = queue.Dequeue();
            if (state.StateType == StateType.RuleStop)
                continue;
            Int32 source = GetState(state);
            foreach (var transition in state.TransitionsArray)
            {
                Int32 target = GetState(transition.target);
                switch (transition.TransitionType)
                {
                    case TransitionType.ACTION:
                        var action = (transition as ActionTransition);
                        if (action.actionIndex == -1 || action.actionIndex == 65535)
                            goto case TransitionType.EPSILON;
                        goto default;
                    case TransitionType.EPSILON:
                    case TransitionType.PRECEDENCE:
                        Add(C(source, epsilon, target));
                        break;
                    case TransitionType.ATOM:
                    case TransitionType.RANGE:
                    case TransitionType.SET:
                        foreach (var terminal in transition.Label.ToIntegerList())
                            Add(S(source, (Char)terminal, target));
                        break;
                    case TransitionType.RULE:
                        Int32 follow = GetState((transition as RuleTransition).followState);
                        Add(C(source, target, follow));
                        break;
                    default:
                        throw new NotSupportedException($"Cannot translate ANTLR4 transition with type {transition.TransitionType}");
                }
            }
        }
        foreach (var state in atn.ruleToStopState)
            Add(R(stateMap[state]));
    }

    IParser<TValue> INativeGrammar.CreateParser<TValue>(IValuationSemiring<TValue> semiring)
    {
        Delegate interpretResult;
        if (semiring is BooleanSemiring)
            interpretResult = new Func<Object, Boolean>(ctx => ctx != null);
        else
            throw new NotSupportedException("Unsupported parsing mode.");
        return new Antlr4ParserAdaptor<TValue>(semiring, (Func<Object, TValue>) interpretResult);
    }
}

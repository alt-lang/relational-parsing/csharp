extern alias rp;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;

using rp.Parser;
using rp.Parser.Benchmarks;
using rp.Parser.Grammars;
using rp.Parser.Valuations;

using A4L = Antlr4.Runtime.Lexer;
using A4P = Antlr4.Runtime.Parser;

public class Antlr4Benchmark : IBenchmark
{
    private static readonly Func<A4L> CreateLexer;

    static Antlr4Benchmark()
    {
        var assembly = typeof(Antlr4Benchmark).Assembly;
        var lexerType = assembly.DefinedTypes.Single(type => typeof(A4L).IsAssignableFrom(type) && !type.IsAbstract);
        var constructor = lexerType.GetConstructors().Single(c => c.GetParameters().Length == 1);
        var parameterType = constructor.GetParameters()[0].ParameterType;
        CreateLexer = Expression.Lambda<Func<A4L>>(Expression.Convert(Expression.New(constructor, Expression.Constant(null, parameterType)), typeof(A4L))).Compile();
    }

    public virtual IGrammar Grammar
        => new Antlr4Grammar();

    public IEnumerable<Char> Input(TextReader reader)
    {
        var lexer = CreateLexer();
        lexer.SetInputStream(new UnbufferedCharStream(reader));
        foreach (var token in lexer.GetAllTokens())
            if (token.Channel != TokenConstants.HiddenChannel)
                yield return Antlr4Tokens.Import(token.Type);
        yield return Antlr4Tokens.Eof;
    }

    public virtual IMode Mode(String name)
    {
        switch (name)
        {
            case "counting":
                return new CountingMode();
            case "recognition":
                return new RecognitionMode();
            default:
                throw new ArgumentException($"Unsupported mode '{name}' requested for {this.GetType().FullName}");
        }
    }
}

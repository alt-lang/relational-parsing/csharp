extern alias rp;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using rp::Parser;
using rp::Parser.Valuations;

using A4P = Antlr4.Runtime.Parser;

public abstract class Antlr4ParserAdaptor
{
    public static readonly Type ParserType;
    protected static readonly Func<A4P> CreateParser;
    protected static readonly Func<A4P, Object> InvokeParse;

    static Antlr4ParserAdaptor()
    {
        var assembly = typeof(Antlr4ParserAdaptor).Assembly;
        ParserType = assembly.DefinedTypes.Single(type => typeof(A4P).IsAssignableFrom(type) && !type.IsAbstract);
        var constructor = ParserType.GetConstructors().Single(c => c.GetParameters().Length == 1);
        var parameterType = constructor.GetParameters()[0].ParameterType;
        CreateParser = Expression.Lambda<Func<A4P>>(Expression.Convert(Expression.New(constructor, Expression.Constant(null, parameterType)), typeof(A4P))).Compile();
        var parseMethod = ParserType.GetMethod(((String[]) ParserType.GetField("ruleNames").GetValue(null))[0]);
        var parameter = Expression.Parameter(typeof(A4P));
        InvokeParse = Expression.Lambda<Func<A4P, Object>>(Expression.Convert(Expression.Call(Expression.Convert(parameter, ParserType), parseMethod), typeof(Object)), parameter).Compile();
    }
}

public class Antlr4ParserAdaptor<TValue> : Antlr4ParserAdaptor, IParser<TValue>
{
    public IValuationSemiring<TValue> Semiring { get; }
    public Func<Object, TValue> InterpretResult { get; }
    private readonly A4P Parser;

    public Antlr4ParserAdaptor(IValuationSemiring<TValue> semiring, Func<Object, TValue> interpretResult)
    {
        Semiring = semiring;
        InterpretResult = interpretResult;
        Parser = CreateParser();
        Parser.BuildParseTree = !(semiring is BooleanSemiring);
        Parser.ErrorHandler = new BailErrorStrategy();
        Parser.RemoveErrorListeners();
    }

    public TValue Run(IEnumerable<Char> input, Action step)
    {
        var tokens = new List<IToken>();
        var factory = CommonTokenFactory.Default;
        foreach (var terminal in input)
        {
            tokens.Add(factory.Create(Antlr4Tokens.Export(terminal), String.Empty));
            step();
        }
        var source = new ListTokenSource(tokens);
        Object result;
        try
        {
            Parser.TokenStream = new UnbufferedTokenStream(source);
            Parser.Interpreter.PredictionMode = PredictionMode.SLL;
            result = InvokeParse(Parser) ?? new Object();
        }
        catch (Exception)
        {
            try
            {
                Parser.TokenStream = new UnbufferedTokenStream(source);
                Parser.Interpreter.PredictionMode = PredictionMode.LL;
                result = InvokeParse(Parser) ?? new Object();
            }
            catch (Exception)
            {
                result = null;
            }
        }
        return InterpretResult(result);
    }
}

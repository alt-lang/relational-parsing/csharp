using System;
using Antlr4.Runtime;

public static class Antlr4Tokens
{
    public static Char Import(Int32 type)
        => unchecked((Char) type);
    public static Int32 Export(Char token)
        => (token == Eof) ? TokenConstants.EOF : (Int32) token;
    public static readonly Char Eof = Import(TokenConstants.EOF);
}

extern alias rp;

using rp::Parser;

public class ElkhoundBenchmark : Antlr4Benchmark
{
    public override IGrammar Grammar =>
        new ElkhoundGrammar(base.Grammar);
}

extern alias rp;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using rp.Parser;
using rp.Parser.Valuations;

public abstract class ElkhoundParserAdaptor
{
    protected static readonly Dictionary<Char, Int32> TerminalMap;

    static ElkhoundParserAdaptor()
    {
        var grammar = new ElkhoundBenchmark().Grammar;
        var analysis = new GrammarAnalysis(grammar);
        TerminalMap = new Dictionary<Char, Int32>();
        foreach (var terminal in analysis.ShiftOrigins.Keys.OrderBy(_ => _))
        {
            var value = TerminalMap.Count + 1;
            TerminalMap.Add(terminal, value);
        }
    }

    [DllImport(@"parser")]
    protected static extern Boolean parse(Int32 mode, Int32[] data, Int32 length, out Int64 result);
}

public class ElkhoundParserAdaptor<TValue> : ElkhoundParserAdaptor, IParser<TValue>
{
    private readonly Int32 Mode;
    public IValuationSemiring<TValue> Semiring { get; }
    private readonly Func<Int64, TValue> Success;
    private readonly Func<TValue> Failure;

    public ElkhoundParserAdaptor(Int32 mode, IValuationSemiring<TValue> semiring, Func<Int64, TValue> success, Func<TValue> failure)
    {
        Mode = mode;
        Semiring = semiring;
        Success = success;
        Failure = failure;
    }

    public TValue Run(IEnumerable<Char> input, Action step)
    {
        var data = input.Select(_ => TerminalMap[_]).ToArray();
        for (var i = 0; i < data.Length; i++)
            step();
        return parse(Mode, data, data.Length, out var result) ? Success(result) : Failure();
    }
}

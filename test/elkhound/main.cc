#include <algorithm>
#include <cassert>
#include <cstdio>
#include <unordered_map>
#include "glr.h"
#include "lexerint.h"
#include "recognizer.h"
#include "counter.h"
#include "parser.h"

#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif

extern "C" { MODULE_API bool parse(int, int const *, int, long &); }

Recognizer recognizer;
Counter counter;
Parser parser;

class Lexer : public LexerInterface {
    public:
        Lexer(int const * data, int length) : data(data), length(length), index(0) {
        }

        static void nextToken(LexerInterface* l) {
            Lexer & lexer = static_cast<Lexer &>(*l);
            lexer.type = (lexer.index < lexer.length) ? (lexer.data[lexer.index++] & 0xfff) : 0;
        }

        virtual NextTokenFunc getTokenFunc() const {
            return &Lexer::nextToken;
        }

        string tokenDesc() const {
            return string();
        }

        string tokenKindDesc(int kind) const {
            return string();
        }

    private:
        int const * data;
        int length;
        int index;
};

long compute_depth(Parser::Node * node, std::unordered_map<Parser::Node *, long> cache) {
    auto it = cache.find(node);
    if (it != cache.end())
        return it->second;
    long value;
    switch (node->type) {
        case 0:
            value = 0;
            break;
        case 1:
            value = compute_depth(node->right, cache);
            break;
        case 2:
            value = std::max(1 + compute_depth(node->left, cache), compute_depth(node->right, cache));
            break;
        case 3:
            value = std::max(compute_depth(node->left, cache), compute_depth(node->right, cache));
            break;
        default:
            assert(!"invalid node type");
    }
    cache.emplace(node, value);
    return value;
}

bool parse(int mode, int const * data, int length, long & result) {
    Lexer lexer(data, length);
    lexer.nextToken(&lexer);
    UserActions * actions;
    ParseTables * parseTables;
    switch (mode) {
        case 0:
            actions = &recognizer;
            parseTables = recognizer.makeTables();
            break;
        case 1:
            actions = &counter;
            parseTables = counter.makeTables();
            break;
        case 2:
            actions = &parser;
            parseTables = parser.makeTables();
            break;
        default:
            assert(!"invalid mode");
    }
    GLR glr(actions, parseTables);
    glr.noisyFailedParse = false;
    SemanticValue value;
    if (!glr.glrParse(lexer, value))
        return false;
    switch (mode) {
        case 0:
            result = 1;
            break;
        case 1:
            result = (long) value;
            break;
        case 2:
            Parser::Node * root = (Parser::Node *) value;
            std::unordered_map<Parser::Node *, long> depth_cache;
            result = compute_depth(root, depth_cache);
            root->release();
            break;
    }
    return true;
}

extern alias rp;

using System;

using rp.Parser;
using rp.Parser.Benchmarks;
using rp.Parser.Grammars;
using rp.Parser.Valuations;

public class ElkhoundGrammar : INativeGrammar
{
    private readonly IGrammar InnerGrammar;

    public ElkhoundGrammar(IGrammar innerGrammar)
    {
        InnerGrammar = innerGrammar;
    }

    IState IGrammar.StartState => InnerGrammar.StartState;
    IState IGrammar.StopState => InnerGrammar.StopState;

    IParser<TValue> INativeGrammar.CreateParser<TValue>(IValuationSemiring<TValue> semiring)
    {
        Int32 mode;
        Delegate success;
        Delegate failure;
        if (semiring is BooleanSemiring)
        {
            mode = 0;
            success = new Func<Int64, Boolean>(_ => true);
            failure = new Func<Boolean>(() => false);
        }
        else if (semiring is CountingSemiring)
        {
            mode = 1;
            success = new Func<Int64, Int64>(_ => _);
            failure = new Func<Int64>(() => 0);
        }
        else
            throw new NotSupportedException();
        return new ElkhoundParserAdaptor<TValue>(mode, semiring, (Func<Int64, TValue>) success, (Func<TValue>) failure);
    }
}

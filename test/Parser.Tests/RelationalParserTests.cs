using NUnit.Framework;

namespace Parser.Tests
{
    using Common;
    using Graphs;
    using Relational;

    public class RelationalParserTests : ParsingTests
    {
        private static ParserGenerator Generator =
            new OptimizingParserGenerator(
                new Relational2ParserGenerator(
                    new CommutativeAtomEngineFactory(
                        new DagAtomEngineFactory(
                            new OrderedGraphEngineFactory())),
                    new RelationOperantFactory(
                        new SubstitutionRelationEngineFactory(
                            new MemoizingDagRelationEngineFactory(
                                    new TrivialSubstitutionWrapEngineFactory(
                                            new CommutativeWrapEngineFactory(
                                            new DagWrapEngineFactory(
                                                new DictionaryGraphEngineFactory()))),
                                new OrderedGraphEngineFactory()))),
                    true,
                    new ForwardAtomicOptimization()),
                new BackwardGrammarOptimization(),
                new ForwardGrammarOptimization());

        [Test]
        public void TestRecognition()
        {
            foreach (var grammar in ParsingTests.Grammars)
                TestRecognizer(Generator, grammar);
        }

        [Test]
        public void TestCounting()
        {
            foreach (var grammar in ParsingTests.Grammars)
                TestCounter(Generator, grammar);
        }
    }
}

﻿using System.Collections.Generic;

namespace Parser.Benchmarks
{
    public interface IMode
    {
        TResult Accept<TResult>(IModeVisitor<TResult> visitor);
    }

    public interface IModeVisitor<out TResult>
    {
        TResult Visit<TValue>(IMode<TValue> mode);
    }

    public interface IMode<TValue> : IMode
    {
        IValuationSemiring<TValue> Semiring { get; }
        void Process(IEnumerable<TValue> results);
    }

    public abstract class Mode<TValue> : IMode<TValue>
    {
        public abstract IValuationSemiring<TValue> Semiring { get; }

        public virtual void Process(IEnumerable<TValue> values)
        {
            foreach (var value in values)
            {
                // ignore
            }
        }

        TResult IMode.Accept<TResult>(IModeVisitor<TResult> visitor)
            => visitor.Visit(this);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Benchmarks
{
    using Valuations;

    public class CountingMode : Mode<Int64>
    {
        public override IValuationSemiring<Int64> Semiring
            => new CountingSemiring();

        public override void Process(IEnumerable<Int64> values)
        {
            Int64 trees = 0;
            foreach (var value in values)
                trees += value;
            Console.WriteLine($"Result: {trees} parse trees.");
        }
    }
}

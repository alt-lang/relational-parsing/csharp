﻿namespace Parser.Benchmarks
{
    public interface INativeGrammar : IGrammar
    {
        IParser<TValue> CreateParser<TValue>(IValuationSemiring<TValue> semiring);
    }

    public class NativeParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features
            => ParserFeatures.ArbitrarySemiring;

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
            => ((INativeGrammar) grammar).CreateParser(semiring);
    }
}

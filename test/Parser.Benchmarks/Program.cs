﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using CommandLine;
using GlobExpressions;

namespace Parser.Benchmarks
{
    using Utilities;

    public class Program
    {
        private class Options
        {
            [Value(0, Min = 1, HelpText = "Files to parse. May include glob patterns.")]
            public IEnumerable<String> Inputs { get; set; }

            [Option('b', "benchmark", Required = true, HelpText = "Benchmark to run.")]
            public String Benchmark { get; set; }

            [Option('i', "input", HelpText = "Cache file for tokenized input. Will be created if it does not exist.")]
            public String InputCache { get; set; }

            [Option('p', "parser", HelpText = "Parser generators to benchmark.")]
            public IEnumerable<String> Generators { get; set; }

            [Option('m', "mode", Default = "recognition", HelpText = "Parsing mode. Can be one of 'recognition', 'counting', or whatever else the benchmark generator supports.")]
            public String Mode { get; set; }

            [Option('d', "dot-every", Default = 10000, HelpText = "Print progress dot every <count> terminals.")]
            public Int32 DotEvery { get; set; }
        }

        private static Int32 RealMain(Options options)
        {
            var timer = new Stopwatch();
            try
            {
                var instantiator = new Instantiator()
                {
                    { typeof(Relational.IWrapEngineFactory), "CommutativeWrapEngineFactory(null)" }
                };
                void ReportError(Exception ex) => Console.Error.WriteLine($"Warning: failed instantiation: {ex}");

                Console.WriteLine("Instantiating benchmark:");
                var benchmark = instantiator.InstantiateAll<IBenchmark>(options.Benchmark, ReportError).Single();
                Console.WriteLine($"  {benchmark}");

                Console.WriteLine("Selecting benchmark mode:");
                var mode = benchmark.Mode(options.Mode);
                Console.WriteLine($"  {mode}");

                Console.WriteLine("Instantiating parser generators:");
                var generators = new List<ParserGenerator>();
                foreach (var description in options.Generators)
                {
                    Console.WriteLine($"  {description}");
                    generators.AddRange(instantiator.InstantiateAll<ParserGenerator>(description, ReportError));
                }
                if (generators.Count == 0)
                    throw new ArgumentException("No parser generators chosen. Aborting.");
                Console.WriteLine($"{generators.Count} generator(s) to benchmark.");

                timer.Restart();
                List<Char[]> inputs;
                var formatter = new BinaryFormatter();
                try
                {
                    using (var stream = File.OpenRead(options.InputCache))
                        inputs = (List<Char[]>)formatter.Deserialize(stream);
                    Console.WriteLine($"Read cached input from {options.InputCache}");
                }
                catch (Exception)
                {
                    Console.WriteLine($"Scanning input files:");
                    inputs = new List<Char[]>();
                    foreach (var pattern in options.Inputs)
                    {
                        Console.Write($"  {pattern} ");
                        var fragments = pattern.Split(Path.DirectorySeparatorChar);
                        var leading = Math.Min(fragments.TakeWhile(s => !s.Contains('*')).Count(), fragments.Length - 1);
                        var root = (leading > 0) ? String.Join(Path.DirectorySeparatorChar, fragments.Take(leading)) : Directory.GetCurrentDirectory();
                        var lookup = String.Join(Path.DirectorySeparatorChar, fragments.Skip(leading));
                        Int64 count = 0;
                        foreach (var file in new DirectoryInfo(root).GlobFiles(lookup))
                            using (var reader = new StreamReader(file.FullName))
                            {
                                var lastCount = count;
                                var input = benchmark.Input(reader).ToArray();
                                inputs.Add(input);
                                count += input.Length;
                                for (var i = count / options.DotEvery - lastCount / options.DotEvery; i > 0; i--)
                                    Console.Write('.');
                            }
                        Console.WriteLine();
                    }
                    if (options.InputCache != null)
                    {
                        Console.WriteLine($"Writing input to {options.InputCache}");
                        using (var stream = File.OpenWrite(options.InputCache))
                            formatter.Serialize(stream, inputs);
                    }
                }
                var inputTime = timer.ElapsedMilliseconds;
                Console.WriteLine($"Inputs: {inputs.Count} file(s), {inputs.Sum(_ => _.Length)} token(s), {inputTime} ms");

                foreach (var generator in generators)
                    mode.Accept(new RunRequest
                    {
                        Grammar = benchmark.Grammar,
                        Inputs = inputs,
                        ParserGenerator = generator,
                        DotEvery = options.DotEvery,
                    });
            }
            catch (Exception error)
            {
                Console.Error.WriteLine(error);
                return 1;
            }
            return 0;
        }

        public static Int32 Main(String[] args) =>
            CommandLine.Parser.Default.ParseArguments<Options>(args).MapResult(RealMain, _ => 1);

        private class RunRequest : IModeVisitor<Object>
        {
            public IGrammar Grammar;
            public IReadOnlyList<Char[]> Inputs;
            public ParserGenerator ParserGenerator;
            public Int32 DotEvery;

            public Object Visit<TValue>(IMode<TValue> mode)
            {
                var timer = new Stopwatch();
                Console.Write($"Running {ParserGenerator} ");
                timer.Restart();
                var parser = ParserGenerator.CreateParser(Grammar, mode.Semiring);
                var createTime = timer.ElapsedMilliseconds;
                Measurement.AssignNames(parser, nameof(parser));
                IEnumerable<TValue> Values()
                {
                    timer.Restart();
                    Int64 count = 0;
                    foreach (var input in Inputs)
                        yield return parser.Run(input, () =>
                        {
                            if (count++ % DotEvery == 0)
                                Console.Write(".");
                        });
                    var parseTime = timer.ElapsedMilliseconds;
                    Console.WriteLine($" took {createTime} + {parseTime} ms");
                };
                mode.Process(Values());
                Measurement.PrintAll(Console.Out);
                return null;
            }
        }
    }
}

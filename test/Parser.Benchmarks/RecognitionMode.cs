using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Benchmarks
{
    using Valuations;

    public class RecognitionMode : Mode<Boolean>
    {
        public override IValuationSemiring<Boolean> Semiring
            => new BooleanSemiring();

        public override void Process(IEnumerable<Boolean> values)
        {
            var accepted = 0;
            foreach (var value in values)
                if (value)
                    accepted++;
            Console.WriteLine($"Result: {accepted} inputs accepted.");
        }
    }
}

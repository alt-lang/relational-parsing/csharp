using System;
using System.Reflection;

namespace Parser.Benchmarks
{
    public class SampleBenchmark : Benchmark
    {
        public override IGrammar Grammar { get; }

        public SampleBenchmark(String name)
        {
            var field = typeof(Parser.Grammars.SampleGrammar).GetField(name, BindingFlags.Public | BindingFlags.Static);
            Grammar = (IGrammar) field.GetValue(null);
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Parser.Benchmarks
{
    public class NativeBenchmark : IBenchmark
    {
        private readonly IBenchmark Benchmark;

        public NativeBenchmark(String assemblyPath)
        {
            var assembly = Assembly.LoadFrom(Path.GetFullPath(assemblyPath));
            var benchmarkType = assembly.GetTypes().Single(typeof(IBenchmark).IsAssignableFrom);
            Benchmark = (IBenchmark) Activator.CreateInstance(benchmarkType);
        }

        public IGrammar Grammar
            => Benchmark.Grammar;
        public IEnumerable<Char> Input(TextReader reader)
            => Benchmark.Input(reader);
        public IMode Mode(String name)
            => Benchmark.Mode(name);
    }
}

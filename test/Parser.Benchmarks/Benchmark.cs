﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Parser.Benchmarks
{
    public interface IBenchmark
    {
        IGrammar Grammar { get; }
        IEnumerable<Char> Input(TextReader reader);
        IMode Mode(String name);
    }

    public abstract class Benchmark : IBenchmark
    {
        public abstract IGrammar Grammar { get; }

        public virtual IEnumerable<Char> Input(TextReader reader)
            => reader.ReadToEnd();

        public virtual IMode Mode(String name)
        {
            switch (name)
            {
                case "counting":
                    return new CountingMode();
                case "recognition":
                    return new RecognitionMode();
                default:
                    throw new ArgumentException($"Unsupported mode '{name}' requested for {this.GetType().FullName}");
            }
        }
    }
}

import java.io.*;
import java.nio.file.*;
import java.util.*;
import javax.tools.*;
import com.sun.source.util.*;

public class JavaParser {
    public static void main(String[] args) throws IOException {
        List<File> files = new ArrayList<File>();
        FileSystem fileSystem = FileSystems.getDefault();
        for (String arg : args) {
            Path cwd = fileSystem.getPath(arg);
            PathMatcher matcher = fileSystem.getPathMatcher("glob:**/*.java");
            Files.walk(cwd).filter(p -> matcher.matches(p)).forEach(p -> files.add(p.toFile()));
        }
        System.out.println("Found " + files.size());
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        JavacTask task = (JavacTask) compiler.getTask(null, fileManager, null, null, null, fileManager.getJavaFileObjectsFromFiles(files));
        long startTime = System.currentTimeMillis();
        task.parse().forEach(t -> {
        });
        long parseTime = System.currentTimeMillis() - startTime;
        System.out.println("Total time: " + parseTime + " ms");
        fileManager.close();
    }
}

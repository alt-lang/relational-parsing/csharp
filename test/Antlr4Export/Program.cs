using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Antlr4;
using Antlr4.Tool.Ast;
using CommandLine;

namespace Antlr4Export
{
    public class Program
    {
        private class Options
        {
            [Option('g', "grammar", Required = true, HelpText = "ANTLR4 grammar to export.")]
            public String Grammar { get; set; }

            [Option('l', "lexer", Required = true, HelpText = "Name of the lexer class (to get terminals from).")]
            public String Lexer { get; set; }

            [Option('c', "class-name", HelpText = "Name of the grammar class to generate.")]
            public String ClassName { get; set; } = "Grammar";

            [Option('x', "extern-alias", HelpText = "Extern alias for Parser namespaces.")]
            public String ExternAlias { get; set; } = "Grammar";

            [Option('o', "output", HelpText = "Output file name.")]
            public String Output { get; set; }
        }

        public static Int32 Main(String[] args)
            => CommandLine.Parser.Default.ParseArguments<Options>(args).MapResult(RealMain, _ => 1);

        private static Int32 RealMain(Options options)
        {
            try
            {
                var antlrTool = new AntlrTool();
                antlrTool.inputDirectory = ".";
                var ast = antlrTool.ParseGrammar(options.Grammar);
                using (var writer = String.IsNullOrEmpty(options.Output) ? new StreamWriter(Console.OpenStandardOutput()) : new StreamWriter(options.Output))
                {
                    var prefix = String.Empty;
                    if (!String.IsNullOrEmpty(options.ExternAlias))
                    {
                        writer.WriteLine($"extern alias {options.ExternAlias};");
                        prefix = options.ExternAlias + "::";
                    }
                    writer.WriteLine($"public class {options.ClassName} : {prefix}Parser.Grammars.SimpleGrammar");
                    writer.WriteLine("{");
                    writer.WriteLine($"    public {options.ClassName}()");
                    writer.WriteLine("    {");
                    writer.WriteLine($"        var lexer = new {options.Lexer}(null);");
                    ast.Visit(new GrammarVisitor(writer));
                    writer.WriteLine("    }");
                    writer.WriteLine("}");
                }
                return 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"ERROR: {ex}");
                return 1;
            }
        }

        private class GrammarVisitor : GrammarASTVisitor
        {
            private readonly TextWriter Writer;
            private readonly Dictionary<String, Int32> RuleToStateMap;
            private Int32 NumStates;
            private Int32 Source;
            private Int32 Target;

            public GrammarVisitor(TextWriter writer)
            {
                Writer = writer;
                RuleToStateMap = new Dictionary<String, Int32>();
                NumStates = 0;
            }

            private Int32 NewState
                => NumStates++;

            private Int32 RuleToState(String name)
            {
                Int32 value;
                if (!RuleToStateMap.TryGetValue(name, out value))
                {
                    value = NewState;
                    RuleToStateMap.Add(name, value);
                }
                return value;
            }

            private Object VisitChildren(GrammarAST node)
            {
                if (node.ChildCount > 0)
                    foreach (GrammarAST child in node.Children)
                        child.Visit(this);
                return null;
            }

            public Object Visit(GrammarAST node) =>
                VisitChildren(node);

            public Object Visit(GrammarRootAST node) =>
                VisitChildren(node);

            public Object Visit(RuleAST node)
            {
                if (node.IsLexerRule())
                    return null;
                Source = RuleToState(node.GetRuleName());
                Target = -1;
                return VisitChildren(node);
            }

            public Object Visit(BlockAST node) =>
                VisitChildren(node);

            public Object Visit(OptionalBlockAST node)
            {
                var epsilon = NewState;
                Writer.WriteLine($"        Add(R({epsilon}));");
                Writer.WriteLine($"        Add(C({Source}, {epsilon}, {Target}));");
                return VisitChildren(node);
            }

            public Object Visit(PlusBlockAST node)
            {
                var (source, target) = (Source, Target);
                var entry = NewState;
                var exit = NewState;
                var epsilon = NewState;
                Writer.WriteLine($"        Add(R({epsilon}));");
                Writer.WriteLine($"        Add(C({Source}, {entry}, {Target}));");
                Writer.WriteLine($"        Add(R({exit}));");
                Writer.WriteLine($"        Add(C({exit}, {epsilon}, {entry}));");
                (Source, Target) = (entry, exit);
                VisitChildren(node);
                (Source, Target) = (source, target);
                return null;
            }

            public Object Visit(StarBlockAST node)
            {
                var (source, target) = (Source, Target);
                var loop = NewState;
                Writer.WriteLine($"        Add(C({Source}, {loop}, {Target}));");
                Writer.WriteLine($"        Add(R({loop}));");
                Source = Target = loop;
                VisitChildren(node);
                (Source, Target) = (source, target);
                return null;
            }

            public Object Visit(AltAST node)
            {
                var (source, target) = (Source, Target);
                var current = source;
                var stack = new Stack<GrammarAST>();
                if (node.ChildCount > 0)
                    foreach (GrammarAST child in node.Children.Reverse())
                        stack.Push(child);
                while (stack.Count > 0)
                {
                    var child = stack.Pop();
                    if (child.GetType() == typeof(GrammarAST))
                    {
                        if (child.ChildCount > 0)
                            foreach (GrammarAST grandchild in child.Children.Reverse())
                                stack.Push(grandchild);
                        continue;
                    }
                    Source = current;
                    Target = current = NewState;
                    child.Visit(this);
                }
                var epsilon = NewState;
                Writer.WriteLine($"        Add(R({epsilon}));");
                (Source, Target) = (source, target);
                if (target == -1)
                {
                    target = NewState;
                    Writer.WriteLine($"        Add(R({target}));");
                }
                Writer.WriteLine($"        Add(C({current}, {epsilon}, {target}));");
                return null;
            }

            public Object Visit(NotAST node) =>
                throw new NotSupportedException();

            public Object Visit(PredAST node) =>
                throw new NotSupportedException();

            public Object Visit(RangeAST node) =>
                throw new NotSupportedException();

            public Object Visit(SetAST node) =>
                throw new NotSupportedException();

            public Object Visit(RuleRefAST node)
            {
                Writer.WriteLine($"        Add(C({Source}, {RuleToState(node.Text)}, {Target}));");
                return null;
            }

            public Object Visit(TerminalAST node)
            {
                Writer.WriteLine($"        Add(S({Source}, (char) lexer.GetTokenType(\"{node.Text}\"), {Target}));");
                return null;
            }
        }
    }
}
